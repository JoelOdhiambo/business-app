package com.org.zoner.fragments

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.org.zoner.API.Business
import com.org.zoner.API.User
import com.org.zoner.R
import com.org.zoner.activities.SearchActivity
import com.org.zoner.adapters.FavouriteAdapter
import com.org.zoner.interfaces.RecyclerViewListener
import es.dmoral.toasty.Toasty

/**
 * A simple [Fragment] subclass.
 */
class FavouriteFragment : Fragment() {
    lateinit var recyclerView: RecyclerView
    lateinit var adapter: FavouriteAdapter
    lateinit var infoCenter: RelativeLayout
    lateinit var clearAll: ImageView

    lateinit var businessApi: Business
    lateinit var userApi: User

    val TAG = "FavouriteFragment"

    lateinit var listener: RecyclerViewListener

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_favourite, container, false)
        initViews(rootView!!)
        return rootView
    }

    private fun initViews(rootView: View) {
        recyclerView = rootView.findViewById(R.id.recyclerView)
        infoCenter = rootView.findViewById(R.id.infoCenter)
        recyclerView.layoutManager = LinearLayoutManager(activity)
        clearAll = rootView.findViewById(R.id.clearAll)

        infoCenter.setOnClickListener {
            val i = Intent(requireActivity(), SearchActivity::class.java)
            requireActivity().startActivity(i)
        }


        businessApi = Business(requireActivity())
        userApi = User(requireActivity())

        businessApi.getBusinesses("f") { list ->
            if (list.isNotEmpty()) {
                if (isAdded && requireActivity() != null) {
                    adapter = FavouriteAdapter(requireActivity(), list, listener)
                    recyclerView.adapter = adapter
                    adapter.notifyDataSetChanged()
                    infoCenter.visibility = View.GONE
                }
            } else {
                infoCenter.visibility = View.VISIBLE
            }
        }

        clearAll.setOnClickListener {
            val dialog = Dialog(requireContext())
            dialog.setContentView(R.layout.confirm_dialog)


            val yes = dialog.findViewById(R.id.yes) as TextView
            val no = dialog.findViewById(R.id.no) as TextView
            val dialogText = dialog.findViewById(R.id.dialogText) as TextView
            dialogText.text = getString(R.string.clear_all_favourites)
            yes.setOnClickListener {
                userApi.deleteFavouriteBusiness(null) { deleted ->
                    if (deleted) {
                        adapter = FavouriteAdapter(requireActivity(), arrayListOf(), listener)
                        recyclerView.adapter = adapter
                        adapter.notifyDataSetChanged()
                        infoCenter.visibility = View.VISIBLE
                    } else {
                        Toasty.error(
                            requireContext(),
                            "Something went wrong! Please try again later",
                            Toasty.LENGTH_SHORT
                        ).show()
                    }
                }
            }
            no.setOnClickListener {
                dialog.dismiss()
            }
            dialog.show()


        }



        listener = object : RecyclerViewListener {
            override fun onClick(view: View?, position: Int) {

            }

            override fun onLongClick(view: View?, position: Int) {
            }

        }
    }
}
