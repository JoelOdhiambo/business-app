package com.org.zoner.offline

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(
    entities = [Business::class, Favourite::class, Search::class],
    version = 2,
    exportSchema = false
)
abstract class BusinessDatabase : RoomDatabase() {
    abstract fun businessDao(): BusinessDAO
    abstract fun favouriteDao(): FavouriteDAO

    companion object {
        var INSTANCE: BusinessDatabase? = null
        fun getDatabase(context: Context): BusinessDatabase? {
            if (INSTANCE == null) {
                synchronized(BusinessDatabase::class) {
                    INSTANCE =
                        Room.databaseBuilder(context, BusinessDatabase::class.java, "business_db")
                            .build()
                }
            }
            return INSTANCE
        }
    }
}