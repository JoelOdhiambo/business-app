package com.org.zoner.utils.recyclerview

import androidx.recyclerview.widget.DiffUtil
import com.org.zoner.pojo.Message

class ChatsDiffUtil(var oldList: ArrayList<Message>, var newList: ArrayList<Message>) :
    DiffUtil.Callback() {
    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return newList[newItemPosition].timeStamp == oldList[oldItemPosition].timeStamp &&
                newList[newItemPosition].message == oldList[oldItemPosition].message
    }

    override fun getOldListSize(): Int {
        return oldList.size
    }

    override fun getNewListSize(): Int {
        return newList.size
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return newList[newItemPosition].timeStamp == oldList[oldItemPosition].timeStamp
    }
}