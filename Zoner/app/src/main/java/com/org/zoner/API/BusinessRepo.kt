package com.org.zoner.API

import android.app.Application
import androidx.lifecycle.LiveData
import com.org.zoner.offline.*
import com.org.zoner.offline.Business
import org.jetbrains.anko.doAsync

class BusinessRepo(application: Application) {
    var database: BusinessDatabase? = BusinessDatabase.getDatabase(application.applicationContext)
    fun getAllBusinessesFromDB(): LiveData<List<Business>> {
        return database!!.businessDao().getAllBusinesses()
    }

    fun getAllFavourites(): LiveData<List<Favourite>> {
        return database!!.favouriteDao().getAllFavourites()
    }

    fun getBusinessById(id: String): LiveData<Business> {
        return database!!.businessDao().getBusinessFromId(id)
    }

    fun getBusinessByName(name: String): LiveData<List<Business>> {
        return database!!.businessDao().getBusinessFromName(name)
    }


    fun getSearchFromBusinessId(businessId: String): LiveData<Search> {
        return database!!.businessDao().getSearchFromBusinessId(businessId)
    }

    fun getFavouriteFromBid(businessId: String): LiveData<Favourite> {
        return database!!.businessDao().getFavouriteFromBusinessId(businessId)
    }

    fun getSearches(): LiveData<List<BusinessesWithSearches>> {
        return database!!.businessDao().getSearches()
    }

    fun getFavourites(): LiveData<List<BusinessWithFavourites>> {
        return database!!.businessDao().getFavourites()
    }

    fun insertSearch(search: Search, callback: (Long) -> Unit) = doAsync {
        callback(database!!.businessDao().insertSearch(search))
    }

    fun insertFavourite(favourite: Favourite, callback: (Long) -> Unit) = doAsync {
        callback(database!!.favouriteDao().insertFavourite(favourite))
    }

    fun insertBusiness(business: Business, callback: (Long) -> Unit) = doAsync {
        callback(database!!.businessDao().insertBusiness(business))
    }

    fun deleteFavourite(favourite: Favourite) = doAsync {
        database!!.favouriteDao().deleteFavourite(favourite)
    }

    fun deleteSearch(search: Search) = doAsync {
        database!!.businessDao().deleteSearch(search)
    }

    fun deleteBusiness(business: Business) = doAsync {
        database!!.businessDao().deleteBusiness(business)
    }

}