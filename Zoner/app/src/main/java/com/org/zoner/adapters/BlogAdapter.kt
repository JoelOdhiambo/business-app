package com.org.zoner.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.org.zoner.R
import com.org.zoner.interfaces.OnBottomReached
import com.org.zoner.pojo.Blog
import com.org.zoner.utils.date.DateTimeStyle
import com.org.zoner.utils.date.DateTimeUtils
import java.util.*


class BlogAdapter(
    context: Context,
    blog: ArrayList<Blog>
) :
    RecyclerView.Adapter<BlogAdapter.BlogHolder>() {
    var ctx: Context = context
    var blog: List<Blog> = blog
    val TAG = "BlogAdapter"
    lateinit var bottomReachedListener: OnBottomReached

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BlogHolder {
        val view = LayoutInflater.from(ctx).inflate(R.layout.blog_layout, parent, false)
        return BlogHolder(view)
    }

    override fun getItemCount(): Int {
        return blog.size
    }

    fun setOnBottomReachedListener(bottomReached: OnBottomReached) {
        bottomReachedListener = bottomReached
    }

    override fun onBindViewHolder(holder: BlogHolder, position: Int) {
        val blogObject: Blog = blog[position]
        if (position == blog.size - 1)
            bottomReachedListener.bottomReached(position)
        with(blogObject) {
            Glide.with(ctx)
                .applyDefaultRequestOptions(
                    RequestOptions().placeholder(
                        R.drawable.placeholder_img
                    )
                )
                .load(image)
                .into(holder.blogImage)
            val date: Date = DateTimeUtils.formatDate(timestamp)
            holder.blogWriteUp.text = writeUp
            holder.blogDate.text = "Written: ${DateTimeUtils.getTimeAgo(ctx, date)}"
        }
    }

    inner class BlogHolder(
        itemView: View
    ) : RecyclerView.ViewHolder(itemView) {

        var blogImage: ImageView = itemView.findViewById(R.id.blogImage)
        var blogWriteUp: TextView = itemView.findViewById(R.id.blogWriteUp)
        var blogDate: TextView = itemView.findViewById(R.id.blogDate)

    }
}