package com.org.zoner.utils.recyclerview

import androidx.recyclerview.widget.DiffUtil
import com.org.zoner.pojo.Business


class BusinessDiffUtil(
    private val oldList: ArrayList<Business>,
    private val newList: ArrayList<Business>
) : DiffUtil.Callback() {
    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].businessId == newList[newItemPosition].businessId
    }

    override fun getOldListSize(): Int {
        return oldList.size
    }

    override fun getNewListSize(): Int {
        return newList.size
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].businessName == newList[newItemPosition].businessName
    }

    override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? {
        return super.getChangePayload(oldItemPosition, newItemPosition)
    }

}