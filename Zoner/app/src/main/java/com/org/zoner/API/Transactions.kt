package com.org.zoner.API

import android.content.Context
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.org.zoner.pojo.Transaction
import java.util.*

class Transactions(context: Context) {
    private val ctx = context
    private val transactionCollection: CollectionReference =
        FirebaseFirestore.getInstance().collection("transaction")
    private val businessApi: Business = Business(ctx)

    fun getTransactions(userDocumentId: String, callback: (ArrayList<Transaction>) -> Unit) {
        val a = arrayListOf<Transaction>()
         transactionCollection.whereEqualTo("uid", userDocumentId).get()
            .addOnSuccessListener { r ->
                if (!r!!.isEmpty) {
                    for ((pos, item) in r.documents.withIndex()) {
                        if (item.exists()) {
                            if (item.contains("bid") && item.contains("a") && item.contains("ts")) {
                                businessApi.getBusinessName(item["bid"] as String) { n ->
                                    if (n != null) {
                                        a.add(
                                            Transaction(
                                                item.id,
                                                item["bid"] as String,
                                                n,
                                                item["a"] as Long,
                                                item["ts"] as Long
                                            )
                                        )
                                        if (pos == r.documents.size - 1) {
                                            Collections.sort(
                                                a,
                                                kotlin.Comparator { o1, o2 -> if (o1.timeStamp > o2.timeStamp) 1 else 0 })
                                            callback(a)
                                        }
                                    }
                                }
                            }
                        }


                    }
                } else
                    callback(a)
            }.addOnFailureListener { _ ->
                callback(a)
            }

    }
}