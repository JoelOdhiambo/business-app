package com.org.zoner.utils.mpesa;

public class Pair<X, Y> {
    public X code;
    public Y message;
    public Pair(X x, Y y){
        this.code = x;
        this.message = y;
    }
}