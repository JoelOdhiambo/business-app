package com.org.zoner.activities

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.org.zoner.API.Business
import com.org.zoner.R
import com.org.zoner.adapters.CategoriesAdapter
import com.org.zoner.adapters.MyBusinessesAdapter
import com.org.zoner.fragments.ProfileFragment
import com.org.zoner.interfaces.MyBusinessesListener
import com.org.zoner.interfaces.RecyclerViewListener
import com.org.zoner.models.BusinessModel
import com.org.zoner.offline.BusinessesWithSearches
import com.org.zoner.offline.Search
import com.org.zoner.pojo.Category
import es.dmoral.toasty.Toasty

class SearchActivity : AppCompatActivity(), SearchView.OnQueryTextListener {
    var toolbar: Toolbar? = null
    lateinit var searchRecylerView: RecyclerView
    lateinit var categoryRecyclerView: RecyclerView
    lateinit var searchView: SearchView
    lateinit var infoCenter: RelativeLayout
    lateinit var searchInfoText: TextView

    var searchedBusinesses = arrayListOf<com.org.zoner.pojo.Business>()
    lateinit var businessModel: BusinessModel

    lateinit var adapter: MyBusinessesAdapter
    lateinit var categoriesAdapter: CategoriesAdapter
    lateinit var listener: RecyclerViewListener

    lateinit var business: Business
    private val TAG = "Search Activity"
    var refreshRecentSearches = true
    val gson = Gson()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        init()
    }

    private fun init() {

        business = Business(this@SearchActivity)
        businessModel = ViewModelProviders.of(this@SearchActivity).get(BusinessModel::class.java)

        toolbar = findViewById(R.id.toolBar)
        searchRecylerView = findViewById(R.id.searchRecylerView)
        categoryRecyclerView = findViewById(R.id.categoriesRecyclerView)
        searchView = findViewById(R.id.search_view)
        infoCenter = findViewById(R.id.infoCenter)
        searchInfoText = findViewById(R.id.searchInfo)


        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_yellow_24dp)
        searchRecylerView.layoutManager = LinearLayoutManager(this)

        initializeCategories()

        searchView.setOnQueryTextListener(this)


        val supportFragmentManager = supportFragmentManager.beginTransaction()
        val fragment = ProfileFragment()
        val bundle = Bundle()

        listener = object : RecyclerViewListener {
            override fun onClick(view: View?, position: Int) {

                hideSoftInput()

                bundle.putString("businessId", searchedBusinesses[position].businessId)
                bundle.putString("name", searchedBusinesses[position].businessName)
                bundle.putString("description", searchedBusinesses[position].businessDescription)
                bundle.putString("created", searchedBusinesses[position].businessCreated)
                bundle.putString("messaging_token", searchedBusinesses[position].message_token)
                intent.putExtra(
                    "point",
                    "${searchedBusinesses[position].coordinates.latitude}, ${searchedBusinesses[position].coordinates.longitude}"
                )

                if (searchedBusinesses[position].businessStatus.isNotEmpty())
                    bundle.putString(
                        "status",
                        gson.toJson(searchedBusinesses[position].businessStatus)
                    )
                if (searchedBusinesses[position].businessImages.isNotEmpty())
                    bundle.putString(
                        "images",
                        gson.toJson(searchedBusinesses[position].businessImages)
                    )


                businessModel.allBusinesses.removeObservers(this@SearchActivity)
                if (!refreshRecentSearches) {
                    if (businessModel.allBusinessesFromId(searchedBusinesses[position].businessId).value?.id == null) {
                        businessModel.insertBusiness(
                            com.org.zoner.offline.Business(
                                id = searchedBusinesses[position].businessId,
                                name = searchedBusinesses[position].businessName,
                                description = searchedBusinesses[position].businessDescription,
                                url = gson.toJson(searchedBusinesses[position].businessImages),
                                location = "${searchedBusinesses[position].coordinates.latitude}, ${searchedBusinesses[position].coordinates.longitude}",
                                timestamp = searchedBusinesses[position].businessCreated,
                                token = searchedBusinesses[position].message_token!!
                            )
                        ) { id ->
                            id.let {
                                businessModel.rememberSearch(
                                    //0 -> Key is auto-generated roomdb
                                    Search(
                                        0,
                                        searchedBusinesses[position].businessId,
                                        System.currentTimeMillis()
                                    )
                                ) {
                                }
                            }
                        }
                    }

                }
                fragment.arguments = bundle
                supportFragmentManager.replace(R.id.rootView, fragment)
                supportFragmentManager.commit()
            }

            override fun onLongClick(view: View?, position: Int) {
                val dialog = Dialog(this@SearchActivity)
                dialog.setContentView(R.layout.confirm_dialog)
                val yes = dialog.findViewById(R.id.yes) as TextView
                val no = dialog.findViewById(R.id.no) as TextView

                yes.setOnClickListener {
                    Toasty.success(
                        this@SearchActivity,
                        "Recent search removed",
                        Toasty.LENGTH_SHORT
                    ).show()
                    businessModel.deleteSearch(
                        Search(
                            searchId = searchedBusinesses[position].searchId
                        )
                    )
                    searchedBusinesses.removeAt(position)
                    adapter.notifyItemRemoved(position)
                    Toasty.success(
                        this@SearchActivity,
                        "Recent search removed",
                        Toasty.LENGTH_SHORT
                    ).show()
                    if (searchedBusinesses.isEmpty())
                        infoCenter.visibility = View.VISIBLE
                    dialog.dismiss()
                }

                no.setOnClickListener {
                    dialog.dismiss()
                }

                dialog.show()
            }
        }

        businessModel.allSearches.observe(this@SearchActivity, allSearchesObserver())


    }


    private fun initializeCategories() {
        categoryRecyclerView.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)

        categoriesAdapter = CategoriesAdapter(
            this@SearchActivity, arrayListOf(
                Category("Agriculture", "#bc4e9c"),
                Category("Education", "#f80759"),
                Category("Transport", "#FC5C7D"),
                Category("Health", "#6A82FB"),
                Category("Labour", "#00F260"),
                Category("Food and Beverages", "#0575E6"),
                Category("Leisure Activities", "#fc4a1a"),
                Category("Real Estate", "#f7b733"),
                Category("Petroleum", "#74ebd5")
            )
        )

        categoryRecyclerView.adapter = categoriesAdapter
        categoriesAdapter.notifyDataSetChanged()
    }

    private fun allSearchesObserver(): Observer<List<BusinessesWithSearches>> {
        return Observer<List<BusinessesWithSearches>> { searches ->
            if (refreshRecentSearches) {
                if (searches.isNotEmpty()) {
                    searchedBusinesses.clear()
                    infoCenter.visibility = View.GONE
                    for ((i, search) in searches.withIndex()) {
                        searchedBusinesses.add(
                            com.org.zoner.pojo.Business(
                                businessId = search.searches.businessId.toString(),
                                businessName = search.business.name,
                                businessDescription = search.business.description,
                                message_token = search.business.token,
                                businessImages = getImageUrls(search.business.url),
                                businessCreated = search.searches.searchedOn.toString(),
                                searchId = search.searches.searchId
                            )
                        )
                        if (i == searches.size - 1) {
                            adapter =
                                MyBusinessesAdapter(
                                    this@SearchActivity,
                                    searchedBusinesses,
                                    listener
                                )
                            searchRecylerView.adapter = adapter
                            adapter.notifyDataSetChanged()
                        }
                    }
                }

            }
        }
    }


    private fun hideSoftInput() {
        val view = this.currentFocus
        view?.let {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    private fun getImageUrls(json: String): java.util.ArrayList<String> {
        val type = object :
            TypeToken<java.util.ArrayList<String?>?>() {}.type
        return gson.fromJson(json, type)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }

    var canSearch = true
    override fun onQueryTextSubmit(query: String?): Boolean {
        if (canSearch) {
            canSearch = false
            refreshRecentSearches = false
            if (!query.equals("") || query != null) {
                business.searchBusiness(query!!.toUpperCase(), object : MyBusinessesListener {
                    override fun myBusinesses(list: ArrayList<com.org.zoner.pojo.Business>) {
                        infoCenter.visibility = View.GONE
                        searchedBusinesses.clear()
                        searchedBusinesses = list
                        canSearch = true
                        adapter = MyBusinessesAdapter(this@SearchActivity, list, listener, true)
                        searchRecylerView.adapter = adapter
                        adapter.notifyDataSetChanged()
                    }

                    override fun error(error: String) {
                        infoCenter.visibility = View.VISIBLE
                        canSearch = true
                        searchInfoText.text = error
                        adapter = MyBusinessesAdapter(this@SearchActivity, arrayListOf(), listener)
                        searchRecylerView.adapter = adapter
                        adapter.notifyDataSetChanged()
                    }
                })
            } else {
                Toasty.error(
                    this@SearchActivity,
                    getString(R.string.empty_search),
                    Toasty.LENGTH_SHORT
                ).show()
            }
        }
        return true
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        return false
    }

    override fun onBackPressed() {
        finish()
    }
}