package com.org.zoner.utils.notification

import android.util.Log
import com.onesignal.OneSignal
import com.org.zoner.Config
import org.json.JSONObject

class SendNotification {

    companion object {
        val TAG = "SendNotification"
        fun push(payload: HashMap<String, Any>, callback: (Boolean) -> Unit) {
            val jsonObject = JSONObject(
                "{'contents':{'en':'" + payload["message"] + "'}," +
                        "'data': { 'businessId': '" + payload["businessId"] + "', " +
                        "'activity': '" + payload["activity"] + "'," +
                        "'fragment': '" + payload["fragment"] + "'} ," +
                        "'include_player_ids':['" + payload["token"] + "']," +
                        "'android_channel_id':['" + Config.NOTIFICATION_CHANNEL_ID + "']," +
                        "'android_group':'" + Config.MESSAGE_GROUP + "'," +
                        "'headings':{'en': '" + payload["header"] + "'}}"
            )
            OneSignal.postNotification(
                jsonObject,
                object : OneSignal.PostNotificationResponseHandler {
                    override fun onSuccess(response: JSONObject?) {
                        Log.d(TAG, response.toString())
                        callback(true)
                    }

                    override fun onFailure(response: JSONObject?) {
                        Log.d(TAG, response.toString())
                        callback(false)
                    }
                })
        }
    }
}