package com.org.zoner.offline

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface FavouriteDAO {
    @Query("SELECT * FROM favourite")
    fun getAllFavourites(): LiveData<List<Favourite>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertFavourite(favourite: Favourite): Long

    @Delete
    fun deleteFavourite(favourite: Favourite)

}