package com.org.zoner.activities

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.app.Activity
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.facebook.shimmer.ShimmerFrameLayout
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.firestore.GeoPoint
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.mapbox.mapboxsdk.camera.CameraPosition
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.plugins.places.picker.PlacePicker
import com.mapbox.mapboxsdk.plugins.places.picker.model.PlacePickerOptions
import com.org.zoner.API.Business
import com.org.zoner.API.Chat
import com.org.zoner.API.PersistentStore
import com.org.zoner.API.User
import com.org.zoner.R
import com.org.zoner.adapters.ProfileImagesAdapter
import com.org.zoner.interfaces.OnBottomReached
import com.org.zoner.utils.date.DateTimeStyle
import com.org.zoner.utils.date.DateTimeUtils
import com.org.zoner.utils.image.ImageUpload
import es.dmoral.toasty.Toasty
import java.util.*

class EditProfileActivity : AppCompatActivity(), View.OnClickListener {
    var toolbar: Toolbar? = null
    lateinit var name: TextInputEditText
    lateinit var email: TextInputEditText
    lateinit var website: TextInputEditText
    lateinit var about: TextInputEditText
    lateinit var businessApi: Business
    lateinit var webContainer: LinearLayout
    lateinit var changeLocation: LinearLayout
    lateinit var emailContainer: LinearLayout
    lateinit var joinedTextView: TextView
    lateinit var deleteBusiness: LinearLayout
    lateinit var changeImageHeader: RelativeLayout
    lateinit var shimmerLayout: ShimmerFrameLayout
    lateinit var profileImagesRecyclerView: RecyclerView
    lateinit var profileImageAdapter: ProfileImagesAdapter
    lateinit var imagesInfo: TextView
    var imagesArray = arrayListOf<String>()

    val gson = Gson()
    var isBusiness: Boolean = false
    var placeCoordinates: GeoPoint? = null
    val REQUEST_CODE_AUTOCOMPLETE = 20
    lateinit var store: PersistentStore

    lateinit var userApi: User
    lateinit var imageUpload: ImageUpload
    var profileImages: ArrayList<String> = arrayListOf()
    var statusImages: ArrayList<String> = arrayListOf()

    val EDIT_PROFILE_IMAGE_REQUEST = 432
    lateinit var chatApi: Chat


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profile)
        init()
    }

    private fun init() {

        businessApi = Business(this@EditProfileActivity)
        userApi = User(this@EditProfileActivity)
        store = PersistentStore(this)
        imageUpload = ImageUpload(this)
        chatApi = Chat(this)

        toolbar = findViewById(R.id.toolBar)
        name = findViewById(R.id.name)
        email = findViewById(R.id.email)
        website = findViewById(R.id.website)
        about = findViewById(R.id.about)
        webContainer = findViewById(R.id.webContainer)
        changeLocation = findViewById(R.id.changeLocation)
        emailContainer = findViewById(R.id.emailContainer)
        joinedTextView = findViewById(R.id.joinedText)
        deleteBusiness = findViewById(R.id.deleteBusiness)
        changeImageHeader = findViewById(R.id.changeImageHeader)
        shimmerLayout = findViewById(R.id.shimmerLayout)
        profileImagesRecyclerView = findViewById(R.id.profileImagesRecyclerView)
        profileImagesRecyclerView.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        imagesInfo = findViewById(R.id.imagesInfo)

        isBusiness = intent.getBooleanExtra("isBusiness", false)
        name.setText(intent.getStringExtra("name"))
        about.setText(intent.getStringExtra("desc"))
        email.setText(intent.getStringExtra("email"))
        val web = intent.getStringExtra("website")

        val c =
            intent.getStringExtra("geoLocation")
        if (c != null) {
            val coordinates = c.split(",").dropLastWhile { s -> false }
            placeCoordinates = GeoPoint(coordinates[0].toDouble(), coordinates[1].toDouble())
        }

        if(intent.getStringExtra("joined") != null){
            val date: Date = DateTimeUtils.formatDate(intent.getStringExtra("joined")!!.toLong())
            joinedTextView.text = DateTimeUtils.formatWithStyle(date, DateTimeStyle.MEDIUM)
        }

        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_yellow_24dp)
        supportActionBar!!.title = "Profile"
        toolbar!!.setTitleTextColor(resources.getColor(R.color.colorPrimary))


        changeLocation.setOnClickListener(this)

        if (web != null)
            website.setText(web)
        else
            webContainer.visibility = View.GONE

        if (isBusiness) {
            emailContainer.visibility = View.GONE
            deleteBusiness.visibility = View.VISIBLE
            changeImageHeader.visibility = View.VISIBLE
            shimmerLayout.visibility = View.VISIBLE

            val images = intent.getStringExtra("businessImages")
            val sImages = intent.getStringExtra("statusImages")!!

            sImages.let {
                statusImages = getImageUrls(it)
            }

            images.let {
                profileImages = getImageUrls(it!!)

                if (profileImages.isNotEmpty()) {
                    profileImageAdapter = ProfileImagesAdapter(this, profileImages)
                    profileImagesRecyclerView.adapter = profileImageAdapter
                    profileImageAdapter.setOnBottomReachedListener(object : OnBottomReached {
                        override fun bottomReached(position: Int) {

                        }

                    })
                    profileImageAdapter.notifyDataSetChanged()
                    shimmerLayout.visibility = View.GONE
                    profileImagesRecyclerView.visibility = View.VISIBLE
                }

            }

        } else {
            changeLocation.visibility = View.GONE
            changeImageHeader.visibility = View.GONE
            profileImagesRecyclerView.visibility = View.GONE
            shimmerLayout.visibility = View.GONE
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.edit_profile_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    private fun openGallery() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
        startActivityForResult(
            Intent.createChooser(intent, "Select image(s)"),
            EDIT_PROFILE_IMAGE_REQUEST
        )
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
            }
            R.id.done -> {
                updateCurrentProfile()
            }
            R.id.addImage -> {
                openGallery()
            }

            R.id.clearAll -> {
                removeAllImages()
            }

        }
        return super.onOptionsItemSelected(item)
    }


    private fun updateCurrentProfile() {
        if (isBusiness) {
            if (imagesArray.isNotEmpty()) {
                val progressDialog = ProgressDialog(this)
                progressDialog.setTitle("Uploading images")
                progressDialog.setMessage("A moment.. Uploading the images...")
                progressDialog.show()
                progressDialog.setCancelable(false)
                progressDialog.setCanceledOnTouchOutside(false)

                if (profileImages.isNotEmpty()) {
                    imageUpload.deleteImageFromBucket(profileImages) { deleted ->
                        if (deleted) {
                            businessApi.uploadBusinessImages(
                                imagesArray,
                                intent.getStringExtra("businessId")!!
                            ) { uploaded ->
                                progressDialog.dismiss()
                                if (uploaded) {
                                    businessApi.updateProfile(
                                        intent.getStringExtra("businessId"),
                                        mutableMapOf(
                                            "d" to about.text.toString(),
                                            "n" to name.text.toString(),
                                            "w" to website.text.toString(),
                                            "geoLocation" to placeCoordinates!!
                                        )
                                    ) { result ->
                                        Toasty.success(
                                            this@EditProfileActivity,
                                            result.second!!,
                                            Toasty.LENGTH_SHORT
                                        )
                                            .show()
                                        store.setBusinessRefresh(true)
                                    }
                                }
                            }
                        }
                    }
                } else {
                    businessApi.uploadBusinessImages(
                        imagesArray,
                        intent.getStringExtra("businessId")!!

                    ) { uploaded ->
                        progressDialog.dismiss()
                        if (uploaded) {
                            businessApi.updateProfile(
                                intent.getStringExtra("businessId"),
                                mutableMapOf(
                                    "d" to about.text.toString(),
                                    "n" to name.text.toString(),
                                    "w" to website.text.toString(),
                                    "geoLocation" to placeCoordinates!!
                                )
                            ) { result ->
                                Toasty.success(
                                    this@EditProfileActivity,
                                    result.second!!,
                                    Toasty.LENGTH_SHORT
                                )
                                    .show()
                                store.setBusinessRefresh(true)
                            }
                        }
                    }
                }

            } else {
                businessApi.updateProfile(
                    intent.getStringExtra("businessId"),
                    mutableMapOf(
                        "d" to about.text.toString(),
                        "n" to name.text.toString().toUpperCase(),
                        "w" to website.text.toString(),
                        "geoLocation" to placeCoordinates!!
                    )
                ) { result ->
                    Toasty.success(this@EditProfileActivity, result.second!!, Toasty.LENGTH_SHORT)
                        .show()
                    store.setBusinessRefresh(true)
                }
            }

        } else {
            userApi.updateProfile(
                mapOf(
                    "a" to about.text.toString(),
                    "n" to name.text.toString(),
                    "e" to email.text.toString()
                )
            ) { updated ->
                if (updated)
                    Toasty.success(
                        this@EditProfileActivity,
                        "Profile updated successfully!",
                        Toasty.LENGTH_SHORT
                    ).show()
                else
                    Toasty.error(
                        this@EditProfileActivity,
                        "Unable to update profile at the moment!",
                        Toasty.LENGTH_SHORT
                    ).show()
                store.setUserRefresh(true)
            }
        }
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.changeLocation -> {
                var lastKnownLocation: LatLng? = null
                if (store.getLastLocation() != null && store.getLastLocation() != "null") {
                    val locationPoint = store.getLastLocation()!!.split(",")
                    lastKnownLocation =
                        LatLng(locationPoint[0].toDouble(), locationPoint[1].toDouble())
                }

                startActivityForResult(
                    PlacePicker.IntentBuilder()
                        .accessToken(getString(R.string.MAP_BOX_ACCESS_TOKEN))
                        .placeOptions(
                            PlacePickerOptions.builder()
                                .includeReverseGeocode(false)
                                .statingCameraPosition(
                                    CameraPosition.Builder()
                                        .target(lastKnownLocation ?: LatLng(1.2921, 36.8219))
                                        .zoom(16.0)
                                        .build()
                                )
                                .build()
                        )
                        .build(this), REQUEST_CODE_AUTOCOMPLETE
                )

            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                REQUEST_CODE_AUTOCOMPLETE -> {

                    val bundle: Bundle = data?.extras!!
                    val cameraPosition =
                        bundle["com.mapbox.mapboxsdk.plugins.places.cameraPosition"].toString()
                    val startIndex = cameraPosition.indexOf("latitude=")
                    val endIndex = cameraPosition.indexOf(", altitude=")
                    val coordinateSubstring = cameraPosition.substring(startIndex, endIndex)
                    val coordinateSplit = coordinateSubstring.split(",")
                    val lat = coordinateSplit[0].replace("latitude=", "")
                    val lng = coordinateSplit[1].replace("longitude=", "")

                    placeCoordinates = GeoPoint(
                        lat.toDouble(),
                        lng.toDouble()
                    )
                }

                EDIT_PROFILE_IMAGE_REQUEST -> {
                    if (data!!.clipData == null) {
                        imagesArray.add(data.data.toString())

                    } else {
                        for (i in 0 until data.clipData!!.itemCount) {
                            imagesArray.add(data.clipData!!.getItemAt(i).uri.toString())
                        }

                    }

                    val animatorSet = AnimatorSet()

                    val scaleX: ObjectAnimator =
                        ObjectAnimator.ofFloat(imagesInfo, "scaleX", 1f, 1.5f)

                    val scaleY: ObjectAnimator =
                        ObjectAnimator.ofFloat(imagesInfo, "scaleY", 1f, 1.5f)


                    val scaleDownX: ObjectAnimator =
                        ObjectAnimator.ofFloat(imagesInfo, "scaleX", 1.5f, 1f)

                    val scaleDownY: ObjectAnimator =
                        ObjectAnimator.ofFloat(imagesInfo, "scaleY", 1.5f, 1f)

                    imagesInfo.text = "${imagesArray.size} image(s) selected"
                    imagesInfo.setTextColor(Color.GREEN)

                    animatorSet.startDelay = 150
                    animatorSet.playTogether(scaleX, scaleY)
                    animatorSet.startDelay = 100
                    animatorSet.playTogether(scaleDownX, scaleDownY)
                    animatorSet.duration = 350
                    animatorSet.start()



                    profileImageAdapter = ProfileImagesAdapter(this, profileImages, true)
                    profileImagesRecyclerView.adapter = profileImageAdapter
                    profileImageAdapter.notifyDataSetChanged()
                }
            }
        }
    }


    private fun getImageUrls(json: String): ArrayList<String> {
        val type = object :
            TypeToken<ArrayList<String?>?>() {}.type
        return gson.fromJson(json, type)
    }

    fun removeAllImages() {
        val dialog = Dialog(this)
        dialog.setContentView(R.layout.confirm_dialog)


        val yes = dialog.findViewById(R.id.yes) as TextView
        val no = dialog.findViewById(R.id.no) as TextView
        val dialogText = dialog.findViewById(R.id.dialogText) as TextView
        dialogText.text = getString(R.string.clear_profile_images)
        yes.setOnClickListener {
            if (profileImages.isNotEmpty() && intent.getStringExtra("businessId") != null) {
                businessApi.deleteProfileImages(
                    intent.getStringExtra("businessId")!!,
                    profileImages
                )
                profileImagesRecyclerView.visibility = View.GONE
                shimmerLayout.visibility = View.VISIBLE
            }
            dialog.dismiss()
        }
        no.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()

    }

    fun changeImage(view: View) {
        openGallery()
    }

    override fun onStop() {
        imagesArray.clear()
        super.onStop()
    }

    fun deleteBusiness(view: View) {
        val dialog = Dialog(this)
        dialog.setContentView(R.layout.confirm_dialog)

        val yes = dialog.findViewById(R.id.yes) as TextView
        val no = dialog.findViewById(R.id.no) as TextView
        val dialogText = dialog.findViewById(R.id.dialogText) as TextView
        dialogText.text = getString(R.string.delete_all_businesses)
        yes.setOnClickListener {
            dialog.dismiss()
            Toasty.success(this, "Deleting business", Toasty.LENGTH_SHORT).show()
            profileImages.addAll(statusImages)
            chatApi.clearAllChats { cleared ->
                if (cleared) {
                    businessApi.deleteBusiness(
                        documentId = intent.getStringExtra("businessId")!!,
                        imageUrls = profileImages
                    ) {
                        if (it.first) {
                            Toasty.success(
                                this@EditProfileActivity,
                                it.second!!,
                                Toasty.LENGTH_SHORT
                            )
                                .show()
                            finish()
                        } else {
                            Toasty.error(this@EditProfileActivity, it.second!!, Toasty.LENGTH_SHORT)
                                .show()
                        }
                    }
                } else {
                    Toasty.error(
                        this,
                        "Something went wrong when clearing chats. Therefor business can not be deleted"
                        ,
                        Toasty.LENGTH_SHORT
                    ).show()
                }
            }
        }
        no.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }


}