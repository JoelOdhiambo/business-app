package com.org.zoner.offline

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.Relation

@Entity(tableName = "business")
data class Business(
    @PrimaryKey var id: String = "",
    var name: String = "",
    var description: String = "",
    var url: String = "",
    var location: String = "",
    var timestamp: String = "",
    var token : String = ""
)








