package com.org.zoner

import android.content.Context
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ProcessLifecycleOwner
import androidx.multidex.MultiDex
import androidx.multidex.MultiDexApplication
import com.onesignal.OneSignal
import com.org.zoner.API.PersistentStore
import com.org.zoner.utils.notification.NotificationOpenedHandler
import com.org.zoner.utils.notification.NotificationReceivedHandler

class MyApp : MultiDexApplication(), LifecycleObserver {
    private var TAG = "MyApplication"
    lateinit var storage: PersistentStore

    override fun onCreate() {
        super.onCreate()
        storage = PersistentStore(this)
        OneSignal.startInit(this)
            .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
            .setNotificationOpenedHandler(
                NotificationOpenedHandler(
                    this
                )
            )
            .setNotificationReceivedHandler(
                NotificationReceivedHandler(
                    this
                )
            )
            .unsubscribeWhenNotificationsAreDisabled(true)
            .init()
        ProcessLifecycleOwner.get().lifecycle.addObserver(this)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun appInForeGround() {
        storage.setAppGrounds(true)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun appInBackground() {
        storage.setAppGrounds(false)
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }
}