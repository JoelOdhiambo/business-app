package com.org.zoner.utils.notification

import android.graphics.Color
import androidx.core.app.NotificationCompat
import com.onesignal.NotificationExtenderService
import com.onesignal.OSNotificationDisplayedResult
import com.onesignal.OSNotificationReceivedResult

class OSNotificationExtender : NotificationExtenderService() {
    override fun onNotificationProcessing(notification: OSNotificationReceivedResult?): Boolean {
        val settings = OverrideSettings()
        settings.extender = NotificationCompat.Extender { builder ->
            builder.setColor(Color.WHITE)
        }
        val result: OSNotificationDisplayedResult? = displayNotification(settings)
        result!!.androidNotificationId
        return true
    }

}