package com.org.zoner.pojo

import com.google.firebase.firestore.GeoPoint

data class Business(
    var businessId: String = "",
    var businessName: String = "",
    var businessDescription: String = "",
    var businessCreated: String = "",
    var message_token: String? = "",
    var coordinates: GeoPoint = GeoPoint(0.0, 0.0),
    var tag: String = "",
    var website: String = "",
    var businessImages: ArrayList<String> = arrayListOf(),
    var businessStatus: ArrayList<String> = arrayListOf(),
    var lastPaidAmount: String?= null,
    var searchId: Int = 0
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Business

        if (businessId != other.businessId) return false
        if (message_token != other.message_token) return false
        if (coordinates != other.coordinates) return false
        if (website != other.website) return false

        return true
    }

    override fun hashCode(): Int {
        var result = businessId.hashCode()
        result = 31 * result + (message_token?.hashCode() ?: 0)
        result = 31 * result + coordinates.hashCode()
        result = 31 * result + website.hashCode()
        return result
    }
}