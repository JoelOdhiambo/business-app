package com.org.zoner.API

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.onesignal.OneSignal
import com.org.zoner.interfaces.SuccessErrListener
import com.org.zoner.interfaces.TrueFalseListener
import com.org.zoner.pojo.User
import es.dmoral.toasty.Toasty

class User(context: Context) {
    var userDB: CollectionReference = FirebaseFirestore.getInstance().collection("user")
    var ctx = context
    var sharedPreferences: SharedPreferences =
        ctx.getSharedPreferences("pref_user_doc_id", Context.MODE_PRIVATE)

    val storage = PersistentStore(context)

    val TAG = "UserApi"

    fun checkIfUserDocumentIdExists(): Boolean {
        return sharedPreferences.getString("id", "null").equals("null")
    }

    fun getProfileDetails(callback: (User) -> Unit) {

        userDB.document(storage.getUserDocumentId()!!).get()
            .addOnSuccessListener { documentSnapshot ->
                if (documentSnapshot.exists()) {
                    if (documentSnapshot.contains("p") && documentSnapshot.contains("j") && documentSnapshot.contains(
                            "t"
                        )
                    ) {
                        var username = "~"
                        var email = "~"
                        var about = "Add something about yourself."
                        if (documentSnapshot["a"] != null)
                            about = documentSnapshot["a"] as String
                        if (documentSnapshot["n"] != null) {
                            username = documentSnapshot["n"] as String
                            email = documentSnapshot["e"] as String
                        }
                        callback(
                            User(
                                documentSnapshot.get("p").toString(),
                                documentSnapshot.get("t").toString(),
                                documentSnapshot.get("j").toString(),
                                username = username,
                                email = email,
                                userId = documentSnapshot.id,
                                about = about
                            )
                        )
                    }

                }
            }

    }


    fun hasBusiness(businessId: String, callback: (Boolean) -> Unit) {
        userDB.document(storage.getUserDocumentId()!!).collection("b")
            .whereEqualTo("i", businessId)
            .limit(1)
            .get().addOnSuccessListener { querySnapshot ->
                if (querySnapshot.isEmpty) {
                    callback(false)
                } else {
                    for (snapshot in querySnapshot) {
                        if (snapshot.exists() && snapshot.contains("i")) {
                            callback(true)
                        } else {
                            callback(false)
                        }
                    }
                }
            }.addOnFailureListener {
                callback(false)
            }
    }

    fun getUserDocumentId(listener: SuccessErrListener) {
        userDB.whereEqualTo("p", "+${storage.getUserPhoneNumber()}")
            .limit(1).get().addOnSuccessListener { querySnapshot ->
                Log.d("User", "Document id does not exist")
                for (snapshot in querySnapshot) {
                    addMessageId(snapshot.id)
                    if (storage.setUserDocumentId(snapshot.id))
                        listener.success("User document id ${snapshot.id}")
                    else
                        listener.error("Error writing user document id to persistent storage")
                }
            }.addOnFailureListener {
                listener.error(it.message.toString())
            }
    }

    private fun addMessageId(userDocumentId: String) {
        OneSignal.idsAvailable { userId, _ ->
            userDB.document(userDocumentId).update(mutableMapOf<String, Any>("t" to userId!!))
                .addOnSuccessListener {
                    storage.setNotificationToken(userId)
                    Log.d(TAG, "Set OneSignal message id ${userId}")
                }.addOnFailureListener {
                    Log.d(TAG, "Failed to set message id in db ${it.message}")
                }
        }
    }

    fun doesEmailExist(phone: String, callback: (Boolean) -> Unit) {
        val q = userDB.whereEqualTo("p", phone).limit(1)
            .get()
        q.addOnSuccessListener { querySnapshot ->
            for (snapshot in querySnapshot) {
                if (snapshot.get("e") == null)
                    callback(false)
                else callback(true)
            }
        }.addOnFailureListener { _ ->
            callback(false)
        }
    }

    fun updateProfile(map: Map<String, Any>, callback: (Boolean) -> Unit) {
        userDB.document(storage.getUserDocumentId()!!)
            .update(map).addOnSuccessListener {
                callback(true)
            }.addOnFailureListener {
                callback(false)
            }
    }

    fun getChatsIStarted(callback: (MutableMap<String, String>) -> Unit) {
        val businessChats = mutableMapOf<String, String>()
        userDB.document(storage.getUserDocumentId()!!)
            .collection("businesschat")
            .orderBy("c")
            .addSnapshotListener { querySnapshot, exception ->
                exception?.let {
                    Toasty.error(ctx, it.message.toString(), Toasty.LENGTH_SHORT).show()
                }
                if (!querySnapshot!!.isEmpty) {
                    for ((_, snapshot) in querySnapshot.withIndex()) {
                        // if (index == querySnapshot.documents.size - 1)
                        if (snapshot.exists() && snapshot.contains("b") && snapshot.contains("c")) {
                            businessChats[snapshot["b"].toString()] = snapshot["c"].toString()
                            callback(businessChats)
                        }
                    }

                }
            }
    }

    fun addToFavourites(businessId: String, callback: (Boolean) -> Unit) {
        userDB.document(storage.getUserDocumentId()!!)
            .collection("f").document()
            .set(mapOf("i" to businessId, "ts" to System.currentTimeMillis()))
            .addOnSuccessListener {
                callback(true)
            }.addOnFailureListener {
                callback(false)
            }
    }


    fun addChatId(details: Map<String, String>, listener: TrueFalseListener) {
        userDB.document(storage.getUserDocumentId()!!).collection("businesschat")
            .add(details)
            .addOnSuccessListener { documentReference ->
                listener.result(result = true, message = documentReference.id)
            }.addOnFailureListener {
                listener.result(false)
            }
    }


    fun deleteFavouriteBusiness(businessId: String?, callback: (Boolean) -> Unit) {
        val query = userDB.document(storage.getUserDocumentId()!!)
            .collection("f")
        if (businessId != null) {
            query.whereEqualTo("i", businessId)
                .get()
                .addOnSuccessListener { snapshot ->
                    if (!snapshot.isEmpty) {
                        for (document in snapshot) {
                            if (document.exists()) {
                                query.document(document.id).delete().addOnSuccessListener {
                                    callback(true)
                                }.addOnFailureListener {
                                    callback(false)
                                }
                            }
                        }
                    }
                }.addOnFailureListener {
                    callback(false)
                }
        } else {
            query.get().addOnSuccessListener { querySnapshot ->
                if (!querySnapshot.isEmpty) {
                    for ((pos, document) in querySnapshot.withIndex()) {
                        if (document.exists() && document.contains("i")) {
                            query.document(document.id).delete()
                        }
                        if (pos == querySnapshot.size() - 1)
                            callback(true)
                    }

                } else {
                    callback(true)
                }
            }
        }
    }
}