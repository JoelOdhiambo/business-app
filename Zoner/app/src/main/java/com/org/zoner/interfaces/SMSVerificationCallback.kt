package com.org.zoner.interfaces

import com.google.firebase.auth.PhoneAuthCredential

interface  SMSVerificationCallback{
    fun onSuccess(credential: PhoneAuthCredential)
    fun onError(errorMessage: String)
    fun onCodeSent(verificationId :String)
}