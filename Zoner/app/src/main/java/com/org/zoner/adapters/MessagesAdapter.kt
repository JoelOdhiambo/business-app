package com.org.zoner.adapters

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.org.zoner.API.PersistentStore
import com.org.zoner.R
import com.org.zoner.pojo.Message
import com.org.zoner.utils.date.DateTimeUtils
import com.org.zoner.utils.recyclerview.ChatsDiffUtil
import java.util.*

class MessagesAdapter(context: Context, messageList: ArrayList<Message>) :
    RecyclerView.Adapter<MessagesAdapter.MessagesHolder>() {
    var ctx: Context = context
    var messages: ArrayList<Message> = messageList
    val persistentStore = PersistentStore(ctx)


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessagesHolder {
        val view = LayoutInflater.from(ctx).inflate(R.layout.chat_layout, parent, false)
        return MessagesHolder(view)
    }

    override fun getItemCount(): Int {
        return messages.size
    }

    override fun onBindViewHolder(holder: MessagesHolder, position: Int) {
        val messageObj = messages[position]
        val params: RelativeLayout.LayoutParams by lazy {
            holder.sideContainer.layoutParams as RelativeLayout.LayoutParams
        }

        with(messageObj) {
            val cal = Calendar.getInstance(TimeZone.getDefault())
            cal.timeInMillis = timeStamp.trim().toLong()
            holder.message.text = message
            var minute = ""
            minute = if (cal.get(Calendar.MINUTE) == 0) "00" else {
                cal.get(Calendar.MINUTE)
                    .toString()
            }

            holder.date.text = DateTimeUtils.formatWithPattern(
                DateTimeUtils.formatDate(timeStamp.toLong()),
                "EE, MMM dd"
            ) + ", " + cal.get(Calendar.HOUR_OF_DAY)
                .toString() + " : " + minute

            if (by != "" && persistentStore.getUserDocumentId() != "null") {
                if (by == persistentStore.getUserDocumentId()) {
                    holder.message.setTextColor(Color.BLACK)
                    holder.date.setTextColor(Color.BLACK)
                    holder.profileImage.visibility = View.GONE
                    params.addRule(RelativeLayout.ALIGN_PARENT_END, RelativeLayout.TRUE)
                    holder.sideContainer.layoutParams = params
                    holder.backgroundIndicator.setBackgroundResource(R.drawable.background_chat_bubble_2)
                }
            }

        }


    }

    fun toLocal(timestamp: Long): Long {
        val cal: Calendar = Calendar.getInstance()
        val offset: Int = cal.getTimeZone().getOffset(timestamp)
        return timestamp - offset
    }

    fun setData(newList: ArrayList<Message>) {
        val diffResult = DiffUtil.calculateDiff(ChatsDiffUtil(messages, newList))

        this.messages.clear()
        this.messages.addAll(newList)
        diffResult.dispatchUpdatesTo(this)
    }

    fun initData(initialData: ArrayList<Message>) {
        this.messages = initialData
        notifyDataSetChanged()
    }

    class MessagesHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var message: TextView = itemView.findViewById(R.id.message)
        var date: TextView = itemView.findViewById(R.id.dateSent)
        var rootView: RelativeLayout = itemView.findViewById(R.id.rootView)
        var sideContainer: LinearLayout = itemView.findViewById(R.id.sideContainer)
        var profileImage: ImageView = itemView.findViewById(R.id.profileImage)
        var backgroundIndicator: RelativeLayout = itemView.findViewById(R.id.backgroundIndicator)
    }
}