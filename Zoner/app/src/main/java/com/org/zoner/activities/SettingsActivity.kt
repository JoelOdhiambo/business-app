package com.org.zoner.activities

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.preference.PreferenceFragmentCompat
import com.org.zoner.R

class SettingsActivity : AppCompatActivity() {

    lateinit var toobar: Toolbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.settings_activity)
        init()
    }

    private fun init() {
        toobar = findViewById(R.id.toolBar)
        setSupportActionBar(toobar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = "Settings"
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_yellow_24dp)
        toobar.setTitleTextColor(resources.getColor(R.color.colorPrimary))



        supportFragmentManager
            .beginTransaction()
            .replace(R.id.settings, SettingsFragment())
            .commit()


    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }

    class SettingsFragment : PreferenceFragmentCompat() {


        override fun onCreatePreferences(
            savedInstanceState: Bundle,
            rootKey: String
        ) {
            setPreferencesFromResource(R.xml.root_preferences, rootKey)

        }

        override fun onStart() {
            super.onStart()

        }

        override fun onStop() {
            super.onStop()

        }

        override fun onResume() {
            super.onResume()
        }
    }
}