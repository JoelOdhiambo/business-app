package com.org.zoner.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout.OnRefreshListener
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.org.zoner.API.Chat
import com.org.zoner.API.PersistentStore
import com.org.zoner.R
import com.org.zoner.activities.SearchActivity
import com.org.zoner.adapters.ChatsAdapter
import com.org.zoner.pojo.Message
import es.dmoral.toasty.Toasty

class ChatFragment : Fragment(), OnRefreshListener, View.OnClickListener {
    lateinit var recyclerView: RecyclerView
    lateinit var swipeRefreshLayout: SwipeRefreshLayout
    lateinit var emptyChatContainer: RelativeLayout

    lateinit var chatsAdapter: ChatsAdapter
    lateinit var chatApi: Chat
    lateinit var store: PersistentStore

    var chats = arrayListOf<Message>()
    val gson = Gson()

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString("chats", listToString())
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_chat, container, false)
        init(rootView, savedInstanceState)
        return rootView
    }

    private fun init(rootView: View, savedInstanceState: Bundle?) {

        chatApi = Chat(context = requireActivity())
        store = PersistentStore(requireActivity())
        store.clearSpecificPref("pref_message")

        recyclerView = rootView.findViewById(R.id.messagesRecyclerView)
        swipeRefreshLayout = rootView.findViewById(R.id.messagesRefreshLayout)
        emptyChatContainer = rootView.findViewById(R.id.emptyChatsContainer)


        recyclerView.layoutManager = LinearLayoutManager(activity)

        if (savedInstanceState != null) {
            chatsAdapter = ChatsAdapter(
                stringToList(savedInstanceState.getString("chats")!!),
                requireActivity()
            )
            recyclerView.adapter = chatsAdapter
            chatsAdapter.notifyDataSetChanged()
            swipeRefreshLayout.isRefreshing = false
            emptyChatContainer.visibility = View.GONE
        } else {
            displayChats()
        }



        swipeRefreshLayout.setOnRefreshListener(this)
        emptyChatContainer.setOnClickListener(this)

    }

    private fun listToString(): String {
        return gson.toJson(chats)
    }

    private fun stringToList(json: String): ArrayList<Message> {
        val token = object : TypeToken<ArrayList<Message>>() {}.type
        return gson.fromJson(json, token)
    }

    private fun displayChats() {
        chatApi.getChats(listener = object : Chat.MessagesListener {
            override fun messages(list: ArrayList<Message>) {
                chats = list
                if (chats.size > 0) {
                    if (isAdded && requireActivity() != null){
                        chatsAdapter = ChatsAdapter(chats, requireActivity())
                        recyclerView.adapter = chatsAdapter
                        chatsAdapter.notifyDataSetChanged()
                        swipeRefreshLayout.isRefreshing = false
                        emptyChatContainer.visibility = View.GONE
                    }
                } else {
                    chatsAdapter =
                        ChatsAdapter(arrayListOf(Message("", "", "", "", "")), requireActivity())
                    recyclerView.adapter = chatsAdapter
                    chatsAdapter.notifyDataSetChanged()
                    emptyChatContainer.visibility = View.VISIBLE
                    swipeRefreshLayout.isRefreshing = false
                }

            }

            override fun error(err: String) {
                Toasty.error(requireActivity(), err, Toasty.LENGTH_SHORT).show()
                swipeRefreshLayout.isRefreshing = false
            }

        })
    }

    override fun onRefresh() {
        chats.clear()
        displayChats()
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.emptyChatsContainer -> {
                val i = Intent(requireActivity(), SearchActivity::class.java)
                requireActivity().startActivity(i)
            }
        }
    }
}