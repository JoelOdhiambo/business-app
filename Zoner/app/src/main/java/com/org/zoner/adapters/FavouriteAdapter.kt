package com.org.zoner.adapters

import android.content.Context
import android.content.Intent
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.PopupMenu
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.gson.Gson
import com.org.zoner.API.User
import com.org.zoner.R
import com.org.zoner.activities.BusinessDetailActivity
import com.org.zoner.activities.StatusActivity
import com.org.zoner.interfaces.RecyclerViewListener
import com.org.zoner.pojo.Business
import es.dmoral.toasty.Toasty


class FavouriteAdapter(
    context: Context,
    savedBusinesses: ArrayList<Business>,
    listener: RecyclerViewListener
) :
    RecyclerView.Adapter<FavouriteAdapter.FavouriteHolder>() {
    var ctx: Context = context
    var businesses: ArrayList<Business> = savedBusinesses
    var recyclerViewListener: RecyclerViewListener = listener
    val userApi: User = User(ctx)
    val gson = Gson()

    val TAG = "FavouriteAdapter"

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavouriteHolder {
        val view = LayoutInflater.from(ctx).inflate(R.layout.favourite_layout, parent, false)
        return FavouriteHolder(view, recyclerViewListener)
    }

    override fun getItemCount(): Int {
        return businesses.size
    }


    override fun onBindViewHolder(holder: FavouriteHolder, position: Int) {
        val businessObj: Business = businesses[position]
        with(businessObj) {

            holder.rootView.setOnClickListener {
                val intent = Intent(ctx, BusinessDetailActivity::class.java)
                intent.putExtra("businessId", businessId)
                intent.putExtra("name", businessName)
                intent.putExtra("description", businessDescription)
                intent.putExtra("created", businessCreated)
                intent.putExtra("message_token", message_token)
                intent.putExtra("status", gson.toJson(businessStatus))
                intent.putExtra("images", gson.toJson(businessImages))
                intent.putExtra(
                    "point", "${coordinates.latitude},${coordinates.longitude} "
                )
                ctx.startActivity(intent)
            }
            holder.name.text = businessName

            if (!TextUtils.isEmpty(tag))
                holder.tag.text = "#${tag}"
            else
                holder.tag.visibility = View.GONE

            if (businessImages.isNotEmpty()) {
                loadImage(businessImages[0], holder.businessImage)
            } else {
                holder.businessImage.setImageResource(R.drawable.placeholder_img)
            }

            if (businessStatus.isNotEmpty()) {
                holder.statusImage.visibility = View.VISIBLE
                holder.statusImage.setOnClickListener {
                    val intent = Intent(ctx, StatusActivity::class.java)
                    intent.putExtra("urls", gson.toJson(businessStatus))
                    intent.putExtra("businessId", gson.toJson(businessId))
                    ctx.startActivity(intent)
                }

            } else {
                holder.statusImage.visibility = View.GONE
            }

            holder.options.setOnClickListener { v ->
                val popUpMenu = PopupMenu(ctx, v)
                popUpMenu.menuInflater.inflate(R.menu.favourite_menu, popUpMenu.menu)
                popUpMenu.setOnMenuItemClickListener { item ->
                    when (item.itemId) {
                        R.id.removeFromFavourite -> {
                            userApi.deleteFavouriteBusiness(businessObj.businessId) { removed ->
                                if (removed) {
                                    Toasty.success(
                                        ctx,
                                        "Business removed from favourite",
                                        Toasty.LENGTH_SHORT
                                    ).show()
                                    businesses.removeAt(position)
                                    notifyItemRemoved(position)
                                } else {
                                    Toasty.error(
                                        ctx,
                                        "Something went wrong. Please try again later!",
                                        Toasty.LENGTH_SHORT
                                    ).show()
                                }
                            }
                        }
                    }
                    return@setOnMenuItemClickListener true
                }
                popUpMenu.show()
            }
        }

    }

    private fun loadImage(s: String, image: ImageView) {
        Glide.with(ctx)
            .applyDefaultRequestOptions(RequestOptions().placeholder(R.drawable.placeholder_img))
            .load(s)
            .into(image)
    }

    inner class FavouriteHolder(
        itemView: View,
        recyclerViewListener: RecyclerViewListener
    ) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        var viewType: Int = 0
        var businessImage: ImageView
        var statusImage: ImageView
        var name: TextView
        var options: ImageView
        var rootView: LinearLayout = itemView.findViewById(R.id.rootView)

        var tag: TextView = itemView.findViewById(R.id.tag)
        var hListener: RecyclerViewListener = recyclerViewListener

        init {
            itemView.setOnClickListener(this)
            tag.visibility = View.VISIBLE
            businessImage = itemView.findViewById(R.id.businessImage)
            statusImage = itemView.findViewById(R.id.statusImage)
            name = itemView.findViewById(R.id.name)
            options = itemView.findViewById(R.id.favouriteOptions)
        }

        override fun onClick(v: View?) {
            hListener.onClick(v, adapterPosition)
        }
    }
}