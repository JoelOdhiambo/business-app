package com.org.zoner.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckedTextView
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.org.zoner.R
import com.org.zoner.activities.MainActivity
import com.org.zoner.fragments.ProfileFragment
import com.org.zoner.interfaces.RecyclerViewListener
import com.org.zoner.pojo.Business
import com.org.zoner.utils.date.DateTimeStyle
import com.org.zoner.utils.date.DateTimeUtils
import es.dmoral.toasty.Toasty
import java.util.*

class MyBusinessesAdapter(
    context: Context,
    businesses: ArrayList<Business>,
    listener: RecyclerViewListener,
    isRecent: Boolean = false,
    val fromProfile: Boolean = false
) :
    RecyclerView.Adapter<MyBusinessesAdapter.MyBusinessesViewHolder>() {
    var ctx: Context = context
    var business: ArrayList<Business> = businesses
    var recyclerViewListener: RecyclerViewListener = listener
    val TAG = "MyBusinessAdapter"
    val recent = isRecent
    val businessApi = com.org.zoner.API.Business(ctx)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyBusinessesViewHolder {
        val view = LayoutInflater.from(ctx).inflate(R.layout.my_businesses, parent, false)
        return MyBusinessesViewHolder(view, recyclerViewListener)
    }

    override fun getItemCount(): Int {
        return business.size
    }

    override fun onBindViewHolder(holder: MyBusinessesViewHolder, position: Int) {
        val businessObj = business[position]
        try {
            val date: Date = DateTimeUtils.formatDate(businessObj.businessCreated.toLong())
            holder.businessDate.text = if (recent) DateTimeUtils.formatWithStyle(
                date,
                DateTimeStyle.MEDIUM
            ) else DateTimeUtils.getTimeAgo(ctx, date)
            holder.businessName.text = businessObj.businessName
        } catch (ex: NumberFormatException) {

        } catch (nullEx: NullPointerException) {

        }
        if (recent) holder.pastIcon.visibility = View.GONE
        if (fromProfile && businessObj.lastPaidAmount != null && businessObj.lastPaidAmount != "0")
            holder.isOnlineCheck.visibility = View.VISIBLE
        holder.isOnlineCheck.setOnClickListener {
            if (!((ctx as MainActivity).supportFragmentManager.findFragmentByTag(4.toString()) as ProfileFragment)
                    .checkIfBusinessIsExpired(
                        businessObj.businessId
                    )
            ) {

                if (!holder.isOnlineCheck.isChecked) {
                    businessApi.businessOnline(businessObj.businessId, true) {
                        if (it) {
                            holder.isOnlineCheck.checkMarkDrawable =
                                ctx.resources.getDrawable(R.drawable.ic_check_circle_orange_24dp)
                            holder.isOnlineCheck.isChecked = true
                            holder.isOnlineCheck.text = ctx.resources.getText(R.string.online)
                        }
                    }
                } else {
                    holder.isOnlineCheck.isChecked = false
                    businessApi.businessOnline(businessObj.businessId, false) {
                        if (it) {
                            holder.isOnlineCheck.checkMarkDrawable =
                                ctx.resources.getDrawable(R.drawable.ic_radio_button_unchecked_orange_24dp)
                            holder.isOnlineCheck.text = ctx.resources.getText(R.string.offline)
                        }
                    }
                }
            } else {
                Toasty.info(
                    ctx,
                    "Subscription is expired. Please renew and try again",
                    Toasty.LENGTH_SHORT
                ).show()
            }

        }
    }

    class MyBusinessesViewHolder(
        itemView: View,
        recyclerViewListener: RecyclerViewListener
    ) : RecyclerView.ViewHolder(itemView), View.OnClickListener, View.OnLongClickListener {
        init {
            itemView.setOnClickListener(this)
            itemView.setOnLongClickListener(this)
        }

        var businessName: TextView = itemView.findViewById(R.id.businessName)
        var businessDate: TextView = itemView.findViewById(R.id.dateCreated)
        var pastIcon: ImageView = itemView.findViewById(R.id.pastIcon)
        var isOnlineCheck: CheckedTextView = itemView.findViewById(R.id.isOnline)
        var hListener: RecyclerViewListener = recyclerViewListener
        val TAG = "MyBusinessHolder"

        override fun onClick(v: View?) {
            hListener.onClick(v, adapterPosition)
        }

        override fun onLongClick(v: View?): Boolean {
            hListener.onLongClick(v, adapterPosition)
            return true
        }
    }
}