package com.org.zoner.offline

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(tableName = "owner", indices = [Index(value = arrayOf("businessId"), unique = true)])
data class Owner(
    @PrimaryKey
    var userId: String = "",
    var businessId: String = ""
)