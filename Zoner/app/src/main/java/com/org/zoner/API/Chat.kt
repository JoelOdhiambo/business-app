package com.org.zoner.API

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import com.google.firebase.firestore.*
import com.org.zoner.activities.ChatActivity
import com.org.zoner.activities.MainActivity
import com.org.zoner.interfaces.TrueFalseListener
import com.org.zoner.pojo.Message
import timber.log.Timber

class Chat(context: Context, businessId: String? = "") {
    var ctx: Context = context
    private val chatCollection: CollectionReference =
        FirebaseFirestore.getInstance().collection("chat")
    private val businessCollection: CollectionReference =
        FirebaseFirestore.getInstance().collection("business")
    private val userCollection: CollectionReference =
        FirebaseFirestore.getInstance().collection("user")

    private val docIdPreferences: SharedPreferences =
        ctx.getSharedPreferences("pref_user_doc_id", Context.MODE_PRIVATE)
    var userDocumentId = docIdPreferences.getString("id", "null")

    val store = PersistentStore(context)

    private val keyGeneratedDocumentReference: DocumentReference by lazy {
        chatCollection.document()
    }

    private val bid by lazy {
        businessId
    }

    var businessApi = Business(ctx)
    var userApi = User(ctx)

    val TAG = "ChatApi"
    private val MESSAGES_TO_LOAD = 10
    private var LAST_KEY = ""
    private var PREVIOUS_KEY = ""

    val messages = arrayListOf<Message>()


    companion object {
        var ITEM_COUNT = 0
    }

    fun getChats(listener: MessagesListener) {
        val messages = arrayListOf<Message>()
        if (MainActivity.businessChats.isNotEmpty()) {
            Log.d(TAG, "Business chats in main activity is not empty")
            for ((key, value) in MainActivity.businessChats) {
                businessCollection.document(key)
                    .get()
                    .addOnSuccessListener { documentSnapshot ->
                        if (documentSnapshot.exists()) {
                            Log.d(TAG, "Chat, get chats document exists for business")
                            getLastMessage(
                                0,
                                key,
                                value,
                                documentSnapshot,
                                object : MessagesListener {
                                    override fun messages(list: ArrayList<Message>) {
                                        listener.messages(list)
                                    }

                                    override fun error(err: String) {
                                        listener.error(err)
                                    }

                                })
                        } else {
                            Log.d(
                                TAG,
                                "Chat, get chats document does not exist for business going to users"
                            )
                            userCollection.document(key)
                                .get().addOnSuccessListener { userSnapshot ->
                                    getLastMessage(
                                        1,
                                        key,
                                        value,
                                        userSnapshot,
                                        object : MessagesListener {
                                            override fun messages(list: ArrayList<Message>) {
                                                listener.messages(list)
                                            }

                                            override fun error(err: String) {
                                                listener.error(err)
                                            }

                                        })
                                }
                        }


                    }.addOnFailureListener {
                        listener.error(it.message.toString())
                    }
            }
        } else {
            Log.d(TAG, "Business chats in main activity is  empty")
            listener.messages(messages)
        }

    }

    private fun getLastMessage(
        type: Int,
        key: String,
        value: String,
        documentSnapshot: DocumentSnapshot?,
        listener: MessagesListener
    ) {
        var displayName = ""
        when (type) {
            0 -> displayName = documentSnapshot!!["n"].toString()
            1 -> displayName = documentSnapshot!!["p"].toString()
        }

        Log.d(TAG, "Getting Chat with document value $key")
        chatCollection.document(value).collection("messages")
            .orderBy("ts", Query.Direction.ASCENDING)
            .limitToLast(1).get().addOnSuccessListener { querySnapshot ->
                if (querySnapshot.isEmpty) {
                    listener.error("Something went wrong...")
                } else {
                    for ((pos, snapshot) in querySnapshot.withIndex()) {
                        if (snapshot.exists() && snapshot.contains("m") && snapshot.contains("ts")) {
                            if (pos == 1)
                                break
                            messages.add(
                                Message(
                                    key,
                                    snapshot["m"].toString(),
                                    "",
                                    displayName,
                                    snapshot!!["ts"].toString(),
                                    messageToken = documentSnapshot!!["t"].toString()
                                )
                            )
                            listener.messages(messages)
                            Log.d(TAG, documentSnapshot["n"].toString())
                        }

                    }
                }

            }.addOnFailureListener {
                listener.error(it.message.toString())
            }

    }

    fun sendMessage(details: Map<String, String>, chatId: String, callback: (Boolean) -> Unit) {
        val chatDocumentReference: DocumentReference
        if (!MainActivity.businessChats.containsValue(chatId)) {
            Log.d(TAG, "Business chats in main activity lacks chat id")
            val ref = keyGeneratedDocumentReference.id
            chatDocumentReference = chatCollection
                .document(ref)
            userApi.addChatId(mutableMapOf("b" to bid!!, "c" to ref), object : TrueFalseListener {
                override fun result(result: Boolean, message: String) {
                    if (result) {
                        businessApi.addChatId(
                            bid!!,
                            mutableMapOf("u" to userDocumentId, "c" to ref),
                            object : TrueFalseListener {
                                override fun result(result: Boolean, message: String) {
                                    ChatActivity.chatId.value = ref
                                    Log.d(TAG, "Added to userchat in business")
                                }
                            })
                    }
                }
            })
        } else {
            Log.d(TAG, "Business chats in main activity lacks chat id")
            chatDocumentReference = chatCollection
                .document(chatId)
        }

        chatDocumentReference
            .collection("messages")
            .add(details)
            .addOnSuccessListener { documentReference ->
                callback(true)
                Log.d(TAG, documentReference.id)
            }.addOnFailureListener {
                callback(false)
            }
    }

    fun loadMessages(chatId: String, loadMore: Boolean = false, listener: MessagesListener) {

        val loadMessagesRef: Query

        if (!loadMore) {
            loadMessagesRef = chatCollection
                .document(chatId)
                .collection("messages")
                .orderBy("ts", Query.Direction.ASCENDING)
                .limitToLast(MESSAGES_TO_LOAD.toLong())
            Log.d(TAG, "Chat Api not loading more")
        } else {
            loadMessagesRef = chatCollection
                .document(chatId)
                .collection("messages")
                .orderBy("ts", Query.Direction.ASCENDING)
                .endAt(LAST_KEY)
                .limitToLast(MESSAGES_TO_LOAD.toLong())
            Log.d(TAG, "Chat Api loading more with key \t $LAST_KEY")
        }

        loadMessagesRef.addSnapshotListener(ctx as Activity) { querySnapshot, firebaseFirestoreException ->
            firebaseFirestoreException?.let {
                listener.error(it.message.toString())
            }
            if (!querySnapshot!!.isEmpty) {
                Log.d(TAG, "Chat snapshot is not empty")
                for ((pos, snapshot) in querySnapshot.documentChanges.withIndex()) {
                    Log.d(TAG, snapshot.document.id)
                    if (snapshot.document.exists() && snapshot.type == DocumentChange.Type.ADDED) {
                        if (ITEM_COUNT == 0) {
                            LAST_KEY = snapshot.document["ts"].toString()
                            Log.d(TAG, "Chat Api last key \t $LAST_KEY")
                            if (!loadMore) {
                                PREVIOUS_KEY = LAST_KEY
                            }
                        }
                        if (loadMore) {
                            Log.d(TAG, "Loading more, snapshot is not empty")
                            if (PREVIOUS_KEY != snapshot.document["ts"].toString()) {
                                messages.add(
                                    ITEM_COUNT++,
                                    Message(
                                        snapshot.document.id,
                                        snapshot.document["m"].toString(),
                                        "",
                                        "",
                                        snapshot.document["ts"].toString(),
                                        snapshot.document["b"].toString()
                                    )
                                )

                            } else {
                                PREVIOUS_KEY = LAST_KEY
                            }

                            Log.d(TAG, snapshot.document["m"].toString())

                        } else {
                            Log.d(TAG, "Not loading more, snapshot is not empty")
                            Timber.tag(TAG).d(ITEM_COUNT.toString())
                            messages.add(
                                ITEM_COUNT++,
                                Message(
                                    snapshot.document.id,
                                    snapshot.document["m"].toString(),
                                    "",
                                    "",
                                    snapshot.document["ts"].toString(),
                                    snapshot.document["b"].toString()
                                )
                            )
                            Log.d(TAG, snapshot.document["m"].toString())
                        }


                        if (pos == querySnapshot.documentChanges.size - 1) {
                            listener.messages(messages)
                        }
                    } else {
                        Log.d(TAG, "Type not added")
                    }
                }

            } else {
                Log.d(TAG, "Query snapshot is empty")
                listener.messages(messages)
            }
        }
    }

    fun clearAllChats(callback: (Boolean) -> Unit) {
        val userChatCollection = userCollection
            .document(store.getUserDocumentId()!!)
            .collection("businesschat")
        userChatCollection.get()
            .addOnSuccessListener { querySnapshot ->
                if (!querySnapshot.isEmpty) {
                    for (snapshot in querySnapshot.documents) {
                        if (snapshot.exists() && snapshot.contains("b") && snapshot.contains("c")) {
                            removeChatDocuments(snapshot.get("c") as String) { deleted ->
                                if (deleted) {
                                    val businessQuery =
                                        businessCollection.document(snapshot.get("b") as String)
                                            .collection("userchat")
                                    businessQuery.whereEqualTo("c", snapshot.get("c"))
                                        .limit(1).get()
                                        .addOnSuccessListener { businessQuerySnapshot ->
                                            if (!businessQuerySnapshot.isEmpty) {
                                                for (businessSnapshot in businessQuerySnapshot.documents) {
                                                    if (businessSnapshot.exists() && businessSnapshot.contains(
                                                            "c"
                                                        ) && businessSnapshot.contains("u")
                                                    ) {
                                                        businessQuery.document(businessSnapshot.id)
                                                            .delete().addOnSuccessListener {
                                                                deleteChatRecordUserChatCollection(
                                                                    userChatCollection,
                                                                    snapshot.id
                                                                ) { done ->
                                                                    callback(done)
                                                                }
                                                            }
                                                    }
                                                }
                                            } else {
                                                deleteChatRecordUserChatCollection(
                                                    userChatCollection,
                                                    snapshot.id
                                                ) { done ->
                                                    callback(done)
                                                }
                                            }
                                        }
                                }
                            }
                        }
                    }
                } else {
                    callback(true)
                }
            }.addOnFailureListener {
                callback(false)
            }

    }

    private fun deleteChatRecordUserChatCollection(
        userChatCollection: CollectionReference,
        documentId: String,
        callback: (Boolean) -> Unit
    ) {
        userChatCollection.document(documentId)
            .delete()
            .addOnSuccessListener {
                callback(true)
            }.addOnFailureListener {
                callback(false)
            }
    }

    private fun removeChatDocuments(chatDocumentReference: String, callback: (Boolean) -> Unit) {
        val messagesCollection =
            chatCollection.document(chatDocumentReference).collection("messages")
        messagesCollection.get().addOnSuccessListener { querySnapshot ->
            if (!querySnapshot.isEmpty) {
                for ((pos, snapshot) in querySnapshot.withIndex()) {
                    if (snapshot.exists()) {
                        messagesCollection.document(snapshot.id).delete().addOnSuccessListener {
                            if (pos == querySnapshot.documents.size - 1) {
                                callback(true)
                            }
                        }
                    }
                }

            } else {
                callback(true)
            }
        }
            .addOnFailureListener {
                callback(false)
            }

    }

    interface MessagesListener {
        fun messages(list: ArrayList<Message>)
        fun error(err: String)
    }

}