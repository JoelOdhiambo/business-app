package com.org.zoner.adapters

import android.content.Context
import android.content.Intent
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.gson.Gson
import com.org.zoner.API.PersistentStore
import com.org.zoner.API.User
import com.org.zoner.R
import com.org.zoner.activities.BusinessDetailActivity
import com.org.zoner.activities.ChatActivity
import com.org.zoner.activities.StatusActivity
import com.org.zoner.interfaces.OnBottomReached
import com.org.zoner.interfaces.RecyclerViewListener
import com.org.zoner.pojo.Business
import com.org.zoner.utils.recyclerview.BusinessDiffUtil


class ListBusinessAdapter(
    context: Context,
    businesses: ArrayList<Business>, listener: RecyclerViewListener
) :
    RecyclerView.Adapter<ListBusinessAdapter.FavouriteHolder>() {
    var ctx: Context = context
    var businesses: ArrayList<Business> = businesses
    var listener: RecyclerViewListener = listener
    val TAG = "ListBusinessAdapter"
    lateinit var bottomReachedListener: OnBottomReached
    val userApi: User = User(ctx)
    val gson = Gson()
    val store = PersistentStore(ctx)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavouriteHolder {
        val view = LayoutInflater.from(ctx).inflate(R.layout.business_layout, parent, false)
        return FavouriteHolder(view, listener)
    }

    override fun getItemCount(): Int {
        return businesses.size
    }

    fun setData(newBusinessList: ArrayList<Business>) {
        val diffUtil = DiffUtil.calculateDiff(BusinessDiffUtil(businesses, newBusinessList))
        businesses.clear()
        businesses.addAll(newBusinessList)
        diffUtil.dispatchUpdatesTo(this)
    }


    fun setOnBottomReachedListener(bottomReached: OnBottomReached) {
        bottomReachedListener = bottomReached
    }

    override fun onBindViewHolder(holder: FavouriteHolder, position: Int) {
        val business: Business = businesses[position]
        if (position == businesses.size - 1)
            bottomReachedListener.bottomReached(position)


        with(business) {
            holder.name.text = businessName
            holder.description.text = businessDescription

            holder.rootView.setOnClickListener {

                val intent = Intent(ctx, BusinessDetailActivity::class.java)
                intent.putExtra("businessId", businessId)
                intent.putExtra("name", businessName)
                intent.putExtra("description", businessDescription)
                intent.putExtra("created", businessCreated)
                intent.putExtra("message_token", message_token)
                intent.putExtra("status", gson.toJson(businessStatus))
                intent.putExtra("images", gson.toJson(businessImages))
                intent.putExtra(
                    "point", "${coordinates.latitude},${coordinates.longitude} "
                )
                ctx.startActivity(intent)
            }

            if (businessStatus.isNotEmpty()) {
                holder.statusImage.visibility = View.VISIBLE
                Glide.with(ctx)
                    .applyDefaultRequestOptions(RequestOptions().placeholder(R.drawable.placeholder_img))
                    .load(businessStatus[0])
                    .into(holder.businessImage)

                holder.statusImage.setOnClickListener {
                    val intent = Intent(ctx, StatusActivity::class.java)
                    intent.putExtra("urls", gson.toJson(businessStatus))
                    intent.putExtra("businessId", gson.toJson(businessId))
                    ctx.startActivity(intent)
                }
            } else {
                holder.statusImage.visibility = View.GONE
            }
            if (!TextUtils.isEmpty(tag)) {
                holder.tagContainer.visibility = View.VISIBLE
                holder.tag.text = "#${tag}"
            }
            if (businessImages.isNotEmpty()) {
                Glide.with(ctx)
                    .applyDefaultRequestOptions(RequestOptions().placeholder(R.drawable.placeholder_img))
                    .load(businessImages[0])
                    .into(holder.businessImage)
            } else {
                holder.businessImage.setImageResource(R.drawable.placeholder_img)
            }

            holder.options.setOnClickListener { v ->
                val popUpMenu = PopupMenu(ctx, v)
                popUpMenu.menuInflater.inflate(R.menu.business_options_menu, popUpMenu.menu)
                if (store.getBusinessesIds().contains(businessId)) {
                    popUpMenu.menu.findItem(R.id.chat).isVisible = false
                }
                popUpMenu.setOnMenuItemClickListener { item ->
                    when (item.itemId) {
                        R.id.favourite -> {
                            userApi.addToFavourites(businessId) { done ->
                                if (done)
                                    showToast("Business added to favourite")
                                else
                                    showToast("Something went wrong please try again later")
                            }
                        }
                        R.id.chat -> {
                            if (message_token != null && !TextUtils.isEmpty(message_token) && !TextUtils.isEmpty(
                                    businessId
                                )
                            ) {
                                val intent = Intent(ctx, ChatActivity::class.java)
                                intent.putExtra("businessId", businessId)
                                intent.putExtra("messaging_token", message_token)
                                intent.putExtra("name", businessName)
                                ctx.startActivity(intent)
                            } else {
                                showToast("This business can not receive messages. Try again later?")
                            }


                        }
                    }
                    return@setOnMenuItemClickListener true
                }
                popUpMenu.show()
            }
        }
    }

    private fun showToast(s: String) {
        Toast.makeText(ctx, s, Toast.LENGTH_SHORT).show()
    }

    inner class FavouriteHolder(
        itemView: View,
        listener: RecyclerViewListener
    ) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        var businessImage: ImageView = itemView.findViewById(R.id.businessImage)
        var name: TextView = itemView.findViewById(R.id.name)
        var description: TextView = itemView.findViewById(R.id.description)
        var options: ImageView = itemView.findViewById(R.id.favouriteOptions)
        var tagContainer: RelativeLayout = itemView.findViewById(R.id.tagContainer)
        var tag: TextView = itemView.findViewById(R.id.tag)
        var statusImage: ImageView = itemView.findViewById(R.id.statusImage)
        var rootView: LinearLayout = itemView.findViewById(R.id.rootLayout)
        var hListener: RecyclerViewListener = listener

        init {
            tagContainer.visibility = View.VISIBLE
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            hListener.onClick(v, adapterPosition)
        }
    }
}