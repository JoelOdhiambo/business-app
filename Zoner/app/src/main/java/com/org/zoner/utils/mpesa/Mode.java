package com.org.zoner.utils.mpesa;

public enum Mode {
    SANDBOX,
    PRODUCTION
}
