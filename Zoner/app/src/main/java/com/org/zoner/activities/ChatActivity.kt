package com.org.zoner.activities

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.org.zoner.API.Chat
import com.org.zoner.API.PersistentStore
import com.org.zoner.R
import com.org.zoner.adapters.MessagesAdapter
import com.org.zoner.pojo.Message
import com.org.zoner.utils.notification.SendNotification
import es.dmoral.toasty.Toasty

class ChatActivity : AppCompatActivity(), SwipeRefreshLayout.OnRefreshListener,
    View.OnClickListener {

    lateinit var toolbar: Toolbar
    lateinit var recyclerView: RecyclerView
    lateinit var swipeRefreshLayout: SwipeRefreshLayout
    lateinit var toolBarTitle: TextView
    lateinit var sendMessage: ImageView
    lateinit var messageEditText: EditText
    lateinit var emptyChatText: TextView

    lateinit var adapter: MessagesAdapter
    lateinit var layoutManager: LinearLayoutManager
    var messageList = arrayListOf<Message>()

    lateinit var chatApi: Chat
    var businessId: String? = null
    lateinit var notificationName: String


    lateinit var sharedPreferences: SharedPreferences

    var userDocumentId: String? = null
    var messageToken: String? = null
    var name: String? = null
    var isInitialized = false
    lateinit var store: PersistentStore

    val TAG = "ChatActivity"

    companion object {
        var chatId = MutableLiveData<String>().default("")
        private fun <T : Any?> MutableLiveData<T>.default(initialValue: T) =
            apply { setValue(initialValue) }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)
        init()
    }


    private fun init() {
        sharedPreferences = getSharedPreferences("pref_user_doc_id", Context.MODE_PRIVATE)
        userDocumentId = sharedPreferences.getString("id", "null")

        businessId = intent.getStringExtra("businessId")
        messageToken = intent.getStringExtra("messaging_token")

        store = PersistentStore(this)

        name = intent.getStringExtra("name")

        notificationName = if (store.getBusinessesIds().contains(businessId!!)) {
            name!!
        } else {
            store.getUserPhoneNumber()!!
        }

        chatApi = Chat(this@ChatActivity, businessId)

        if (messageToken == null || messageToken.equals(""))
            Toasty.error(
                this@ChatActivity,
                "Can't send notifications to this person / business.",
                Toasty.LENGTH_LONG
            ).show()

        toolbar = findViewById(R.id.toolBar)
        setSupportActionBar(toolbar)
        supportActionBar!!.title = ""
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp)


        recyclerView = findViewById(R.id.recyclerView)
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout)
        toolBarTitle = findViewById(R.id.toolBarTitle)

        messageEditText = findViewById(R.id.message)
        sendMessage = findViewById(R.id.sendMessage)
        emptyChatText = findViewById(R.id.emptyChatText)
        adapter = MessagesAdapter(this@ChatActivity, messageList)
        recyclerView.adapter = adapter

        layoutManager = LinearLayoutManager(this)
        layoutManager.stackFromEnd = true
        recyclerView.itemAnimator = DefaultItemAnimator()
        recyclerView.layoutManager = layoutManager
        toolBarTitle.text = name



        if (MainActivity.businessChats.isNotEmpty()) {
            chatId.value = if (MainActivity.businessChats.containsKey(businessId!!)) {
                MainActivity.businessChats.getValue(businessId!!).toString()
            } else {
                "empty"
            }
        } else {
            chatId.value = "empty"

        }

        chatId.observe(this@ChatActivity, Observer {
            getMessages(false)
        })

        sendMessage.setOnClickListener(this)
        swipeRefreshLayout.setOnRefreshListener(this)

    }

    override fun onStop() {
        super.onStop()
        Chat.ITEM_COUNT = 0
    }

    override fun onRefresh() {
        Chat.ITEM_COUNT = 0
        getMessages(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun getMessages(loadMore: Boolean) {
        if (chatId.value != "empty") {
            chatApi.loadMessages(
                chatId = chatId.value!!,
                loadMore = loadMore,
                listener = object : Chat.MessagesListener {
                    override fun messages(list: ArrayList<Message>) {
                        Log.d(TAG, list.toString())

                        if (list.size > 0) {

                            emptyChatText.visibility = View.GONE
                            messageList = list
                            if (messageList.isEmpty()) {
                                adapter.initData(messageList)
                                isInitialized = true

                            } else {
                                adapter.setData(messageList)
                            }


                            layoutManager.scrollToPositionWithOffset(10, 0)

                            if (loadMore)
                                swipeRefreshLayout.isRefreshing = false

                            recyclerView.scrollToPosition(adapter.itemCount - 1);

                        } else {
                            swipeRefreshLayout.isRefreshing = false
                        }

                    }

                    override fun error(err: String) {
                        swipeRefreshLayout.isRefreshing = false
                        Toasty.error(this@ChatActivity, err, Toasty.LENGTH_SHORT).show()
                    }

                })
        } else {
            emptyChatText.visibility = View.VISIBLE
        }

    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.sendMessage -> {
                chatApi.sendMessage(
                    mutableMapOf(
                        "b" to userDocumentId!!,
                        "m" to messageEditText.text.toString(),
                        "ts" to System.currentTimeMillis().toString()
                    ), chatId.value!!, callback = { sent ->
                        if (sent) {
                            val message = messageEditText.text.toString()
                            messageEditText.setText("")
                            if (messageToken != null) {
                                SendNotification.push(payload = hashMapOf<String, Any>(
                                    "businessId" to businessId!!,
                                    "activity" to 0,
                                    "fragment" to -1,
                                    "token" to messageToken!!,
                                    "message" to message,
                                    "header" to notificationName
                                ), callback = { _ ->

                                })
                            }
                        }
                    }
                )
            }
        }
    }


}
