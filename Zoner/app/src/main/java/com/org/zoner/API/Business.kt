package com.org.zoner.API

import android.app.ProgressDialog
import android.content.Context
import android.content.SharedPreferences
import android.location.Location
import android.location.LocationManager
import android.util.Log
import com.ckdroid.geofirequery.GeoQuery
import com.ckdroid.geofirequery.model.Distance
import com.ckdroid.geofirequery.setLocation
import com.ckdroid.geofirequery.utils.BoundingBoxUtils
import com.google.firebase.firestore.*
import com.org.zoner.interfaces.MyBusinessesListener
import com.org.zoner.interfaces.TrueFalseListener
import com.org.zoner.pojo.Business
import com.org.zoner.utils.image.ImageUpload
import es.dmoral.toasty.Toasty


@Suppress("DEPRECATION")
class Business(context: Context) {
    val ctx: Context = context

    private val businessCollection: CollectionReference =
        FirebaseFirestore.getInstance().collection("business")
    private val userCollection: CollectionReference =
        FirebaseFirestore.getInstance().collection("user")


    private val searchPreferences: SharedPreferences =
        ctx.getSharedPreferences("pref_search", Context.MODE_PRIVATE)

    var searchMode = searchPreferences.getString("mode", "n")

    private val store = PersistentStore(ctx)

    private val TAG = "BusinessApi"

    private val ITEMS_LIMIT = 3

    private val imageUpload = ImageUpload(context)


    fun addBusiness(
        details: HashMap<String, Any>,
        images: ArrayList<String>,
        placeCoordinates: String?,
        callback: (Pair<Boolean, String?>) -> Unit
    ) {
        val progressDialog = ProgressDialog(ctx)
        progressDialog.setTitle("Adding business to collection")
        progressDialog.setMessage("A moment.. add the business...")
        progressDialog.setCancelable(false)
        progressDialog.setCanceledOnTouchOutside(false)
        progressDialog.show()


        businessCollection.add(details)
            .addOnSuccessListener { documentReference ->
                userCollection.document(store.getUserDocumentId()!!)
                    .collection("b")
                    .add(
                        hashMapOf(
                            "i" to documentReference.id
                        )
                    )
                    .addOnSuccessListener { _ ->
                        if (placeCoordinates != null && placeCoordinates != "") {
                            val coordinates = placeCoordinates.split(",")
                            businessCollection.document(documentReference.id).setLocation(
                                coordinates[1].toDouble(),
                                coordinates[0].toDouble()
                            )
                                .addOnSuccessListener {
                                    if (images.isNotEmpty()) {
                                        uploadBusinessImages(
                                            images,
                                            documentReference.id
                                        ) { uploaded ->
                                            if (uploaded) {
                                                progressDialog.dismiss()
                                                callback(Pair(true, documentReference.id))
                                            } else {
                                                progressDialog.dismiss()
                                                Pair(
                                                    false,
                                                    "An error occurred when uploading images"
                                                )
                                            }
                                        }
                                    } else {
                                        progressDialog.dismiss()
                                        callback(Pair(true, documentReference.id))
                                    }

                                }.addOnFailureListener {
                                    progressDialog.dismiss()
                                    callback(
                                        Pair(
                                            false,
                                            "An error occurred when inserting geolocation"
                                        )
                                    )
                                }
                        } else {
                            progressDialog.dismiss()
                            callback(Pair(true, documentReference.id))
                        }

                    }.addOnFailureListener {
                        progressDialog.dismiss()
                        callback(Pair(false, it.message.toString()))
                    }
            }.addOnFailureListener { exception ->
                progressDialog.dismiss()
                callback(Pair(false, exception.message.toString()))
            }
    }


    fun uploadBusinessImages(
        images: ArrayList<String>,
        businessId: String,
        callback: (Boolean) -> Unit
    ) {
        imageUpload.uploadImage(images, businessId, "r") { result ->
            if (result.second == images.size - 1) {
                callback(true)
            }
        }
    }

    fun businessOnline(businessId: String, isOnline: Boolean, callback: (Boolean) -> Unit) {
        businessCollection.document(businessId).update(mapOf("o" to isOnline))
            .addOnSuccessListener {
                callback(true)
            }.addOnFailureListener {
                callback(false)
            }
    }

    fun getBusinessIds(collectionPath: String, callback: (ArrayList<String>) -> Unit) {
        val businessIds = arrayListOf<String>()
        userCollection
            .document(store.getUserDocumentId()!!)
            .collection(collectionPath)
            .get().addOnSuccessListener { querySnapshot ->
                if (!querySnapshot.isEmpty) {
                    for ((pos, document) in querySnapshot.documents.withIndex()) {
                        if (document.exists() && document.contains("i")) {
                            businessIds.add(document["i"] as String)
                            if (pos == querySnapshot.documents.size -1)
                                callback(businessIds)
                        }
                    }
                }else{
                    callback(businessIds)
                }
            }.addOnFailureListener {
                callback(businessIds)
            }
    }


    fun getBusinesses(collectionPath: String, callback: (ArrayList<Business>) -> Unit) {
        val myBusinesses = arrayListOf<Business>()
        userCollection
            .document(store.getUserDocumentId()!!)
            .collection(collectionPath)
            .get()
            .addOnSuccessListener { querySnapshot ->
                if (!querySnapshot.isEmpty) {
                    for ((_, document) in querySnapshot.documents.withIndex()) {
                        if (document.exists() && document.contains("i")) {
                            businessCollection.document(document["i"].toString()).get()
                                .addOnSuccessListener { snapshot ->
                                    if (snapshot.exists()) {
                                        var website = "You don't have a website."
                                        if (snapshot["w"] != null)
                                            website = snapshot["w"] as String
                                        try {
                                            if (snapshot.contains("suspended") && snapshot.contains(
                                                    "d"
                                                ) && snapshot.contains(
                                                    "ts"
                                                ) && snapshot.contains("n") && snapshot.contains("geoLocation")
                                            ) {
                                                if (!(snapshot["suspended"] as Boolean)) {

                                                    myBusinesses.add(
                                                        Business(
                                                            document["i"].toString(),
                                                            firstUpperCase(snapshot["n"].toString()),
                                                            snapshot["d"].toString(),
                                                            snapshot["ts"].toString(),
                                                            message_token = snapshot["t"] as String,
                                                            coordinates = snapshot["geoLocation"] as GeoPoint,
                                                            website = website,
                                                            tag = snapshot["c"] as String,
                                                            businessStatus = if (snapshot["s"] != null) snapshot["s"] as ArrayList<String> else arrayListOf(),
                                                            businessImages = if (snapshot["r"] != null) snapshot["r"] as ArrayList<String> else arrayListOf(),
                                                            lastPaidAmount = snapshot["p"] as String
                                                        )
                                                    )
                                                }
                                            }

                                        } catch (e: TypeCastException) {

                                        } catch (e: NullPointerException) {
                                        }
                                    } else {
                                        userCollection.document(store.getUserDocumentId()!!)
                                            .collection("b")
                                            .document(document.id).delete()
                                    }
                                    callback(myBusinesses)
                                }.addOnFailureListener {
                                    Toasty.error(
                                        ctx,
                                        "An unexpected error occurred",
                                        Toasty.LENGTH_SHORT
                                    ).show()
                                }
                        }
                    }
                } else {
                    callback(myBusinesses)
                }
            }.addOnFailureListener {
                Toasty.error(ctx, "An unexpected error occurred", Toasty.LENGTH_SHORT).show()
            }
    }

    var LAST_KEY = ""

    fun getBusinessByCategory(
        category: String,
        loadMore: Boolean,
        callback: (ArrayList<Business>) -> Unit
    ) {
        val businessList = arrayListOf<Business>()
        val query: Query = if (!loadMore) {
            businessCollection.whereEqualTo("c", category)
                .orderBy("ts", Query.Direction.DESCENDING)
                .limit(ITEMS_LIMIT.toLong())
        } else {
            businessCollection.whereEqualTo("c", category)
                .orderBy("ts", Query.Direction.DESCENDING)
                .startAfter(LAST_KEY)
                .limit(ITEMS_LIMIT.toLong())
        }

        query.get().addOnSuccessListener { querySnapshot ->
            if (!querySnapshot.isEmpty) {
                for ((i, snapshot) in querySnapshot.withIndex()) {
                    if (snapshot.exists() && snapshot.contains("n") && snapshot.contains(
                            "d"
                        ) && snapshot.contains("geoLocation") && snapshot.contains("ts")
                    ) {
                        var website = "No website"
                        if (snapshot["w"] != null)
                            website = snapshot["w"] as String
                        businessList.add(
                            Business(
                                snapshot.id,
                                firstUpperCase(snapshot["n"].toString()),
                                snapshot["d"].toString(),
                                snapshot["ts"].toString(),
                                coordinates = snapshot["geoLocation"] as GeoPoint,
                                website = website,
                                message_token = snapshot["t"] as String,
                                tag = snapshot["c"] as String,
                                businessStatus = if (snapshot["s"] != null) snapshot["s"] as ArrayList<String> else arrayListOf(),
                                businessImages = if (snapshot["r"] != null) snapshot["r"] as ArrayList<String> else arrayListOf()
                            )
                        )
                    }

                    if (i == querySnapshot.size() - 1) {
                        LAST_KEY = snapshot.get("ts") as String
                        callback(businessList)

                    }
                }
            } else {
                callback(businessList)
            }
        }.addOnFailureListener {
            callback(businessList)
        }
    }

    fun addPaymentDetailsToUserDocument(
        details: Map<String, Any>,
        callback: (Boolean) -> Unit
    ) {
        userCollection.document(store.getUserDocumentId()!!)
            .update(
                details
            ).addOnSuccessListener {
                callback(true)
            }.addOnFailureListener {
                callback(false)
            }
    }

    fun getChatsToMe(callback: (MutableMap<String, String>) -> Unit) {
        val businessChats = mutableMapOf<String, String>()
        businessCollection.whereEqualTo("b", store.getUserDocumentId())
            .get()
            .addOnSuccessListener { querySnapshot ->
                for (snapshot in querySnapshot.documents) {
                    businessCollection.document(snapshot.id)
                        .collection("userchat")
                        .orderBy("u")
                        .addSnapshotListener { it, exception ->
                            exception?.let {
                                Toasty.error(ctx, it.message.toString(), Toasty.LENGTH_SHORT).show()
                            }
                            if (!it!!.isEmpty) {
                                for ((_, document) in it.documents.withIndex()) {
                                    // if (index == it.documents.size - 1)
                                    businessChats[document["u"].toString()] =
                                        document["c"].toString()
                                    callback(businessChats)
                                }
                            }
                        }
                }
            }.addOnFailureListener {
                Toasty.error(ctx, it.message.toString(), Toasty.LENGTH_SHORT).show()
            }
    }

    fun searchBusiness(query: String?, businessesListener: MyBusinessesListener) {
        val myBusinesses = arrayListOf<Business>()
        businessCollection.orderBy(searchMode!!).startAt(query!!.trim())
            .endAt(query + "\uf8ff")
            .get()
            .addOnSuccessListener { querySnapshot ->
                if (!querySnapshot.isEmpty) {
                    for (snapshot in querySnapshot) {
                        if (snapshot.exists()) {
                            if (snapshot.contains("n") && snapshot.contains("d") && snapshot.contains(
                                    "ts"
                                ) && snapshot.contains("geoLocation")
                            ) {
                                myBusinesses.add(
                                    Business(
                                        snapshot.id,
                                        firstUpperCase(snapshot["n"].toString()),
                                        snapshot["d"].toString(),
                                        snapshot["ts"].toString(),
                                        coordinates = snapshot["geoLocation"] as GeoPoint,
                                        message_token = snapshot["t"].toString(),
                                        tag = snapshot["c"] as String,
                                        businessStatus = if (snapshot["s"] != null) snapshot["s"] as ArrayList<String> else arrayListOf(),
                                        businessImages = if (snapshot["r"] != null) snapshot["r"] as ArrayList<String> else arrayListOf()
                                    )
                                )
                            }

                        }
                    }
                    businessesListener.myBusinesses(myBusinesses)
                } else {
                    businessesListener.error("Business does not exist")
                }
            }.addOnFailureListener {
                businessesListener.error(it.message.toString())
            }
    }

    fun addChatId(
        businessDocumentId: String,
        details: Map<String, String?>,
        listener: TrueFalseListener
    ) {
        businessCollection.document(businessDocumentId).collection("userchat")
            .add(details)
            .addOnSuccessListener { documentReference ->
                listener.result(result = true, message = documentReference.id)
            }.addOnFailureListener {
                listener.result(false)
            }
    }

    fun updateCurrentPaidAmount(businessId: String, amount: String, callback: (Boolean) -> Unit) {
        businessCollection.document(businessId).update(mapOf("p" to amount))
            .addOnSuccessListener {
                callback(true)
            }.addOnFailureListener {
                callback(false)
            }
    }


    fun updateProfile(
        documentId: String?,
        details: MutableMap<String, Any>,
        callback: (Pair<Boolean, String?>) -> Unit
    ) {
        val point = details["geoLocation"] as GeoPoint
        details.remove("geoLocation")
        businessCollection.document(documentId!!).setLocation(point.latitude, point.longitude)
            .addOnSuccessListener {
                businessCollection.document(documentId).update(details).addOnSuccessListener {
                    callback(Pair(true, "Profile updated successfully"))
                }.addOnFailureListener {
                    callback(Pair(false, "Unable to update profile at the moment!"))
                }
            }

    }

    fun deleteBusiness(
        documentId: String,
        imageUrls: ArrayList<String>,
        callback: (Pair<Boolean, String?>) -> Unit
    ) {
        val progressDialog = ProgressDialog(ctx)
        progressDialog.setTitle("Deleting businesses")
        progressDialog.setMessage("A moment.. Deleting your business...")
        progressDialog.show()

        if (imageUrls.isNotEmpty()) {
            imageUpload.deleteImageFromBucket(imageUrls) { deleted ->
                if (deleted) {
                    businessCollection.document(documentId).delete().addOnSuccessListener {
                        userCollection.document(store.getUserDocumentId()!!)
                            .collection("b").whereEqualTo("i", documentId).limit(1).get()
                            .addOnSuccessListener {
                                for (s in it) {
                                    userCollection.document(store.getUserDocumentId()!!)
                                        .collection("b")
                                        .document(s.id)
                                        .delete().addOnSuccessListener {
                                            progressDialog.dismiss()
                                            callback(Pair(true, "Business deleted!"))
                                        }
                                }
                            }

                    }.addOnFailureListener {
                        progressDialog.dismiss()
                        callback(Pair(false, "Unable to delete business"))
                    }
                }
            }
        } else {
            businessCollection.document(documentId).delete().addOnSuccessListener {
                userCollection.document(store.getUserDocumentId()!!)
                    .collection("b").whereEqualTo("i", documentId).limit(1).get()
                    .addOnSuccessListener {
                        for (s in it) {
                            userCollection.document(store.getUserDocumentId()!!).collection("b")
                                .document(s.id)
                                .delete().addOnSuccessListener {
                                    progressDialog.dismiss()
                                    callback(Pair(true, "Business deleted!"))

                                }
                        }
                    }

            }.addOnFailureListener {
                progressDialog.dismiss()
                callback(Pair(false, "Unable to delete business"))
            }
        }
    }

    fun getAllBusinessesListedInRange(
        center: GeoPoint,
        range: Double = 3.0,
        category: String? = null,
        callback: (ArrayList<Business>) -> Unit
    ) {
        Log.d(TAG, "Getting businesses in range")
        val centerLocation = Location(LocationManager.GPS_PROVIDER)
        centerLocation.latitude = center.latitude
        centerLocation.longitude = center.longitude
        val distance = Distance(range, BoundingBoxUtils.DistanceUnit.KILOMETERS)

        val query = if (category == null) {
            GeoQuery()
                .collection("business")
                .whereNearToLocation(centerLocation, distance)
        } else {

            GeoQuery()
                .collection("business")
                .whereEqualTo("c", category)
                .whereEqualTo("suspended", false)
                .whereNearToLocation(centerLocation, distance)

        }

        query.get()
            .addOnCompleteListener { e, documentRefs, _ ->
                val rList = arrayListOf<Business>()
                if (documentRefs.isNotEmpty()) {
                    for ((i, document) in documentRefs.withIndex()) {
                        if (document.exists() && document.contains("n") && document.contains("d") && document.contains(
                                "ts"
                            ) && document.contains("geoLocation") && document.contains("suspended") && !(document["suspended"] as Boolean)
                        ) {
                            rList.add(
                                Business(
                                    businessId = document.id,
                                    businessName = firstUpperCase(document["n"] as String),
                                    businessDescription = document["d"].toString(),
                                    message_token = document["t"].toString(),
                                    businessCreated = document["ts"].toString(),
                                    coordinates = document["geoLocation"] as GeoPoint,
                                    tag = document["c"] as String,
                                    lastPaidAmount = document["p"] as String,
                                    businessStatus = if (document["s"] != null) document["s"] as ArrayList<String> else arrayListOf(),
                                    businessImages = if (document["r"] != null) document["r"] as ArrayList<String> else arrayListOf()
                                )
                            )
                            Log.d(TAG, document["t"] as String)

                        }
                        if (i == documentRefs.size - 1) {
                            callback(rList)
                        }
                    }
                } else {
                    callback(rList)
                }
            }
    }

    fun deleteProfileImages(businessId: String, urls: ArrayList<String>) {
        val imageUpdates = hashMapOf<String, Any>(
            "r" to FieldValue.delete()
        )
        businessCollection.document(businessId).update(imageUpdates).addOnSuccessListener {
            imageUpload.deleteImageFromBucket(urls) { done ->
                if (done) {
                    Toasty.success(ctx, "Profile images deleted successfully", Toasty.LENGTH_SHORT)
                        .show()
                } else {
                    Toasty.error(
                        ctx,
                        "An error occurred deleting profile images",
                        Toasty.LENGTH_SHORT
                    ).show()
                }
            }
        }
    }


    fun getBusinessName(businessId: String, callback: (String?) -> Unit) {
        businessCollection.document(businessId).get().addOnSuccessListener { documentSnapshot ->
            Log.d(TAG, documentSnapshot!!.data.toString())
            if (!documentSnapshot.data.isNullOrEmpty()) {
                callback(documentSnapshot.data?.get("n") as String)
            } else {
                callback("No name available")
            }
        }.addOnFailureListener { e ->
            Log.d(TAG, e.message)
            callback(null)
        }
    }

    fun updateBusinessStatus(
        businessId: String,
        remainingUrls: ArrayList<String>,
        callback: (Boolean) -> Unit
    ) {
        businessCollection.document(businessId).update(mapOf("s" to remainingUrls))
            .addOnSuccessListener {
                callback(true)
            }.addOnFailureListener {
                callback(false)
            }
    }

    private fun firstUpperCase(x: String): String {
        val builder = StringBuffer()
        var char = ' '
        val charArr: CharArray = x.toCharArray()
        for (i in charArr.indices) {
            if (char == ' ' && charArr[i] != ' ') {
                builder.append(charArr[i].toUpperCase())
            } else {
                builder.append(charArr[i].toLowerCase())
            }
            char = charArr[i]
        }
        return builder.toString().trim()
    }
}