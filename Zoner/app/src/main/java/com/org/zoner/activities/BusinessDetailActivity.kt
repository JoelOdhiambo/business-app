package com.org.zoner.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import com.org.zoner.R
import com.org.zoner.fragments.ProfileFragment

class BusinessDetailActivity : AppCompatActivity() {
    lateinit var rootView: ConstraintLayout
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_business_detail)
        rootView = findViewById(R.id.rootView)

        val supportFragmentManager = supportFragmentManager.beginTransaction()
        val fragment = ProfileFragment()
        val bundle = Bundle()

        bundle.putString("businessId", intent.getStringExtra("businessId"))
        bundle.putString("name", intent.getStringExtra("name"))
        bundle.putString("description", intent.getStringExtra("description"))
        bundle.putString("created", intent.getStringExtra("created"))
        bundle.putString("messaging_token", intent.getStringExtra("message_token"))
        bundle.putString("status", intent.getStringExtra("status"))
        bundle.putString("images", intent.getStringExtra("images"))
        bundle.putString("point", intent.getStringExtra("point"))

        fragment.arguments = bundle
        supportFragmentManager.replace(R.id.rootView, fragment)
        supportFragmentManager.commit()

    }
}
