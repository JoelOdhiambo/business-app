package com.org.zoner.utils.notification

import android.content.Context
import android.content.Intent
import com.onesignal.OSNotificationOpenResult
import com.onesignal.OneSignal
import com.org.zoner.activities.MainActivity
import org.json.JSONObject

class NotificationOpenedHandler(context: Context) : OneSignal.NotificationOpenedHandler {
    var ctx: Context = context
    override fun notificationOpened(result: OSNotificationOpenResult?) {
        val jsonObject: JSONObject = result!!.notification.payload.additionalData
        val activityToBeOpened: Int = jsonObject.optInt("activity", -1)
        val fragmentToDirect: Int = jsonObject.optInt("fragment", -1)
        when (activityToBeOpened) {
            0 -> {
                openActivity(
                    MainActivity::class.java,
                    mapOf<String, String>("fragment" to fragmentToDirect.toString())
                )
            }
        }
    }

    private fun <T : Any?> openActivity(
        activityToBeOpened: Class<T>,
        additionalData: Map<String, String>
    ) {
        val intent: Intent = Intent(ctx, activityToBeOpened)
        additionalData.map {
            intent.putExtra(it.key, it.value)
        }
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        ctx.startActivity(intent)
    }


}