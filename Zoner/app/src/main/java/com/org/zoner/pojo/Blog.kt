package com.org.zoner.pojo

data class Blog(
    var id: String,
    var writeUp: String,
    var image: String,
    var timestamp: Long
)