package com.org.zoner.adapters

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.org.zoner.R
import com.org.zoner.activities.SearchCategoryActivity
import com.org.zoner.pojo.Category

class CategoriesAdapter(context: Context, categoryList: ArrayList<Category>) :
    RecyclerView.Adapter<CategoriesAdapter.CategoriesHolder>() {
    var ctx: Context = context
    var categories = categoryList
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoriesHolder {
        val view = LayoutInflater.from(ctx).inflate(R.layout.category_container, parent, false)
        return CategoriesHolder(view)
    }

    override fun getItemCount(): Int {
        return categories.size
    }

    override fun onBindViewHolder(holder: CategoriesHolder, position: Int) {
        val categoryObject: Category = categories[position]
        with(categoryObject) {
            val gd = holder.rootView.background as GradientDrawable
            gd.setColor(Color.parseColor(backgroundColor))
            holder.rootView.setOnClickListener { _ ->
            }
            holder.category.text = categoryName
        }
        holder.rootView.setOnClickListener {
            val intent = Intent(ctx, SearchCategoryActivity::class.java)
            intent.putExtra("category", categoryObject.categoryName)
            ctx.startActivity(intent)
        }
    }

    class CategoriesHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val rootView: RelativeLayout = itemView.findViewById(R.id.rootView)
        val category = itemView.findViewById<TextView>(R.id.category)
    }
}