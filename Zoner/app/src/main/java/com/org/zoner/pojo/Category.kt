package com.org.zoner.pojo

import android.graphics.Color

data class Category(
    var categoryName : String,
    var backgroundColor : String
)