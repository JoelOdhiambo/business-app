package com.org.zoner.offline

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(tableName = "favourite", indices = arrayOf(Index(value = ["businessId"], unique = true)))
data class Favourite(
    @PrimaryKey(autoGenerate = true)
    var favouriteId: Int = 0,
    var businessId: String = "",
    var favouritedOn: Long = System.currentTimeMillis()
)