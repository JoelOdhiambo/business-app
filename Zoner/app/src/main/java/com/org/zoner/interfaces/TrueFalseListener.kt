package com.org.zoner.interfaces

interface TrueFalseListener {
    fun result(result: Boolean, message: String = "")
}