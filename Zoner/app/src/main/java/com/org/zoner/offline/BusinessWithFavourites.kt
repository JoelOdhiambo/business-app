package com.org.zoner.offline

import androidx.room.Embedded
import androidx.room.Relation

data class BusinessWithFavourites(
    @Embedded var favourite : Favourite = Favourite(),
    @Relation(
        parentColumn = "businessId",
        entityColumn = "id"
    )
    var business: Business = Business()
)
