package com.org.zoner.interfaces

import android.view.View

interface RecyclerViewListener {
    fun onClick(view: View?, position: Int)
    fun onLongClick(view: View?, position: Int)
}