package com.org.zoner.activities

import android.graphics.Color
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.org.zoner.R
import com.org.zoner.adapters.ListBusinessAdapter
import com.org.zoner.interfaces.OnBottomReached
import com.org.zoner.interfaces.RecyclerViewListener
import com.org.zoner.pojo.Business

class SearchCategoryActivity : AppCompatActivity() {

    lateinit var toolBar: Toolbar
    lateinit var recyclerView: RecyclerView
    lateinit var adapter: ListBusinessAdapter
    lateinit var info: TextView

    var businessCategories = arrayListOf<Business>()
    lateinit var businessApi: com.org.zoner.API.Business

    val TAG = "SearchCategoryActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_category)
        initViews()
    }

    private fun initViews() {


        businessApi = com.org.zoner.API.Business(this@SearchCategoryActivity)

        toolBar = findViewById(R.id.toolBar)
        recyclerView = findViewById(R.id.recyclerView)
        info = findViewById(R.id.info)

        setSupportActionBar(toolBar)
        supportActionBar!!.title = intent.getStringExtra("category")
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp)

        toolBar.setTitleTextColor(Color.WHITE)

        recyclerView.layoutManager = LinearLayoutManager(this)

        adapter = ListBusinessAdapter(this@SearchCategoryActivity, arrayListOf(), adapterListener())
        recyclerView.adapter = adapter
        adapter.setOnBottomReachedListener(object : OnBottomReached {
            override fun bottomReached(position: Int) {
                getCategories(true)
            }

        })

        getCategories(false)
    }

    private fun getCategories(loadMore: Boolean) {
        businessApi.getBusinessByCategory(intent.getStringExtra("category")!!, loadMore) { list ->
            if (list.isNotEmpty()) {
                info.visibility = View.GONE
                businessCategories.addAll(list)
                adapter =
                    ListBusinessAdapter(
                        this@SearchCategoryActivity,
                        businessCategories,
                        adapterListener()
                    )
                adapter.setOnBottomReachedListener(object : OnBottomReached {
                    override fun bottomReached(position: Int) {
                        getCategories(true)
                    }

                })
                recyclerView.adapter = adapter
                adapter.notifyDataSetChanged()
            } else {
                if (businessCategories.isEmpty())
                    info.visibility = View.VISIBLE
            }
        }
    }

    fun adapterListener(): RecyclerViewListener {
        return object : RecyclerViewListener {
            override fun onClick(view: View?, position: Int) {

            }

            override fun onLongClick(view: View?, position: Int) {

            }

        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
