package com.org.zoner.activities

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.fragment.app.Fragment
import androidx.preference.PreferenceManager
import com.aurelhubert.ahbottomnavigation.AHBottomNavigation
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.mapbox.mapboxsdk.Mapbox
import com.org.zoner.API.Business
import com.org.zoner.API.PersistentStore
import com.org.zoner.API.User
import com.org.zoner.R
import com.org.zoner.fragments.*
import com.org.zoner.interfaces.SuccessErrListener

class MainActivity : AppCompatActivity() {
    var bottomNavigation: AHBottomNavigation? = null
    var coordinatorLayout: CoordinatorLayout? = null

    lateinit var userApi: User
    lateinit var businessApi: Business
    lateinit var store: PersistentStore
    var tabPosition: Int = 0;

    val TAG = "MainActivity"
    val gson = Gson()

    companion object {
        var businessChats: MutableMap<String, String> = mutableMapOf()

    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt("tabPosition", tabPosition)
        outState.putString("businessChats", getStringFromMap())
    }

    private fun getStringFromMap(): String {
        return gson.toJson(businessChats)
    }

    fun getMapFromString(json: String): MutableMap<String, String> {
        val token = object : TypeToken<MutableMap<String, String>>() {}.type
        return gson.fromJson(json, token)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        userApi = User(this)
        businessApi = Business(this)

        checkIfDocumentIdExists()
        getAllBusinessChats()
        getBusinessIds()
        init(savedInstanceState)
        store.setUserDocumentId("ewuZmBA4OELG0E2mmO7L")
    }

    private fun getBusinessIds() {
        businessApi.getBusinessIds("b") { ids ->
            store.setBusinessIds(ids)
        }
    }

    private fun getAllBusinessChats() {
        userApi.getChatsIStarted() { userListMap ->
            if (userListMap.isNotEmpty())
                businessChats = userListMap
        }
        getChatsToMe()

    }

    fun getChatsToMe() {
        businessApi.getChatsToMe { chatListMap ->
            if (chatListMap.isNotEmpty()) {
                for ((key, value) in chatListMap) {
                    businessChats[key] = value
                }
            }
        }

    }


    private fun checkIfDocumentIdExists() {
        if (userApi.checkIfUserDocumentIdExists()) {
            Log.d(TAG, "User document id does not exist")
            userApi.getUserDocumentId(object : SuccessErrListener {
                override fun success(ok: String) {
                    Log.d(TAG, ok)
                }

                override fun error(error: String) {
                    Log.d(TAG, error)
                }
            })
        } else {
            Log.d(TAG, "User document id does exist")
        }
    }

    private fun init(savedInstanceState: Bundle?) {
        Mapbox.getInstance(
            applicationContext,
            resources.getString(R.string.MAP_BOX_ACCESS_TOKEN)
        )


        PreferenceManager.setDefaultValues(this@MainActivity, R.xml.root_preferences, false)

        store = PersistentStore(this@MainActivity)

        bottomNavigation = findViewById(R.id.bottom_navigation)
        coordinatorLayout = findViewById(R.id.coordinator)


        val fragment: Fragment = ExploreFragment()
        val ft =
            supportFragmentManager.beginTransaction()
        ft.replace(R.id.coordinator, fragment)
        ft.commit()


        val explore =
            AHBottomNavigationItem("Explore", R.drawable.ic_explore_black_24dp)
        val chats =
            AHBottomNavigationItem("Chats", R.drawable.speech_bubble)
        val favourites =
            AHBottomNavigationItem("Favourites", R.drawable.ic_favorite_black_24dp)
        val profile =
            AHBottomNavigationItem("Profile", R.drawable.ic_person_black_24dp)
        val blog =
            AHBottomNavigationItem("blog", R.drawable.ic_info_outline_black_24dp)


        bottomNavigation!!.addItem(blog)
        bottomNavigation!!.addItem(explore)
        bottomNavigation!!.addItem(chats)
        bottomNavigation!!.addItem(favourites)
        bottomNavigation!!.addItem(profile)

        bottomNavigation!!.setForceTint(true)

        if (savedInstanceState != null) {
            bottomNavigation!!.currentItem = savedInstanceState.getInt("tabPosition")
            switchFragment(savedInstanceState.getInt("tabPosition"))
            tabPosition = savedInstanceState.getInt("tabPosition")
            businessChats = getMapFromString(savedInstanceState.getString("businessChats")!!)

        } else {
            bottomNavigation!!.currentItem = 1
        }
        bottomNavigation!!.setTranslucentNavigationEnabled(true)
        bottomNavigation!!.setTitleState(AHBottomNavigation.TitleState.ALWAYS_HIDE)
        bottomNavigation!!.setAccentColor(Color.parseColor("#f5af19"))
        bottomNavigation!!.setNotificationBackgroundColor(Color.RED)
        bottomNavigation!!.setNotificationTextColor(Color.WHITE)

        if (store.getMessageCount()!! > 0)
            bottomNavigation!!.setNotification(store.getMessageCount().toString(), 2)

        bottomNavigation!!.setOnTabSelectedListener(AHBottomNavigation.OnTabSelectedListener { position, _ ->
            tabPosition = position
            switchFragment(position)
            true
        })
    }

    private fun switchFragment(position: Int) {
        var fragment: Fragment? = null
        when (position) {
            0 -> fragment = BlogFragment()
            1 -> fragment = ExploreFragment()
            2 -> fragment = ChatFragment()
            3 -> fragment = FavouriteFragment()
            4 -> fragment = ProfileFragment()

        }
        val ft =
            supportFragmentManager.beginTransaction()
        assert(fragment != null)
        ft.replace(R.id.coordinator, fragment!!, position.toString())
        ft.commit()
    }

    override fun onResume() {
        super.onResume()
        if (store.getMessageCount()!! > 0)
            bottomNavigation!!.setNotification(store.getMessageCount().toString(), 2)
    }


    override fun onBackPressed() {
        val count = supportFragmentManager.backStackEntryCount
        if (count == 0) {
            super.onBackPressed()
        } else {
            supportFragmentManager.popBackStack()
        }
    }

    override fun onStop() {
        super.onStop()

    }
}