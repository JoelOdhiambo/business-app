package com.org.zoner.offline

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface BusinessDAO {
    @Query("SELECT * FROM business")
    fun getAllBusinesses(): LiveData<List<Business>>


    @Query("SELECT * FROM business WHERE id = :id")
    fun getBusinessFromId(id: String): LiveData<Business>

    @Query("SELECT * FROM business WHERE name = :name")
    fun getBusinessFromName(name: String): LiveData<List<Business>>

    @Transaction
    @Query("SELECT * FROM favourite")
    fun getFavourites(): LiveData<List<BusinessWithFavourites>>

    @Transaction
    @Query("SELECT * FROM search")
    fun getSearches(): LiveData<List<BusinessesWithSearches>>

    @Query("SELECT * FROM favourite WHERE businessId=:businessId")
    fun getFavouriteFromBusinessId(businessId: String): LiveData<Favourite>


    @Query("SELECT * FROM search WHERE businessId=:businessId")
    fun getSearchFromBusinessId(businessId: String): LiveData<Search>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertBusiness(business: Business): Long


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertSearch(search: Search): Long

    @Delete
    fun deleteSearch(search: Search)


    @Delete
    fun deleteBusiness(business: Business)
}