package com.org.zoner.fragments

import android.annotation.SuppressLint
import android.app.Activity.RESULT_OK
import android.app.ProgressDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.text.TextUtils
import android.util.Log
import android.view.*
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import androidx.viewpager.widget.ViewPager.OnPageChangeListener
import com.google.android.material.appbar.CollapsingToolbarLayout
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.firebase.firestore.GeoPoint
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.org.zoner.API.Business
import com.org.zoner.API.PersistentStore
import com.org.zoner.API.Transactions
import com.org.zoner.R
import com.org.zoner.activities.*
import com.org.zoner.adapters.MyBusinessesAdapter
import com.org.zoner.adapters.ProfilePagerAdapter
import com.org.zoner.interfaces.RecyclerViewListener
import com.org.zoner.models.BusinessModel
import com.org.zoner.pojo.Transaction
import com.org.zoner.pojo.User
import com.org.zoner.utils.image.ImageUpload
import com.rbrooks.indefinitepagerindicator.IndefinitePagerIndicator
import com.snatik.storage.Storage
import es.dmoral.toasty.Toasty
import kotlinx.android.synthetic.main.fragment_profile.*
import java.io.File
import java.util.*
import java.util.concurrent.TimeUnit

class ProfileFragment : Fragment(),
    View.OnClickListener, OnPageChangeListener {
    var viewPager: ViewPager? = null
    var toolbar: Toolbar? = null
    var collapsingToolbarLayout: CollapsingToolbarLayout? = null
    var pagerIndicator: IndefinitePagerIndicator? = null
    var autoPlay: RelativeLayout? = null
    var editProfile: RelativeLayout? = null
    var autoSlide: ImageView? = null
    lateinit var myBusinessesRecyclerView: RecyclerView
    lateinit var myBusinessesSheet: LinearLayout
    var myBusinesses = arrayListOf<com.org.zoner.pojo.Business>()
    var bottomSheetBehavior: BottomSheetBehavior<*>? = null
    lateinit var revealBusinessSheet: RelativeLayout
    lateinit var collapseBottomSheet: ImageView
    lateinit var firstBusinessText: TextView
    lateinit var businessDescription: TextView
    lateinit var noAddedBusinesses: TextView
    lateinit var chatContainer: RelativeLayout
    lateinit var likeContainer: RelativeLayout
    lateinit var viewRecentPayments: RelativeLayout
    lateinit var businessDetails: LinearLayout
    lateinit var statusContainer: RelativeLayout
    lateinit var shareContainer: RelativeLayout
    lateinit var directionContainer: RelativeLayout
    lateinit var addStatus: ImageView
    lateinit var account: ImageView
    lateinit var website: TextView
    lateinit var userName: TextView
    lateinit var userEmail: TextView
    lateinit var businessCount: TextView
    lateinit var passedBusinessName: TextView


    lateinit var renewText: TextView

    lateinit var addBusiness: RelativeLayout

    lateinit var gson: Gson
    lateinit var transactionsApi: Transactions
    lateinit var userApi: com.org.zoner.API.User


    var transactionList = arrayListOf<Transaction>()

    var currentPage = 0
    var isAutoPlaying = false
    var profilePagerAdapter: ProfilePagerAdapter? = null
    lateinit var businessesAdapter: MyBusinessesAdapter
    lateinit var businessApi: Business
    lateinit var store: PersistentStore

    var businessId: String? = null
    var passedName: String? = null
    var passedDescription: String? = null
    var passedCreated: String? = null
    var passedToken: String? = ""
    var passedStatus: String? = null
    var passedImages: String? = null
    var passedPoint: String? = null

    var geoPoint: GeoPoint? = null
    lateinit var userData: User
    lateinit var recyclerViewListener: RecyclerViewListener

    val TAG = "ProfileFragment"
    var currentBusinessId = ""

    lateinit var businessModel: BusinessModel

    lateinit var imageUpload: ImageUpload
    lateinit var storage: Storage


    val GALLERY_IMAGE_REQUEST = 200

    var currentBusinessItemPos = 0


    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        if (this::userData.isInitialized)
            outState.putString("businesses", getStringFromBusinessArray())
        if (businessId != null)
            outState.putString("businessId", businessId)
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val rootView =
            inflater.inflate(R.layout.fragment_profile, container, false)

        if (isAdded) {
            init(rootView, savedInstanceState)
        }
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewPagerForward.setOnClickListener {
            if (viewPager!!.currentItem + 1 <= myBusinesses[currentBusinessItemPos].businessImages.size)
                viewPager!!.currentItem = viewPager!!.currentItem + 1
        }

        viewPagerBackword.setOnClickListener {
            if (viewPager!!.currentItem - 1 >= 0)
                viewPager!!.currentItem = viewPager!!.currentItem - 1
        }

        renewText.setOnClickListener {
            val i = Intent(requireContext(), AddBusinessActivity::class.java)
            i.putExtra("businessId", currentBusinessId)
            requireActivity().startActivity(i)
        }
    }

    @SuppressLint("RestrictedApi")
    private fun init(rootView: View, savedInstanceState: Bundle?) {

        gson = Gson()
        store = PersistentStore(requireActivity())
        imageUpload = ImageUpload(requireActivity())
        transactionsApi = Transactions(requireActivity())
        userApi = com.org.zoner.API.User(requireActivity())

        storage = Storage(requireContext())

        viewPager = rootView.findViewById(R.id.imagesViewPager)
        toolbar = rootView.findViewById(R.id.toolbar)
        collapsingToolbarLayout = rootView.findViewById(R.id.collapsingToolbar)
        pagerIndicator = rootView.findViewById(R.id.pager_indicator)
        autoPlay = rootView.findViewById(R.id.autoPlay)
        autoSlide = rootView.findViewById(R.id.autoSlide)
        editProfile = rootView.findViewById(R.id.editProfile)
        myBusinessesRecyclerView = rootView.findViewById(R.id.myBusinessesRecyclerView)
        myBusinessesSheet = rootView.findViewById(R.id.myBusinessesSheet)
        bottomSheetBehavior = BottomSheetBehavior.from(myBusinessesSheet)
        revealBusinessSheet = rootView.findViewById(R.id.revealBusinessSheet)
        collapseBottomSheet = rootView.findViewById(R.id.collapseBottomSheet)
        firstBusinessText = rootView.findViewById(R.id.firstBusinessText)
        businessDescription = rootView.findViewById(R.id.businessDescription)
        noAddedBusinesses = rootView.findViewById(R.id.noAddedBusinesses)
        chatContainer = rootView.findViewById(R.id.chatContainer)
        likeContainer = rootView.findViewById(R.id.likeContainer)
        viewRecentPayments = rootView.findViewById(R.id.viewRecentPayments)
        businessDetails = rootView.findViewById(R.id.businessDetails)
        renewText = rootView.findViewById(R.id.renewText)

        statusContainer = rootView.findViewById(R.id.statusContainer)
        account = rootView.findViewById(R.id.account)
        website = rootView.findViewById(R.id.website)
        businessCount = rootView.findViewById(R.id.businessCount)
        userName = rootView.findViewById(R.id.userName)
        userEmail = rootView.findViewById(R.id.userEmail)
        shareContainer = rootView.findViewById(R.id.shareContainer)
        directionContainer = rootView.findViewById(R.id.dirContainer)
        addStatus = rootView.findViewById(R.id.addStatus)
        addBusiness = rootView.findViewById(R.id.addBusiness)
        passedBusinessName = rootView.findViewById(R.id.passedBusinessName)



        collapsingToolbarLayout!!.title = " "
        setHasOptionsMenu(true)
        (activity as AppCompatActivity?)!!.setSupportActionBar(toolbar)
        (activity as AppCompatActivity?)!!.supportActionBar!!.setDefaultDisplayHomeAsUpEnabled(true)
        (activity as AppCompatActivity?)!!.supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_edit_white_24dp)

        businessApi = Business(requireActivity())


        businessId = arguments?.getString("businessId")
        passedName = arguments?.getString("name")
        passedDescription = arguments?.getString("description")
        passedCreated = arguments?.getString("created")
        passedToken = arguments?.getString("messaging_token")
        passedStatus = arguments?.getString("status")
        passedImages = arguments?.getString("images")
        passedPoint = arguments?.getString("point")

        if (savedInstanceState != null) {
            businessId = savedInstanceState.getString("businessId")
        }


        viewPager!!.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {

            }

            override fun onPageSelected(position: Int) {
                if (businessId == null) {
                    if (myBusinesses[currentBusinessItemPos].businessImages.isNotEmpty()) {
                        if (position >= 0 && myBusinesses[currentBusinessItemPos].businessImages.isNotEmpty()) {
                            viewPagerForward.visibility = View.VISIBLE
                        }

                        if (position == myBusinesses[currentBusinessItemPos].businessImages.lastIndex) {
                            viewPagerForward.visibility = View.GONE
                        }

                        if (position > 0)
                            viewPagerBackword.visibility = View.VISIBLE

                        if (position == 0)
                            viewPagerBackword.visibility = View.GONE
                    }
                }

            }

        })




        recyclerViewListener = object : RecyclerViewListener {
            override fun onClick(view: View?, position: Int) {
                businessDetails.visibility = View.VISIBLE
                statusContainer.visibility = View.GONE
                addStatus.visibility = View.VISIBLE
                currentBusinessId = myBusinesses[position].businessId
                firstBusinessText.text = myBusinesses[position].businessName
                businessDescription.text = myBusinesses[position].businessDescription
                geoPoint = myBusinesses[position].coordinates
                website.text = myBusinesses[position].website

                directionContainer.visibility = View.VISIBLE
                shareContainer.visibility = View.VISIBLE
                bottomSheetBehavior!!.state = BottomSheetBehavior.STATE_COLLAPSED
                profilePagerAdapter =
                    ProfilePagerAdapter(requireActivity(), myBusinesses[position].businessImages)
                viewPager!!.adapter = profilePagerAdapter
                profilePagerAdapter!!.notifyDataSetChanged()

                renewText.visibility = View.GONE

                if (myBusinesses[position].lastPaidAmount!!.toInt() == 0) {
                    val difference =
                        System.currentTimeMillis() - myBusinesses[position].businessCreated.toLong()
                    val diffInDays = TimeUnit.DAYS.convert(difference, TimeUnit.MILLISECONDS)
                    if (diffInDays >= 30) {
                        renewText.visibility = View.VISIBLE
                        renewText.text = getString(R.string.boost_coverage)
                    }
                }

                currentBusinessItemPos = position

                if (checkIfBusinessIsExpired(myBusinesses[position].businessId)) {
                    renewText.visibility = View.VISIBLE
                    renewText.text = getString(R.string.renew_subscription)
                    businessApi.updateCurrentPaidAmount(
                        myBusinesses[position].businessId,
                        0.toString()
                    ) {

                    }
                }

                if (myBusinesses[currentBusinessItemPos].businessImages.isNotEmpty()) {
                    viewPagerForward.visibility = View.VISIBLE
                    autoPlay!!.visibility = View.VISIBLE
                }

            }

            override fun onLongClick(view: View?, position: Int) {
            }

        }

        if (businessId != null) {
            revealBusinessSheet.visibility = View.GONE
            likeContainer.visibility = View.VISIBLE
            chatContainer.visibility = View.VISIBLE
            viewRecentPayments.visibility = View.GONE
            firstBusinessText.text = passedName
            businessDescription.text = passedDescription
            currentBusinessId = businessId!!
            editProfile!!.visibility = View.GONE
            businessDetails.visibility = View.VISIBLE
            statusContainer.visibility = View.GONE
            addBusiness.visibility = View.GONE
            passedBusinessName.visibility = View.VISIBLE
            directionContainer.visibility = View.VISIBLE
            shareContainer.visibility = View.VISIBLE
            passedBusinessName.text = passedName
            if (passedPoint != null) {
                val latlng = passedPoint!!.split(",")
                geoPoint = GeoPoint(latlng[0].toDouble(), latlng[1].toDouble())
            }

            if (passedImages != null) {
                profilePagerAdapter = ProfilePagerAdapter(
                    activity,
                    getImageUrls(passedImages!!)
                )

                viewPager!!.adapter = profilePagerAdapter
                profilePagerAdapter!!.notifyDataSetChanged()
                pagerIndicator!!.attachToViewPager(viewPager)
            }

            if (store.getBusinessesIds().contains(businessId!!)) {
                chatContainer.visibility = View.GONE
            }


        } else {
            revealBusinessSheet.visibility = View.VISIBLE
            businessDetails.visibility = View.GONE
            if (savedInstanceState != null) {
                restoreDataFromState(savedInstanceState)
            } else {
                getProfile()
                showBusiness()
                getBusinessTransactions()
            }
        }


        autoPlay!!.setOnClickListener(this)
        viewPager!!.addOnPageChangeListener(this)
        editProfile!!.setOnClickListener(this)
        revealBusinessSheet.setOnClickListener(this)
        collapseBottomSheet.setOnClickListener(this)
        chatContainer.setOnClickListener(this)
        likeContainer.setOnClickListener(this)
        account.setOnClickListener(this)
        viewRecentPayments.setOnClickListener(this)
        shareContainer.setOnClickListener(this)
        directionContainer.setOnClickListener(this)
        addStatus.setOnClickListener(this)
        addBusiness.setOnClickListener {
            val intent = Intent(requireActivity(), AddBusinessActivity::class.java)
            requireActivity().startActivity(intent)
        }



        businessModel = ViewModelProviders.of(requireActivity()).get(BusinessModel::class.java)

        businessCount.text = "0"

    }

    private fun getOfflineTransactions() {
        transactionList = store.getTransactions().first
    }


    private fun getBusinessTransactions() {
        transactionsApi.getTransactions(store.getUserDocumentId()!!) {
            if (it.isNotEmpty()) {
                transactionList = it
                store.setTransactionDetails(transactionList)
            } else {
                Log.d(TAG, "Transactions are empty")
            }
        }
    }


    fun checkIfBusinessIsExpired(businessId: String): Boolean {
        val transactions = transactionList.filter { transaction: Transaction ->
            transaction.businessId == businessId
        }
        return if (transactions.isNotEmpty()) {
            System.currentTimeMillis() > (transactions[0].timeStamp + 2592000000)
        } else {
            false
        }
    }

    private fun getOfflineBusinesses() {
        myBusinesses = store.getBusinesses().first
        whenListReceived(myBusinesses, recyclerViewListener)
    }

    private fun getOfflineProfile() {
        userData = store.getUserDetails().first!!
        whenUserDataIsReceived(userData)
    }


    private fun restoreDataFromState(savedInstanceState: Bundle) {
        try {
            userData = getUserObject(savedInstanceState.getString("user")!!)
            myBusinesses = getBusinesses(savedInstanceState.getString("businesses")!!)
            whenListReceived(myBusinesses, recyclerViewListener)
            whenUserDataIsReceived(userData)
        } catch (ex: Exception) {

        }
    }

    private fun getProfile() {
        userApi.getProfileDetails { user: User ->
            whenUserDataIsReceived(user)
        }
    }

    private fun whenUserDataIsReceived(user: User) {
        userName.text = user.username
        userEmail.text = user.email
        businessDescription.text = user.about
        userData = user
        store.setUserDetails(user)
    }


    private fun showBusiness() {
        businessApi.getBusinesses("b") { list ->
            whenListReceived(list, recyclerViewListener)
        }
    }

    private fun whenListReceived(
        list: ArrayList<com.org.zoner.pojo.Business>,
        recyclerViewListener: RecyclerViewListener
    ) {
        businessCount.text = list.size.toString()
        if (list.size > 0) {
            if (isAdded && requireActivity() != null) {
                noAddedBusinesses.visibility = View.GONE
                myBusinessesRecyclerView.visibility = View.VISIBLE
                myBusinesses = list
                businessesAdapter =
                    MyBusinessesAdapter(
                        requireActivity(),
                        list,
                        recyclerViewListener,
                        isRecent = true,
                        fromProfile = true
                    )
                myBusinessesRecyclerView.adapter = businessesAdapter
                myBusinessesRecyclerView.layoutManager = LinearLayoutManager(requireActivity())
                businessesAdapter.notifyDataSetChanged()
                store.setBusinesses(myBusinesses)
            }
        } else {
            noAddedBusinesses.visibility = View.VISIBLE
            myBusinessesRecyclerView.visibility = View.GONE
            revealBusinessSheet.visibility = View.GONE
        }
    }

    override fun onCreateOptionsMenu(
        menu: Menu,
        inflater: MenuInflater
    ) {
        super.onCreateOptionsMenu(menu, inflater)
        requireActivity().menuInflater.inflate(R.menu.profile_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {
            android.R.id.home -> {
            }
            R.id.more -> {
                val view = requireActivity().findViewById<View>(R.id.more)
                val popupMenu = PopupMenu(requireActivity(), view)
                popupMenu.menuInflater.inflate(R.menu.profile_popup_menu, popupMenu.menu)
                if (businessId != null) {
                    popupMenu.menu.findItem(R.id.refresh).isVisible = false
                    if (passedImages == null)
                        popupMenu.menu.findItem(R.id.viewStatus).isVisible = false
                }
                popupMenu.setOnMenuItemClickListener { menuItem: MenuItem? ->
                    when (menuItem!!.itemId) {
                        R.id.refresh -> {
                            getProfile()
                            showBusiness()
                        }
                        R.id.viewStatus -> {
                            if (currentBusinessId != null || currentBusinessId != "") {
                                if (passedStatus != null && !TextUtils.isEmpty(
                                        passedStatus
                                    )
                                ) {
                                    if (passedStatus != null && getImageUrls(passedStatus!!).isNotEmpty()) {
                                        openStatusActivity(passedStatus!!)
                                    } else
                                        Toast.makeText(
                                            requireContext(),
                                            "This business has no status",
                                            Toast.LENGTH_SHORT
                                        ).show()
                                } else {
                                    if (myBusinesses.isNotEmpty()) {
                                        if (myBusinesses[currentBusinessItemPos].businessStatus.isNotEmpty()) {
                                            openStatusActivity(gson.toJson(myBusinesses[currentBusinessItemPos].businessStatus))
                                        } else {
                                            Toasty.info(
                                                requireActivity(),
                                                "You have no status",
                                                Toasty.LENGTH_SHORT
                                            ).show()
                                        }
                                    } else {
                                        Toasty.info(
                                            requireActivity(),
                                            "No status",
                                            Toasty.LENGTH_SHORT
                                        ).show()
                                    }
                                }
                            }
                        }
                    }
                    return@setOnMenuItemClickListener true
                }
                popupMenu.show()

            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun openStatusActivity(imageUrls: String) {
        val i = Intent(
            requireActivity(),
            StatusActivity::class.java
        )
        i.putExtra("businessId", currentBusinessId)
        i.putExtra(
            "urls",
            imageUrls
        )
        if (businessId == null) {
            i.putExtra("myStatus", true)
        }
        requireActivity().startActivity(i)
    }


    var autoSwipe = Timer()
    override fun onClick(v: View) {
        when (v.id) {

            R.id.addStatus -> {
                openGallery()
            }

            R.id.shareContainer -> {
                shareBusiness()
            }

            R.id.viewRecentPayments -> {
                val i = Intent(requireActivity(), TransactionActivity::class.java)
                requireActivity().startActivity(i)
            }
            R.id.account -> {
                businessDetails.visibility = View.GONE
                statusContainer.visibility = View.VISIBLE
                addStatus.visibility = View.GONE
                userEmail.text = userData.email
                userName.text = userData.username
                autoPlay!!.visibility = View.GONE
                profilePagerAdapter =
                    ProfilePagerAdapter(requireActivity(), listOf())
                profilePagerAdapter!!.notifyDataSetChanged()

                directionContainer.visibility = View.GONE
                shareContainer.visibility = View.GONE

                firstBusinessText.text = resources.getString(R.string.switch_to_business_view)
                bottomSheetBehavior!!.state = BottomSheetBehavior.STATE_COLLAPSED
                businessDescription.text = userData.about
                currentBusinessId = ""
            }
            R.id.dirContainer -> {
                openGoogleMapsIntent()
            }
            R.id.autoPlay -> {
                if (isAutoPlaying) {
                    autoSwipe.cancel()
                    autoSlide!!.setImageResource(R.drawable.ic_baseline_fast_forward_24)
                    isAutoPlaying = false
                } else {
                    autoSlide!!.setImageResource(R.drawable.ic_baseline_stop_24)
                    val handler = Handler()
                    val runnable = Runnable {
                        if (currentPage == profilePagerAdapter!!.count) currentPage =
                            0 else viewPager!!.setCurrentItem(currentPage++, true)
                    }
                    autoSwipe.schedule(object : TimerTask() {
                        override fun run() {
                            handler.post(runnable)
                        }
                    }, 2000, 2000)
                }
                isAutoPlaying = true
            }
            R.id.editProfile -> {
                val i = Intent(activity, EditProfileActivity::class.java)
                if (currentBusinessId != "") {
                    i.putExtra("businessId", currentBusinessId)
                    i.putExtra("name", firstBusinessText.text.toString())
                    i.putExtra("desc", businessDescription.text.toString())
                    i.putExtra("email", userData.email)
                    i.putExtra(
                        "businessImages",
                        gson.toJson(myBusinesses[currentBusinessItemPos].businessImages)
                    )
                    i.putExtra(
                        "statusImages",
                        gson.toJson(myBusinesses[currentBusinessItemPos].businessStatus)
                    )
                    i.putExtra("geoPoint", " ${geoPoint!!.latitude},${geoPoint!!.longitude}")
                    i.putExtra("website", website.text.toString())
                    i.putExtra("isBusiness", true)
                } else {
                    if (this::userData.isInitialized) {
                        i.putExtra("businessId", userData.userId)
                        i.putExtra("name", userData.username)
                        i.putExtra("desc", userData.about)
                        i.putExtra("email", userData.email)
                        i.putExtra("isBusiness", false)

                        requireActivity().startActivity(i)
                    } else {
                        i.putExtra("businessId", "")
                        i.putExtra("name", "No name")
                        i.putExtra("desc", "No description")
                        i.putExtra("email", "noemail@mail.com")
                        i.putExtra("isBusiness", false)
                        i.putExtra("joined", "No date joined")
                        requireActivity().startActivity(i)
                    }
                }

            }
            R.id.revealBusinessSheet -> {
                bottomSheetBehavior!!.state = BottomSheetBehavior.STATE_EXPANDED
            }
            R.id.collapseBottomSheet -> {
                bottomSheetBehavior!!.state = BottomSheetBehavior.STATE_COLLAPSED
            }
            R.id.chatContainer -> {
                val i = Intent(activity, ChatActivity::class.java)
                i.putExtra("businessId", currentBusinessId)
                i.putExtra("name", firstBusinessText.text.toString())
                i.putExtra("messaging_token", passedToken)
                requireActivity().startActivity(i)
            }
            R.id.likeContainer -> {
                if (businessId != null) {
                    userApi.addToFavourites(businessId!!) { added ->
                        if (added)
                            Toasty.success(
                                requireActivity(),
                                "Business added to favourites",
                                Toasty.LENGTH_SHORT
                            ).show()
                        else
                            Toasty.error(
                                requireActivity(),
                                "Unable to add this business to your favourite. Try again later",
                                Toasty.LENGTH_LONG
                            ).show()
                    }
                }
            }
        }
    }

    private fun shareBusiness() {

        if (passedDescription != null) {
            if (passedImages != null) {
                shareImageIntent(
                    getImageUrls(passedImages!!).first(),
                    passedName!!,
                    passedDescription!!
                )
                return
            } else {
                shareTextIntent(passedName!!, passedDescription!!)
                return
            }
        } else {
            if (myBusinesses.isNotEmpty()
            ) {
                if (myBusinesses[currentBusinessItemPos].businessImages.isNotEmpty()) {
                    shareImageIntent(
                        myBusinesses[currentBusinessItemPos].businessImages.first(),
                        myBusinesses[currentBusinessItemPos].businessName,
                        myBusinesses[currentBusinessItemPos].businessDescription
                    )
                    return
                } else {
                    shareTextIntent(
                        myBusinesses[currentBusinessItemPos].businessName,
                        myBusinesses[currentBusinessItemPos].businessDescription
                    )
                }

            }
        }


    }

    private fun shareTextIntent(name: String, description: String) {
        val share = Intent(Intent.ACTION_SEND)
        share.type = "text/plain"
        share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET)
        share.putExtra(
            Intent.EXTRA_TEXT, "I'm sharing with you this business" +
                    " from Zoner. ${name} is a business in Zoner! Here is the business description: ${description}. " +
                    "You can download the app for free from https://zoner.co.ke"
        )
        requireActivity().startActivity(Intent.createChooser(share, "Share profile!"))
    }

    private fun shareImageIntent(imageUrl: String, name: String, description: String) {
        getBitmapFromUrl(imageUrl) { uri ->
            if (uri != null) {
                val shareIntent = Intent()
                shareIntent.action = Intent.ACTION_SEND
                shareIntent.putExtra(
                    Intent.EXTRA_TEXT, "I'm sharing with you this business" +
                            " from Zoner. ${name} is a business in Zoner! Here is the business description ${description}. " +
                            "You can download the app for free from https://zoner.co.ke"
                )
                shareIntent.putExtra(Intent.EXTRA_STREAM, uri)
                shareIntent.type = "image/*"
                shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(Intent.createChooser(shareIntent, "Share business..."))
            } else {
                //?
            }
        }

    }

    fun getBitmapFromUrl(url: String, callback: (Uri?) -> Unit) {
        val HALF_MEGABYTE: Long = 512 * 512;
        val httpsReference: StorageReference =
            FirebaseStorage.getInstance()
                .getReferenceFromUrl(url)

        httpsReference.getBytes(HALF_MEGABYTE).addOnSuccessListener { bytes: ByteArray? ->
            if (bytes != null) {
                val savedFilePath =
                    "${storage.getExternalStorageDirectory(Environment.DIRECTORY_PICTURES)}/Zoner-${System.currentTimeMillis()}.png"
                val createFile = storage.createFile(
                    savedFilePath,
                    bytes
                )
                if (createFile) {
                    val uri: Uri = FileProvider.getUriForFile(
                        requireContext(), requireActivity().applicationContext
                            .packageName.toString() + ".provider", File(savedFilePath)
                    )
                    callback(uri)
                } else {
                    callback(null)
                }
            } else {
                callback(null)
            }
        }.addOnFailureListener {
            callback(null)
        }
    }


    private fun openGallery() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
        startActivityForResult(
            Intent.createChooser(intent, "Select image(s)"),
            GALLERY_IMAGE_REQUEST
        )
    }


    private val uriList by lazy {
        arrayListOf<String>()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val progressDialog = ProgressDialog(requireActivity())
        progressDialog.setTitle("Status")
        progressDialog.setMessage("Uploading status, a moment please...")
        progressDialog.setCancelable(false)

        if (resultCode == RESULT_OK) {
            when (requestCode) {
                GALLERY_IMAGE_REQUEST -> {
                    if (currentBusinessId != "") {
                        progressDialog.show()
                        if (data!!.clipData == null) {
                            imageUpload.uploadImage(
                                arrayListOf(data.data.toString()),
                                currentBusinessId, "s"
                            ) { result ->
                                if (result.second == 0)
                                    progressDialog.dismiss()
                            }
                        } else {

                            for (i in 0 until data.clipData!!.itemCount) {
                                val dataAtPos = data.clipData!!.getItemAt(i).uri.toString()
                                dataAtPos.let {
                                    uriList.add(dataAtPos)
                                }
                            }

                            imageUpload.uploadImage(uriList, currentBusinessId, "s") { result ->
                                if (result.second == uriList.size - 1) {
                                    progressDialog.dismiss()
                                }
                            }
                        }
                    } else {
                        Toasty.error(
                            requireActivity(),
                            "Select a business first",
                            Toasty.LENGTH_SHORT
                        )
                            .show()
                    }
                }
            }
        }
    }


    private fun openGoogleMapsIntent() {
        if (geoPoint != null) {
            val intent = Intent(
                Intent.ACTION_VIEW,
                Uri.parse(
                    "http://maps.google.com/maps?daddr=${geoPoint!!.latitude},${geoPoint!!.longitude}"
                )
            )
            requireActivity().startActivity(intent)
        }
    }

    override fun onPageScrolled(
        position: Int,
        positionOffset: Float,
        positionOffsetPixels: Int
    ) {
    }

    override fun onPageSelected(position: Int) {
        currentPage = position
    }


    override fun onPageScrollStateChanged(state: Int) {}

    private fun getStringFromUserObject(): String {
        return gson.toJson(userData)
    }

    private fun getStringFromBusinessArray(): String {
        return gson.toJson(myBusinesses)
    }

    private fun getUserObject(json: String): User {
        return gson.fromJson(json, User::class.java)
    }

    private fun getBusinesses(json: String): ArrayList<com.org.zoner.pojo.Business> {
        val type = object :
            TypeToken<ArrayList<com.org.zoner.pojo.Business?>?>() {}.type
        return gson.fromJson(json, type)
    }

    private fun getImageUrls(json: String): ArrayList<String> {
        val type = object :
            TypeToken<ArrayList<String?>?>() {}.type
        return gson.fromJson(json, type)
    }

    override fun onStop() {
        uriList.clear()
        super.onStop()
    }

}