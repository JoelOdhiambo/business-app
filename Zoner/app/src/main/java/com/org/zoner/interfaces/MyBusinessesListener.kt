package com.org.zoner.interfaces

import com.org.zoner.pojo.Business

interface MyBusinessesListener{
    fun myBusinesses(list : ArrayList<Business>)
    fun error(error : String)
}