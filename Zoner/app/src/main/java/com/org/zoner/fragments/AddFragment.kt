package com.org.zoner.fragments

import android.Manifest
import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.app.Activity.RESULT_OK
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import android.widget.AdapterView.OnItemSelectedListener
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.crystal.crystalrangeseekbar.widgets.BubbleThumbSeekbar
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.mapbox.mapboxsdk.camera.CameraPosition
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.plugins.places.picker.PlacePicker
import com.mapbox.mapboxsdk.plugins.places.picker.model.PlacePickerOptions
import com.org.zoner.API.Business
import com.org.zoner.API.PersistentStore
import com.org.zoner.Config
import com.org.zoner.Config.Companion.PRODUCTION_PASSKEY
import com.org.zoner.Config.Companion.PRODUCTION_SHORTCODE
import com.org.zoner.R
import com.org.zoner.activities.AddBusinessActivity
import com.org.zoner.utils.mpesa.Mpesa
import com.org.zoner.utils.mpesa.STKPush
import es.dmoral.toasty.Toasty

class AddFragment : Fragment(), OnItemSelectedListener,
    View.OnClickListener {
    var arrayAdapter: ArrayAdapter<String>? = null
    var myBusinessesArray = ArrayList<String>()
    var imagesArray = ArrayList<String>()

    var spinner: Spinner? = null

    lateinit var nameContainer: RelativeLayout
    lateinit var descContainer: RelativeLayout
    lateinit var categoryContainer: RelativeLayout
    lateinit var imagesContainer: RelativeLayout
    lateinit var costCalcContainer: RelativeLayout
    lateinit var locationContainer: RelativeLayout
    lateinit var addLocation: RelativeLayout
    lateinit var nextDetailFab: FloatingActionButton
    lateinit var fillOutProgress: ProgressBar
    lateinit var coordinator: CoordinatorLayout
    lateinit var lView: LinearLayout

    lateinit var rangeSeekBar: BubbleThumbSeekbar
    lateinit var nameEditText: EditText
    lateinit var descEditText: EditText
    lateinit var characterCounterText: TextView
    lateinit var addImage: RelativeLayout
    lateinit var priceText: TextView
    lateinit var distance: TextView
    lateinit var imagesCounter: TextView
    lateinit var locationText: TextView
    lateinit var back: ImageView
    lateinit var businessCategory: String
    var placeCoordinates: String? = null

    lateinit var businessApi: Business
    lateinit var store: PersistentStore
    var ANIM_DURATION: Long = 250
    var CHARGE: Int = 0


    var currentView: Int = 0

    val REQUIRED_PERMISSIONS = arrayOf(
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )
    lateinit var docIdPreferences: SharedPreferences
    var userDocumentId: String? = null

    val REQUEST_PERMISSIONS_CODE = 104
    val GALLERY_IMAGE_REQUEST = 3
    val REQUEST_CODE_AUTOCOMPLETE = 4
    val TAG = "AddFragment"
    var passedBusinessId: String? = null


    private val viewList = mutableListOf<View>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_add, container, false)
        init(rootView)
        return rootView
    }

    override fun onStart() {
        super.onStart()
        if (!checkSelfPermissions())
            ActivityCompat.requestPermissions(
                requireActivity(),
                REQUIRED_PERMISSIONS,
                REQUEST_PERMISSIONS_CODE
            )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (arguments != null && arguments?.getString("businessId") != null) {
            passedBusinessId = arguments?.getString("businessId")
            for (i in 0..4) {
                displayNextDetailView()
            }
        }
    }

    private fun init(rootView: View) {

        businessApi = Business(requireActivity())
        store = PersistentStore(requireActivity())

        spinner = rootView.findViewById(R.id.businessesSpinner)
        docIdPreferences =
            requireActivity().getSharedPreferences("pref_user_doc_id", Context.MODE_PRIVATE)
        userDocumentId = docIdPreferences.getString("id", "null")

        nameContainer = rootView.findViewById(R.id.nameContainer) as RelativeLayout
        descContainer = rootView.findViewById(R.id.descContainer)
        categoryContainer = rootView.findViewById(R.id.categoryContainer)
        imagesContainer = rootView.findViewById(R.id.imagesContainer)
        costCalcContainer = rootView.findViewById(R.id.costCalcContainer)
        nextDetailFab = rootView.findViewById(R.id.nextDetailFab)
        fillOutProgress = rootView.findViewById(R.id.fillOutProgress)
        rangeSeekBar = rootView.findViewById(R.id.rangeSeekBar)
        nameEditText = rootView.findViewById(R.id.nameEditText)
        descEditText = rootView.findViewById(R.id.descEditText)
        characterCounterText = rootView.findViewById(R.id.characterCounter)
        addImage = rootView.findViewById(R.id.addImage)
        distance = rootView.findViewById(R.id.distance)
        priceText = rootView.findViewById(R.id.priceText)
        imagesCounter = rootView.findViewById(R.id.imagesCounter)
        locationContainer = rootView.findViewById(R.id.locationContainer)
        addLocation = rootView.findViewById(R.id.addLocation)
        locationText = rootView.findViewById(R.id.locationText)
        coordinator = rootView.findViewById(R.id.coordinator)
        lView = rootView.findViewById(R.id.lview)
        back = rootView.findViewById(R.id.back)

        arrayAdapter = ArrayAdapter(
            requireActivity(),
            R.layout.businesses_spinner_layout,
            myBusinessesArray
        )
        arrayAdapter!!.setDropDownViewResource(R.layout.businesses_spinner_layout)
        spinner!!.setAdapter(arrayAdapter)
        spinner!!.setOnItemSelectedListener(this)
        nextDetailFab.setOnClickListener(this)
        addImage.setOnClickListener(this)
        addLocation.setOnClickListener(this)
        back.setOnClickListener(this)

        populate()
        arrayAdapter!!.notifyDataSetChanged()



        rangeSeekBar.setOnSeekbarChangeListener { number ->
            CHARGE = ((number.toInt() - 2) * 50)
            priceText.text = ((number.toInt() - 2) * 50).toString() + " KES"
            distance.text = "$number kM"
        }

        descEditText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                characterCounterText.text = (200 - s!!.length).toString()
            }

        })
    }

    private fun checkSelfPermissions() = REQUIRED_PERMISSIONS.all {
        ContextCompat.checkSelfPermission(
            requireActivity(),
            it
        ) == PackageManager.PERMISSION_GRANTED
    }

    private fun populate() {

        myBusinessesArray.add("Agriculture")
        myBusinessesArray.add("Education")
        myBusinessesArray.add("Transport")
        myBusinessesArray.add("Health")
        myBusinessesArray.add("Labour")
        myBusinessesArray.add("Food and Beverages")
        myBusinessesArray.add("Leisure Activities")
        myBusinessesArray.add("Real Estate")
        myBusinessesArray.add("Petroleum")

        businessCategory = myBusinessesArray[0]

        viewList.add(nameContainer)
        viewList.add(descContainer)
        viewList.add(categoryContainer)
        viewList.add(locationContainer)
        viewList.add(imagesContainer)
        viewList.add(costCalcContainer)
    }


    override fun onItemSelected(
        parent: AdapterView<*>?,
        view: View,
        position: Int,
        id: Long
    ) {
        businessCategory = myBusinessesArray[position]
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {}
    override fun onClick(v: View) {
        when (v.id) {
            R.id.back -> {
                if (currentView != 0)
                    displayPreviousDetailView()
            }
            R.id.nextDetailFab -> {
                if (passedBusinessId == null) {
                    if (currentView == 5) {
                        if (CHARGE == 0) {
                            if (nameEditText.text.toString() == "" || descEditText.text.toString() == "")
                                Toasty.error(
                                    requireActivity(),
                                    getString(R.string.no_business_name),
                                    Toasty.LENGTH_LONG
                                ).show()
                            else
                                addBusinessToCollection(false)
                        } else {
                            addBusinessToCollection(true)
                        }

                    } else {
                        displayNextDetailView()
                    }
                } else {
                    //Updating business payment
                    if (CHARGE != 0) {
                        AddBusinessActivity.businessId = passedBusinessId!!
                        pay()
                    } else {
                        Toasty.success(
                            requireContext(),
                            "Reverting to free business",
                            Toasty.LENGTH_SHORT
                        )
                            .show()
                        businessApi.updateCurrentPaidAmount(passedBusinessId!!, 0.toString()) { s ->
                            if (s) {
                                Toasty.success(
                                    requireContext(),
                                    "Reverted to free business",
                                    Toasty.LENGTH_SHORT
                                )
                                    .show()
                                requireActivity().finish()
                            }
                        }
                    }
                }

            }
            R.id.addImage -> {

                val dialog = Dialog(requireActivity())
                dialog.setContentView(R.layout.add_image_dialog)

                val gallery = dialog.findViewById(R.id.gallery) as TextView

                gallery.setOnClickListener {
                    dialog.dismiss()
                    openGallery()
                }
                dialog.window!!.attributes.windowAnimations = R.style.DialogEnterAnimations
                dialog.show()
            }
            R.id.addLocation -> {
                openSearchLocationActivity()
            }
        }
    }

    private fun addBusinessToCollection(paid: Boolean) {
        nextDetailFab.isEnabled = false
        val price: String = if (priceText.text.toString().replace(" KES", "").toInt() == 0)
            "0"
        else
            priceText.text.toString().replace(" KES", "")

        val details = hashMapOf<String, Any>(
            "b" to userDocumentId!!,
            "n" to nameEditText.text.toString().toUpperCase(),
            "d" to descEditText.text.toString(),
            "t" to store.getNotificationToken()!!,
            "c" to businessCategory,
            "p" to price,
            "suspended" to false,
            "ts" to System.currentTimeMillis().toString()
        )

        businessApi.addBusiness(
            details,
            imagesArray, placeCoordinates!!
        ) { result ->
            store.setBusinessRefresh(true)
            if (result.first) {
                nextDetailFab.isEnabled = true
                if (!paid) {
                    Toasty.success(
                        requireActivity(),
                        "Business added to the collection.",
                        Toasty.LENGTH_LONG
                    ).show()
                    requireActivity().finish()
                } else {
                    AddBusinessActivity.businessId = result.second!!
                    pay()
                }
            } else {
                nextDetailFab.isEnabled = true
                Toasty.error(requireActivity(), result.second!!, Toasty.LENGTH_LONG).show()
            }
        }
    }


    private fun pay() {
        val amount = priceText.text.toString().replace(" KES", "").toInt()
        if (amount > 0) {
            val pref: SharedPreferences =
                requireActivity().getSharedPreferences("pref_phone", Context.MODE_PRIVATE)
            val phoneNumber = pref.getString("phone", "null")
            if (!phoneNumber.equals("null")) {
                val builder: STKPush.Builder = STKPush.Builder(
                    PRODUCTION_SHORTCODE,
                    PRODUCTION_PASSKEY,
                    1,
                    PRODUCTION_SHORTCODE,
                    phoneNumber!!.replace("+", "")
                )
                builder.setCallBackURL(Config.CALLBACK_URL)
                val push: STKPush = builder.build()
                Mpesa.getInstance().pay(requireActivity(), push)
            } else {
                Toasty.error(
                    requireActivity(),
                    getString(R.string.missing_phone_number),
                    Toasty.LENGTH_SHORT
                ).show()
            }
        } else {
            Toasty.error(
                requireContext(),
                "Something went wrong, unexpected amount!",
                Toasty.LENGTH_SHORT
            ).show()
        }


    }

    private fun openSearchLocationActivity() {

        locationContainer.visibility = View.GONE
        var lastKnownLocation: LatLng? = null
        if (store.getLastLocation() != null && store.getLastLocation() != "null") {
            val locationPoint = store.getLastLocation()!!.split(",")
            lastKnownLocation = LatLng(locationPoint[0].toDouble(), locationPoint[1].toDouble())
        }

        startActivityForResult(
            PlacePicker.IntentBuilder()
                .accessToken(getString(R.string.MAP_BOX_ACCESS_TOKEN))
                .placeOptions(
                    PlacePickerOptions.builder()
                        .includeReverseGeocode(false)
                        .statingCameraPosition(
                            CameraPosition.Builder()
                                .target(lastKnownLocation ?: LatLng(1.2921, 36.8219)).zoom(16.0)
                                .build()
                        )
                        .build()
                )
                .build(requireActivity()), REQUEST_CODE_AUTOCOMPLETE
        )

    }

    private fun openGallery() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
        startActivityForResult(
            Intent.createChooser(intent, "Select image(s)"),
            GALLERY_IMAGE_REQUEST
        )
    }

    private fun openCamera() {

    }


    private fun displayNextDetailView() {
        slideLeftWithOpacity(viewList[currentView])
        slideRightWithOpacity(viewList[currentView + 1])
        currentView++
        fillOutProgress.progress = fillOutProgress.progress + 17
        if (currentView == 1) {
            scaleAnim(back, 0f, 1f) { f ->
                if (f) {
                    if (passedBusinessId == null)
                        back.visibility = View.VISIBLE
                }
            }
        }
    }

    private fun displayPreviousDetailView() {
        if (currentView == 1) {
            scaleAnim(back, 1f, 0f) { f ->
                if (f) {
                    if (passedBusinessId == null)
                        back.visibility = View.GONE
                }
            }
        }
        slideRightWithOpacity(viewList[currentView - 1])
        slideLeftWithOpacity(viewList[currentView])
        currentView--
        fillOutProgress.progress = fillOutProgress.progress - 17
    }

    private val scaleAnimatorSet: AnimatorSet = AnimatorSet()

    private fun scaleAnim(view: View, from: Float, to: Float, callback: (Boolean) -> Unit) {
        val scaleX: ObjectAnimator =
            ObjectAnimator.ofFloat(view, "scaleX", from, to)

        val scaleY: ObjectAnimator =
            ObjectAnimator.ofFloat(view, "scaleY", from, to)
        scaleAnimatorSet.play(scaleX).with(scaleY)
        scaleAnimatorSet.duration = ANIM_DURATION

        scaleAnimatorSet.addListener(object : Animator.AnimatorListener {
            override fun onAnimationRepeat(animation: Animator?) {

            }

            override fun onAnimationEnd(animation: Animator?) {
                callback(true)
            }

            override fun onAnimationCancel(animation: Animator?) {

            }

            override fun onAnimationStart(animation: Animator?) {
                callback(true)
            }

        })
        scaleAnimatorSet.start()
    }

    private val animationSet: AnimatorSet = AnimatorSet()

    private fun slideLeftWithOpacity(view: View) {
        val slideLeft: ObjectAnimator =
            ObjectAnimator.ofFloat(view, "translationX", 0f, -view.width.toFloat())
        val lowerOpacity: ObjectAnimator = ObjectAnimator.ofFloat(view, "alpha", 1f, 0f)
        animationSet.duration = 350
        animationSet.play(slideLeft).with(lowerOpacity)
        animationSet.addListener(object : Animator.AnimatorListener {
            override fun onAnimationRepeat(animation: Animator?) {

            }

            override fun onAnimationEnd(animation: Animator?) {
                view.alpha = 0f
                view.visibility = View.GONE
            }

            override fun onAnimationCancel(animation: Animator?) {

            }

            override fun onAnimationStart(animation: Animator?) {

            }

        })

        animationSet.start()
    }

    private fun slideRightWithOpacity(view: View) {
        val slideRight: ObjectAnimator =
            ObjectAnimator.ofFloat(view, "translationX", view.width.toFloat(), 0f)
        val increaseOpacity: ObjectAnimator = ObjectAnimator.ofFloat(view, "alpha", 1f, 0f)

        animationSet.play(slideRight).with(increaseOpacity)
        animationSet.addListener(object : Animator.AnimatorListener {
            override fun onAnimationRepeat(animation: Animator?) {

            }

            override fun onAnimationEnd(animation: Animator?) {
                view.alpha = 1f
                view.visibility = View.VISIBLE
            }

            override fun onAnimationCancel(animation: Animator?) {

            }

            override fun onAnimationStart(animation: Animator?) {

            }

        })
        animationSet.startDelay = 350
        animationSet.duration = 350
        animationSet.start()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == REQUEST_PERMISSIONS_CODE) {
            if (!checkSelfPermissions()) {
                Toasty.error(
                    requireActivity(),
                    getString(R.string.storage_permission_denied),
                    Toasty.LENGTH_SHORT
                ).show()
            } else {
                nextDetailFab.isEnabled = false
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            when (requestCode) {
                GALLERY_IMAGE_REQUEST -> {
                    if (data!!.clipData == null) {
                        imagesArray.add(data.data.toString())

                    } else {
                        for (i in 0 until data.clipData!!.itemCount) {
                            imagesArray.add(data.clipData!!.getItemAt(i).uri.toString())
                        }

                    }
                    imagesCounter.text =
                        " ${imagesArray.size}\t${getString(R.string.images_selected)}"
                }
                REQUEST_CODE_AUTOCOMPLETE -> {
                    locationContainer.visibility = View.VISIBLE
                    val bundle: Bundle = data?.extras!!
                    val cameraPosition =
                        bundle["com.mapbox.mapboxsdk.plugins.places.cameraPosition"].toString()
                    val startIndex = cameraPosition.indexOf("latitude=")
                    val endIndex = cameraPosition.indexOf(", altitude=")
                    val coordinateSubstring = cameraPosition.substring(startIndex, endIndex)
                    val coordinateSplit = coordinateSubstring.split(",")
                    val lng = coordinateSplit[0].replace("latitude=", "")
                    val lat = coordinateSplit[1].replace("longitude=", "")

                    placeCoordinates =
                        "${lat},${lng}"
                    locationText.text = "Location Selected"


                }
            }
        }
    }
}