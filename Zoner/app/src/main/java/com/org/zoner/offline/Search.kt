package com.org.zoner.offline

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(tableName = "search", indices = arrayOf(Index(value = ["businessId"], unique = true)))
data class Search(
    @PrimaryKey(autoGenerate = true)
    var searchId: Int = 0,
    var businessId: String = "",
    var searchedOn: Long = System.currentTimeMillis()
)