package com.org.zoner.pojo

data class User(
    var phoneNumber: String,
    var token: String,
    var joined: String,
    var username: String = "~",
    var email: String = "~",
    var userId: String = "",
    var about: String = "Add something about yourself."
)