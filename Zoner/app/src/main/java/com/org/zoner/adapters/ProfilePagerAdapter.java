package com.org.zoner.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import com.org.zoner.R;

import java.util.List;

public class ProfilePagerAdapter extends PagerAdapter {
    private Context ctx;
    private List<String> imageList;
    private LayoutInflater layoutInflater;

    public ProfilePagerAdapter(Context ctx, List<String> imageList) {
        this.ctx = ctx;
        this.imageList = imageList;
        this.layoutInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return imageList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == ((View) object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View view = layoutInflater.inflate(R.layout.large_profile_image, container, false);

        ImageView largeImage = view.findViewById(R.id.postedImages);

        RequestOptions requestOptions = new RequestOptions().error(R.drawable.no_image_placeholder);
        Glide.with(ctx).load(imageList.get(position))
                .transition(DrawableTransitionOptions.withCrossFade(600))
                .apply(requestOptions).into(largeImage);

        container.addView(view);

        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((ImageView) object);
    }
}
