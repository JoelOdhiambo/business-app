package com.org.zoner.activities

import android.graphics.Color
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.org.zoner.API.PersistentStore
import com.org.zoner.API.Transactions
import com.org.zoner.R
import com.org.zoner.adapters.TransactionAdapter
import com.org.zoner.databinding.ActivityTransactionBinding
import com.org.zoner.pojo.Transaction
import es.dmoral.toasty.Toasty

class TransactionActivity : AppCompatActivity(), SwipeRefreshLayout.OnRefreshListener {

    lateinit var activityBinding: ActivityTransactionBinding
    lateinit var transactionApi: Transactions
    lateinit var store: PersistentStore
    var transactionList = arrayListOf<Transaction>()

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViews(savedInstanceState)
    }

    private fun initViews(savedInstanceState: Bundle?) {
        activityBinding =
            DataBindingUtil.setContentView(this@TransactionActivity, R.layout.activity_transaction)

        activityBinding.recyclerView.layoutManager = LinearLayoutManager(this@TransactionActivity)
        setSupportActionBar(activityBinding.toolBar)

        activityBinding.toolBar.setTitleTextColor(Color.WHITE)
        supportActionBar!!.title = "Transactions"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp)

        transactionApi = Transactions(this@TransactionActivity)
        store = PersistentStore(this@TransactionActivity)
        getTransactions()
    }

    private fun getTransactions() {
        transactionApi.getTransactions(store.getUserDocumentId()!!) { list ->
            transactionList.clear()
            if (activityBinding.swipeRefreshLayout.isRefreshing)
                activityBinding.swipeRefreshLayout.isRefreshing = false
            transactionList = list

            if (transactionList.isEmpty()) {
                activityBinding.noTransactionText.visibility = View.VISIBLE
            } else {

                activityBinding.noTransactionText.visibility = View.GONE
                val adapter = TransactionAdapter(this@TransactionActivity, list)
                activityBinding.recyclerView.adapter = adapter
                adapter.notifyDataSetChanged()

            }
        }
    }

    override fun onRefresh() {
        getTransactions()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
