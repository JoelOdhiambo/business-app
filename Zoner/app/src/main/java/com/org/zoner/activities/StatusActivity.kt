package com.org.zoner.activities

import android.app.Dialog
import android.os.Bundle
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.org.zoner.API.Business
import com.org.zoner.R
import com.org.zoner.utils.image.ImageUpload
import es.dmoral.toasty.Toasty
import jp.shts.android.storiesprogressview.StoriesProgressView
import kotlinx.android.synthetic.main.activity_status.*


class StatusActivity : AppCompatActivity(), StoriesProgressView.StoriesListener {

    lateinit var storiesView: StoriesProgressView
    lateinit var statusImage: ImageView
    lateinit var imageUpload: ImageUpload
    lateinit var pauseView: View

    lateinit var businessApi: Business

    var storyUrls = ArrayList<String>()
    val gson = Gson()
    var counter = 0
    var businessId: String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_status)

        businessApi = Business(this)


        businessId = intent.getStringExtra("businessId")
        storyUrls = getArrayFromString(intent.getStringExtra("urls"))



        imageUpload = ImageUpload(this)

        storiesView = findViewById(R.id.stories)
        statusImage = findViewById(R.id.statusImage)
        pauseView = findViewById(R.id.pauseView)

        pauseView.setOnTouchListener(onTouchListener)
        storiesView.setStoriesCount(storyUrls.size); // <- set stories
        storiesView.setStoryDuration(5200L); // <- set a story duration
        storiesView.setStoriesListener(this); // <- set listener
        storiesView.startStories(); // <- start progress

        pauseView.setOnClickListener {
            // storiesView.resume()
        }
        showImage(counter)

        if (intent.getBooleanExtra("myStatus", false)) {
            statusDelete.visibility = View.VISIBLE
        }

        statusDelete.setOnClickListener {
            val dialog = Dialog(this)
            dialog.setContentView(R.layout.confirm_dialog)
            storiesView.pause()

            val yes = dialog.findViewById(R.id.yes) as TextView
            val no = dialog.findViewById(R.id.no) as TextView
            val dialogText = dialog.findViewById(R.id.dialogText) as TextView
            dialogText.text = getString(R.string.delete_status)

            yes.setOnClickListener {

                deleteStatus()
            }
            no.setOnClickListener {
                storiesView.resume()
                dialog.dismiss()
            }
            dialog.show()
        }
        statusFinish.setOnClickListener {
            finish()
        }

    }

    private fun getArrayFromString(json: String): ArrayList<String> {
        val token = object : TypeToken<ArrayList<String>>() {}.type
        return gson.fromJson(json, token)
    }

    private fun showImage(counter: Int) {
        Glide.with(this@StatusActivity)
            .applyDefaultRequestOptions(RequestOptions().error(R.drawable.person_placeholder))
            .load(storyUrls[counter])
            .into(statusImage)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.status_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
//            R.id.deleteStatus -> {
//                deleteStatus()
//            }
        }
        return super.onOptionsItemSelected(item)
    }


    private fun deleteStatus() {
        if (businessId != null) {
            storyUrls.removeAt(counter)
            businessApi.updateBusinessStatus(
                businessId!!,
                storyUrls
            ) {
                storiesView.resume()
                Toasty.success(this, "Status deleted successfully", Toasty.LENGTH_SHORT).show()
            }
        } else {
            storiesView.resume()
            Toasty.error(this, "Unexpected error unable to delete status", Toasty.LENGTH_SHORT)
                .show()
        }
    }


    var pressTime = 0L
    var limit = 500L

    private val onTouchListener: View.OnTouchListener = object : View.OnTouchListener {
        override fun onTouch(v: View?, event: MotionEvent?): Boolean {
            v!!.performClick()
            when (event!!.action) {
                MotionEvent.ACTION_DOWN -> {
                    pressTime = System.currentTimeMillis()
                    storiesView.pause()
                    return false
                }
                MotionEvent.ACTION_UP -> {
                    val now = System.currentTimeMillis()
                    storiesView.resume()
                    return limit < now - pressTime
                }
            }
            return false
        }

    }


    override fun onComplete() {
        finish()
    }

    override fun onPrev() {
        counter--
        showImage(counter)
    }

    override fun onNext() {
        counter++
        showImage(counter)
    }
}
