package com.org.zoner.API

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.annotation.NonNull
import com.google.firebase.FirebaseException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthProvider
import com.google.firebase.firestore.FirebaseFirestore
import com.org.zoner.activities.MainActivity
import com.org.zoner.interfaces.SMSVerificationCallback
import com.org.zoner.interfaces.UserExistsInterface
import es.dmoral.toasty.Toasty
import java.util.concurrent.TimeUnit


class Auth(context: Context, firebaseAuth: FirebaseAuth?) {
    var ctx: Context = context
    private var auth: FirebaseAuth? = firebaseAuth
    val db: FirebaseFirestore = FirebaseFirestore.getInstance()
    val store = PersistentStore(context)

    private val TAG: String = "AuthApi"

    fun sendSMSVerification(@NonNull phoneNumber: String, callback: SMSVerificationCallback) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
            phoneNumber,
            60,
            TimeUnit.SECONDS,
            ctx as Activity,
            object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
                override fun onVerificationCompleted(credentials: PhoneAuthCredential) {

                }

                override fun onVerificationFailed(p0: FirebaseException) {
                    callback.onError("Error verifying phone number: " + p0.message)
                }

                override fun onCodeSent(
                    verificationID: String,
                    p1: PhoneAuthProvider.ForceResendingToken
                ) {
                    super.onCodeSent(verificationID, p1)
                    callback.onCodeSent(verificationID)
                }
            }
        )
    }

    fun signInUser(
        credentials: PhoneAuthCredential,
        phoneNumber: String,
        callback: (Boolean) -> Unit
    ) {
        auth!!.signInWithCredential(credentials).addOnCompleteListener(ctx as Activity) { task ->
            if (task.isSuccessful) {
                if (store.clearSpecificPref("pref_phone")) {
                    if (store.setUserPhoneNumber(phoneNumber.replace("+", ""))
                    ) {
                        callback(true)
                    }
                }

            } else {
                callback(false)
                Toasty.error(ctx, task.result.toString(), Toasty.LENGTH_SHORT).show()
            }
        }
    }


    fun checkUserExistenceOnDb(phoneNumber: String, userExistsInterface: UserExistsInterface) {
        db.collection("user")
            .whereEqualTo("p", phoneNumber)
            .get()
            .addOnCompleteListener { documents ->
                if (documents.isSuccessful) {
                    if (documents.result!!.isEmpty) {
                        userExistsInterface.nonExistent()
                    } else {
                        userExistsInterface.exists()
                    }
                }
            }
    }

    fun addNewUserToStore(
        userDetails: Map<String, Any>
    ) {
        db.collection("user").whereEqualTo("p", userDetails["p"].toString()).limit(1)
            .get().addOnSuccessListener { querySnapshot ->
                if (querySnapshot.isEmpty) {
                    db.collection("user")
                        .add(userDetails)
                        .addOnSuccessListener { _ ->
                            saveInPref(userDetails)
                        }
                } else {
                    for (item in querySnapshot) {
                        db.collection("user").document(item.id).update(
                            mutableMapOf(
                                "n" to userDetails["n"],
                                "e" to userDetails["e"],
                                "j" to System.currentTimeMillis().toString()
                            )
                        ).addOnSuccessListener {
                            saveInPref(userDetails)
                        }
                    }
                }
            }

    }

    private fun saveInPref(
        userDetails: Map<String, Any>
    ) {
        if (store.clearSpecificPref("pref_phone")) {
            if (store.setUserPhoneNumber(
                    userDetails.get("p").toString().replace("+", "")
                )
            ) {
                val intent = Intent(
                    ctx,
                    MainActivity::class.java
                )
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                ctx.startActivity(intent)
            }
        }
    }


}