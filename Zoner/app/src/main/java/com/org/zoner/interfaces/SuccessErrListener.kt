package com.org.zoner.interfaces

interface SuccessErrListener{
    fun success(ok : String)
    fun error(error: String)
}