package com.org.zoner.API

import android.content.Context
import android.util.Log
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.QuerySnapshot
import com.org.zoner.pojo.Blog

class Blog(context: Context) {
    var ctx = context
    var blogReference = FirebaseFirestore.getInstance().collection("blog")
    var LAST_ITEM_KEY = 0.toLong()
    var TAG = "BlogApi"

    fun getBlogItems(loadMore: Boolean, blogList: (ArrayList<Blog>) -> Unit) {
        val blogs = arrayListOf<Blog>()
        val query: Query = if (!loadMore) {
            blogReference.orderBy("ts", Query.Direction.DESCENDING).limit(10)
        } else {
            Log.d(TAG, "Trying to get more")
            blogReference.orderBy("ts", Query.Direction.DESCENDING).startAfter(LAST_ITEM_KEY)
                .limit(10)
        }

        query.get().addOnSuccessListener { querySnapshot: QuerySnapshot? ->
            if (!querySnapshot!!.isEmpty) {
                for ((pos, snapshot) in querySnapshot.withIndex()) {
                    blogs.add(
                        Blog(
                            snapshot.id,
                            snapshot["w"] as String,
                            snapshot["r"] as String,
                            snapshot["ts"] as Long
                        )
                    )
                    if (pos == querySnapshot.documents.size - 1)
                        LAST_ITEM_KEY = snapshot["ts"] as Long
                }
                blogList(blogs)
            } else {
                blogList(blogs)
            }
        }
    }
}