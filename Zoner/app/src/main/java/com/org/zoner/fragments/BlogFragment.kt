package com.org.zoner.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.org.zoner.API.Blog
import com.org.zoner.R
import com.org.zoner.adapters.BlogAdapter
import com.org.zoner.interfaces.OnBottomReached

/**
 * A simple [Fragment] subclass.
 */
class BlogFragment : Fragment() {

    lateinit var blogRecyclerView: RecyclerView
    lateinit var blogApi: Blog
    lateinit var adapter: BlogAdapter
    lateinit var noBlogs: TextView
    var blogList = arrayListOf<com.org.zoner.pojo.Blog>()
    val gson = Gson()
    val TAG = "BlogFragment"

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        if (blogList.isNotEmpty())
            outState.putString("bloglist", gson.toJson(blogList))
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_blog, container, false)
        initViews(view, savedInstanceState)
        return view
    }

    private fun initViews(view: View, savedInstanceState: Bundle?) {
        blogApi = Blog(requireContext())
        blogRecyclerView = view.findViewById(R.id.blogRecyclerView)
        noBlogs = view.findViewById(R.id.noBlogsText)
        if (isAdded && requireActivity() != null){
            blogRecyclerView.layoutManager = LinearLayoutManager(requireActivity())

            adapter = BlogAdapter(requireContext(), blogList)
            blogRecyclerView.adapter = adapter
        }

        if (savedInstanceState != null) {
            val blogJson = savedInstanceState.getString("bloglist", "")
            if (blogJson != "")
                setAdapter(getBlogArray(blogJson))
            Log.d(TAG, "Saved Instance state not null")
        } else {
            Log.d(TAG, "Getting blog items")
            getBlogItems(false)
        }
    }

    private fun getBlogItems(loadMore: Boolean) {
        blogApi.getBlogItems(loadMore) { list ->
            if (list.isNotEmpty()) {
                if (loadMore)
                    blogList.addAll(list)
                else
                    blogList = list
                setAdapter(blogList)
            } else {
                if (blogList.isEmpty()) {
                    noBlogs.visibility = View.VISIBLE
                }
            }
        }
    }

    private fun setAdapter(list: ArrayList<com.org.zoner.pojo.Blog>) {
        noBlogs.visibility = View.GONE
        adapter = BlogAdapter(requireActivity(), list)
        adapter.setOnBottomReachedListener(object : OnBottomReached {
            override fun bottomReached(position: Int) {
                if (blogList.size - 1 >= 10) {
                    getBlogItems(true)
                }
            }
        })
        blogRecyclerView.adapter = adapter
        adapter.notifyDataSetChanged()
    }


    private fun getBlogArray(json: String): java.util.ArrayList<com.org.zoner.pojo.Blog> {
        val type = object :
            TypeToken<java.util.ArrayList<com.org.zoner.pojo.Blog?>?>() {}.type
        return gson.fromJson(json, type)
    }

}
