package com.org.zoner.utils.image

import android.content.Context
import android.graphics.Color
import androidx.core.content.ContextCompat
import androidx.lifecycle.LifecycleOwner
import com.org.zoner.R
import com.skydoves.balloon.ArrowOrientation
import com.skydoves.balloon.Balloon
import com.skydoves.balloon.BalloonAnimation
import com.skydoves.balloon.createBalloon

class ProfileBalloonFactory : Balloon.Factory() {
    override fun create(context: Context, lifecycle: LifecycleOwner?): Balloon {
        return createBalloon(context) {
            setText("Add a status")
            setArrowSize(10)
            setArrowOrientation(ArrowOrientation.BOTTOM)
            setWidthRatio(0.55f)
            setHeight(70)
            setArrowPosition(0.9f)
            setDismissWhenClicked(true)
            setDismissWhenTouchOutside(true)
            setTextColor(Color.WHITE)
            setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary))
            setBalloonAnimation(BalloonAnimation.CIRCULAR)
            setLifecycleOwner(lifecycle)
        }
    }
}