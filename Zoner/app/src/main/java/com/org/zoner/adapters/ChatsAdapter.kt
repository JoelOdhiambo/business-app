package com.org.zoner.adapters

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.org.zoner.API.Chat
import com.org.zoner.R
import com.org.zoner.activities.ChatActivity
import com.org.zoner.activities.MainActivity
import com.org.zoner.activities.SearchActivity
import com.org.zoner.adapters.ChatsAdapter.MessagesViewHolder
import com.org.zoner.pojo.Message
import com.org.zoner.utils.image.ColorGenerator
import com.org.zoner.utils.image.TextDrawable
import es.dmoral.toasty.Toasty
import java.util.*

class ChatsAdapter(
    private val messages: ArrayList<Message>,
    private val context: Context
) : RecyclerView.Adapter<MessagesViewHolder>() {

    var HEADER = 0
    var BODY = 1
    var chatApi = Chat(context)


    var colorGenerator: ColorGenerator = ColorGenerator.MATERIAL
    val TAG = "ChatsAdapter"

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessagesViewHolder {
        if (viewType == HEADER) {
            val headerView =
                LayoutInflater.from(context).inflate(R.layout.message_header, parent, false)
            return MessagesViewHolder(headerView, viewType)
        } else {
            val messageView =
                LayoutInflater.from(context).inflate(R.layout.message_body, parent, false)
            return MessagesViewHolder(messageView, viewType)
        }

    }

    override fun onBindViewHolder(holder: MessagesViewHolder, position: Int) {
        if (holder.viewType == HEADER) {
            holder.newMessage!!.setOnClickListener { _: View? ->
                val i = Intent(context, SearchActivity::class.java)
                context.startActivity(i)
            }
            holder.clearChats.setOnClickListener {
                val dialog = Dialog(context)
                dialog.setContentView(R.layout.confirm_dialog)

                val yes = dialog.findViewById(R.id.yes) as TextView
                val no = dialog.findViewById(R.id.no) as TextView
                val dialogText = dialog.findViewById(R.id.dialogText) as TextView

                dialogText.text = context.getString(R.string.clear_all_sent_chats)
                yes.setOnClickListener {
                    Toasty.success(context, "Clearing chats!", Toasty.LENGTH_SHORT).show()
                    chatApi.clearAllChats { cleared ->
                        if (cleared) {
                            messages.clear()
                            MainActivity.businessChats.clear()
                            notifyDataSetChanged()
                            Toasty.success(context, "Sent chats cleared!", Toasty.LENGTH_SHORT)
                                .show()
                            dialog.dismiss()

                        } else {
                            Toasty.error(
                                context,
                                "Something went wrong. Unable to clear chats!",
                                Toasty.LENGTH_SHORT
                            ).show()
                            dialog.dismiss()
                        }
                    }
                }
                no.setOnClickListener {
                    dialog.dismiss()
                }
                dialog.show()

            }
        } else if (holder.viewType == BODY) {
            val message = messages[position - 1]
            if (message.id == "") {
                holder.profileImage!!.visibility = View.GONE
            } else {
                holder.message!!.text = message.message
                holder.username!!.text = message.username
                val rq = RequestOptions().error(R.drawable.person_placeholder)
                Glide.with(context)
                    .load(message.profileUrl)
                    .apply(rq)
                    .listener(object : RequestListener<Drawable?> {
                        override fun onLoadFailed(
                            e: GlideException?,
                            model: Any,
                            target: Target<Drawable?>,
                            isFirstResource: Boolean
                        ): Boolean {
                            holder.fallOutImageView.visibility = View.VISIBLE
                            holder.profileImage!!.visibility = View.GONE
                            val randomColor = colorGenerator.randomColor
                            val letterDrawable: Drawable =
                                TextDrawable.builder()
                                    .buildRound(message.username.first().toString(), randomColor)
                            holder.fallOutImageView.setImageDrawable(letterDrawable)
                            return true
                        }

                        override fun onResourceReady(
                            resource: Drawable?,
                            model: Any,
                            target: Target<Drawable?>,
                            dataSource: DataSource,
                            isFirstResource: Boolean
                        ): Boolean {
                            holder.fallOutImageView.visibility = View.GONE
                            holder.profileImage!!.visibility = View.VISIBLE
                            return true
                        }
                    })
                    .into(holder.profileImage!!)
                holder.rootView!!.setOnClickListener { _: View? ->
                    openChatActivity(
                        context,
                        message.id,
                        message.username,
                        message.messageToken
                    )
                }
            }
        }
    }

    private fun openChatActivity(
        context: Context,
        id: String,
        username: String,
        token: String
    ) {
        val i = Intent(context, ChatActivity::class.java)
        i.putExtra("businessId", id)
        i.putExtra("name", username)
        i.putExtra("messaging_token", token)
        context.startActivity(i)
    }

    override fun getItemCount(): Int {
        return messages.size + 1
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == 0) HEADER else BODY
    }

    inner class MessagesViewHolder(itemView: View, viewType: Int) :
        RecyclerView.ViewHolder(itemView) {
        var viewType = 0
        var username: TextView? = null
        var message: TextView? = null
        var profileImage: ImageView? = null
        var newMessage: ImageView? = null
        var rootView: LinearLayout? = null
        lateinit var fallOutImageView: ImageView
        lateinit var clearChats: ImageView

        init {
            if (viewType == BODY) {
                username = itemView.findViewById(R.id.username)
                message = itemView.findViewById(R.id.messageText)
                profileImage = itemView.findViewById(R.id.profileImage)
                rootView = itemView.findViewById(R.id.rootView)
                fallOutImageView = itemView.findViewById(R.id.fallOutImage)
                this.viewType = BODY
            } else if (viewType == HEADER) {
                newMessage = itemView.findViewById(R.id.newMessage)
                clearChats = itemView.findViewById(R.id.clearAllChats)
                this.viewType = HEADER
            }
        }
    }

}