package com.org.zoner.pojo

import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.org.zoner.utils.date.DateTimeStyle
import com.org.zoner.utils.date.DateTimeUtils
import org.fabiomsr.moneytextview.MoneyTextView
import java.util.*

data class Transaction(
    var id: String,
    var businessId: String,
    var businessName: String,
    var price: Long,
    var timeStamp: Long
) {
    companion object {
        @BindingAdapter("formattedDate")
        @JvmStatic
        fun formattedDate(textView: TextView, date: Long) {
            val returnDate: Date = DateTimeUtils.formatDate(timeStamp = date.toLong())
            textView.text = DateTimeUtils.formatWithStyle(returnDate, DateTimeStyle.MEDIUM)
        }

        @BindingAdapter("setAmount")
        @JvmStatic
        fun setAmount(moneyTextView: MoneyTextView, amount: Long) {
            moneyTextView.amount = amount.toFloat()
        }
    }
}