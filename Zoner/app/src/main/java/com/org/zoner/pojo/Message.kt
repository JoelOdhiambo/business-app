package com.org.zoner.pojo

data class Message(
    var id: String,
    var message: String,
    var profileUrl: String,
    var username: String,
    var timeStamp: String,
    var by: String = "",
    var messageToken: String = ""
) : Comparable<Message> {
    override fun compareTo(other: Message): Int {
        return if (other.id == this.id && other.timeStamp == this.timeStamp) 0 else 1
    }
}