package com.org.zoner.offline

import androidx.room.Embedded
import androidx.room.Relation

data class BusinessesWithSearches(
    @Embedded var searches: Search = Search(),
    @Relation(
        parentColumn = "businessId",
        entityColumn = "id"
    )
    var business: Business = Business()
)