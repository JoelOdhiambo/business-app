package com.org.zoner.interfaces

interface OnBottomReached {
    fun bottomReached(position : Int)
}