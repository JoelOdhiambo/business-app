package com.org.zoner.interfaces;

import com.org.zoner.utils.mpesa.Pair;

public interface AuthListener {
    public void onAuthError(Pair<Integer, String> result);
    public void onAuthSuccess();
}