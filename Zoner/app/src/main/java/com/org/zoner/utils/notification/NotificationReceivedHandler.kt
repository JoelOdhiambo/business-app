package com.org.zoner.utils.notification

import android.content.Context
import android.util.Log
import com.onesignal.OSNotification
import com.onesignal.OneSignal
import com.org.zoner.API.PersistentStore
import org.json.JSONObject

class NotificationReceivedHandler(context: Context) : OneSignal.NotificationReceivedHandler {
    private val TAG = "NReceivedHandler"
    private var ctx: Context = context
    private val store = PersistentStore(context)

    override fun notificationReceived(notification: OSNotification?) {
        val jsonObject: JSONObject = notification!!.payload.additionalData
        val info: String = jsonObject.optString("info", "")
        if (info == null && info == "" && info != "payments") {
            if (!store.getAppGround()!!)
                if (store.setMessageCount(store.getMessageCount()!! + 1))
                    Log.d(TAG, "Message Count ${store.getMessageCount()}")
        }

    }
}