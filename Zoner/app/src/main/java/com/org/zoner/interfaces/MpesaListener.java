package com.org.zoner.interfaces;


import com.org.zoner.utils.mpesa.Pair;

public interface MpesaListener {
    public void onMpesaError(Pair<Integer, String> result);
    public void onMpesaSuccess(String MerchantRequestID, String CheckoutRequestID, String CustomerMessage);
}
