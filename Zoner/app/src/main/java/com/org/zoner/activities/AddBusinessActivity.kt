package com.org.zoner.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.org.zoner.API.Business
import com.org.zoner.API.PersistentStore
import com.org.zoner.R
import com.org.zoner.fragments.AddFragment
import com.org.zoner.interfaces.AuthListener
import com.org.zoner.interfaces.MpesaListener
import com.org.zoner.utils.mpesa.Mode
import com.org.zoner.utils.mpesa.Mpesa
import com.org.zoner.utils.mpesa.Pair
import es.dmoral.toasty.Toasty

class AddBusinessActivity : AppCompatActivity(), AuthListener, MpesaListener {
    companion object {
        var businessId: String = ""
    }

    lateinit var businessApi: Business
    lateinit var store: PersistentStore
    var passedBusinessId: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_business)
        initViews()
    }

    private fun initViews() {
        businessApi = Business(this)
        Mpesa.with(
            this,
            com.org.zoner.Config.PRODUCTION_CONSUMER_KEY,
            com.org.zoner.Config.PRODUCTION_CONSUMER_SECRET,
            Mode.SANDBOX
        )

        store = PersistentStore(this)
        val bundle = Bundle()
        val fragment: Fragment = AddFragment()
        val ft =
            supportFragmentManager.beginTransaction()
        passedBusinessId = intent.getStringExtra("businessId")
        if (intent.getStringExtra("businessId") != null) {
            bundle.putString("businessId", intent.getStringExtra("businessId"))
            fragment.arguments = bundle
        }
        ft.replace(R.id.coordinator, fragment)
        ft.commit()
    }

    override fun onAuthError(result: Pair<Int, String>?) {
        Toasty.error(this, result!!.message, Toasty.LENGTH_SHORT).show()
    }

    override fun onAuthSuccess() {

    }

    override fun onMpesaError(result: Pair<Int, String>?) {
        Toasty.error(this@AddBusinessActivity, result!!.message, Toasty.LENGTH_SHORT).show()
        businessId.let { id ->
            businessApi.deleteBusiness(businessId, arrayListOf()) {

            }
        }

    }

    override fun onMpesaSuccess(
        MerchantRequestID: String?,
        CheckoutRequestID: String?,
        CustomerMessage: String?
    ) {
        store.setTransactionsRefresh(true)
        val map = mutableMapOf<String, Any>("bid" to businessId, "crd" to CheckoutRequestID!!)
        if (passedBusinessId != null)
            map["existed"] = true
        businessApi.addPaymentDetailsToUserDocument(map) {
            if (it) {
                Toasty.success(
                    this@AddBusinessActivity,
                    "Request submitted for processing",
                    Toasty.LENGTH_SHORT
                ).show()
            }
        }
    }
}
