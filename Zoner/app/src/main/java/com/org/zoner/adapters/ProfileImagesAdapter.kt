package com.org.zoner.adapters

import android.content.Context
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.org.zoner.R
import com.org.zoner.interfaces.OnBottomReached
import es.dmoral.toasty.Toasty


class ProfileImagesAdapter(
    context: Context,
    var imageUrls: ArrayList<String>,
    var isUri: Boolean = false
) :
    RecyclerView.Adapter<ProfileImagesAdapter.ProfileImagesHolder>() {
    var ctx: Context = context
    val TAG = "ListBusinessAdapter"
    lateinit var bottomReachedListener: OnBottomReached

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProfileImagesHolder {
        val view = LayoutInflater.from(ctx).inflate(R.layout.display_profile_image, parent, false)
        return ProfileImagesHolder(view)
    }

    override fun getItemCount(): Int {
        return imageUrls.size
    }

    fun setOnBottomReachedListener(bottomReached: OnBottomReached) {
        bottomReachedListener = bottomReached
    }

    override fun onBindViewHolder(holder: ProfileImagesHolder, position: Int) {
        val imageUrl: String = imageUrls[position]
        if (position == imageUrls.size - 1)
            bottomReachedListener.bottomReached(position)


        Glide.with(ctx)
            .applyDefaultRequestOptions(RequestOptions().placeholder(R.drawable.placeholder_img))
            .load(if (isUri) Uri.parse(imageUrl) else imageUrl)
            .into(holder.businessImage)

    }

    inner class ProfileImagesHolder(
        itemView: View
    ) : RecyclerView.ViewHolder(itemView) {

        var businessImage: ImageView = itemView.findViewById(R.id.businessImage)

    }
}