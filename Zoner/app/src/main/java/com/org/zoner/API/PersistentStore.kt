package com.org.zoner.API

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.org.zoner.pojo.Business
import com.org.zoner.pojo.Transaction
import com.org.zoner.pojo.User

class PersistentStore(context: Context) {
    private var ctx: Context = context
    private var docPref: SharedPreferences =
        ctx.getSharedPreferences("pref_user_doc_id", Context.MODE_PRIVATE)

    private var phonePref: SharedPreferences =
        ctx.getSharedPreferences("pref_phone", Context.MODE_PRIVATE)

    private var tokenPref: SharedPreferences =
        ctx.getSharedPreferences("pref_token", Context.MODE_PRIVATE)

    private var messagePref: SharedPreferences =
        ctx.getSharedPreferences("pref_message", Context.MODE_PRIVATE)

    private var groundPref: SharedPreferences =
        ctx.getSharedPreferences("pref_grounds", Context.MODE_PRIVATE)

    private var locationPref: SharedPreferences =
        ctx.getSharedPreferences("pref_location", Context.MODE_PRIVATE)


    private var myBusinessesPref: SharedPreferences =
        ctx.getSharedPreferences("pref_businesses", Context.MODE_PRIVATE)

    private var prefUserDetails: SharedPreferences =
        ctx.getSharedPreferences("pref_user_details", Context.MODE_PRIVATE)

    private var prefTransactionDetails: SharedPreferences =
        ctx.getSharedPreferences("pref_transaction_details", Context.MODE_PRIVATE)


    private val gson = Gson()


    private var allPrefs = arrayListOf(
        docPref,
        phonePref,
        groundPref,
        tokenPref,
        messagePref,
        groundPref,
        prefUserDetails,
        prefTransactionDetails,
        myBusinessesPref,
        locationPref
    )

    fun setShownStatusBalloon(): Boolean {
        return prefUserDetails.edit().putBoolean("balloon", true).commit()
    }


    fun getStatusBallon(): Boolean {
        return prefUserDetails.getBoolean("balloon", false)
    }

    fun getExploreBalloon(): Boolean {
        return prefUserDetails.getBoolean("explore_balloon", false)
    }

    fun setExploreShownBalloon(): Boolean {
        return prefUserDetails.edit().putBoolean("explore_balloon", true).commit()
    }


    fun setUserDetails(user: User): Boolean {
        return prefUserDetails.edit().putString("user", gson.toJson(user)).commit() &&
                prefUserDetails.edit().putBoolean("refresh", false).commit()
    }

    fun setBusinesses(businesses: ArrayList<Business>): Boolean {
        myBusinessesPref.edit().clear().commit()
        return myBusinessesPref.edit().putString("businesses", gson.toJson(businesses)).commit() &&
                myBusinessesPref.edit().putBoolean("refresh", false).commit()
    }

    fun setBusinessIds(businesses: ArrayList<String>): Boolean {
        myBusinessesPref.edit().clear().commit()
        return myBusinessesPref.edit().putString("businessids", gson.toJson(businesses)).commit()
    }

    fun setTransactionDetails(transactions: ArrayList<Transaction>): Boolean {
        return prefTransactionDetails.edit().putString("transactions", gson.toJson(transactions))
            .commit() &&
                prefTransactionDetails.edit().putBoolean("refresh", false).commit()
    }


    fun setBusinessRefresh(refresh: Boolean): Boolean {
        return myBusinessesPref.edit().putBoolean("refresh", refresh).commit()
    }

    fun setTransactionsRefresh(refresh: Boolean): Boolean {
        return prefTransactionDetails.edit().putBoolean("refresh", refresh).commit()
    }

    fun setUserRefresh(refresh: Boolean): Boolean {
        return prefUserDetails.edit().putBoolean("refresh", refresh).commit()
    }

    fun shouldGetUserDetails(): Boolean {
        return prefUserDetails.getBoolean("refresh", true)
    }

    fun shouldGetMyBusinesses(): Boolean {
        return myBusinessesPref.getBoolean("refresh", true)
    }


    fun getUserDetails(): Pair<User?, Long> {
        val token = object : TypeToken<User>() {}.type
        val json = myBusinessesPref.getString("user", null)
        if (json != null) {
            return Pair(gson.fromJson(json, token), myBusinessesPref.getLong("updated", 0))
        }
        return Pair(null, 0)
    }

    fun getBusinesses(): Pair<ArrayList<Business>, Long> {
        val token = object : TypeToken<ArrayList<Business>>() {}.type
        val json = myBusinessesPref.getString("businesses", null)
        if (json != null) {
            return Pair(gson.fromJson(json, token), myBusinessesPref.getLong("updated", 0))
        }
        return Pair(arrayListOf(), 0)
    }

    fun getBusinessesIds(): ArrayList<String> {
        val token = object : TypeToken<ArrayList<String>>() {}.type
        val json = myBusinessesPref.getString("businessids", null)
        if (json != null) {
            return gson.fromJson(json, token)
        }
        return arrayListOf()
    }

    fun getTransactions(): Pair<ArrayList<Transaction>, Long> {
        val token = object : TypeToken<ArrayList<Transaction>>() {}.type
        val json = myBusinessesPref.getString("transactions", null)
        if (json != null) {
            return Pair(gson.fromJson(json, token), myBusinessesPref.getLong("updated", 0))
        }
        return Pair(arrayListOf(), 0)
    }


    fun getUserDocumentId(): String? {
        return docPref.getString("id", "null")
    }

    fun getAppGround(): Boolean? {
        return groundPref.getBoolean("foreground", false)
    }


    fun getUserPhoneNumber(): String? {
        return phonePref.getString("phone", "null")
    }

    fun getLastLocation(): String? {
        return locationPref.getString("location", "null")
    }

    fun getNotificationToken(): String? {
        return tokenPref.getString("token", "null")
    }

    fun getMessageCount(): Int? {
        return messagePref.getInt("count", 0)
    }

    fun setUserDocumentId(documentId: String): Boolean {
        return docPref.edit().putString("id", documentId).commit()
    }

    fun setUserPhoneNumber(phoneNumber: String): Boolean {
        return phonePref.edit().putString("phone", phoneNumber).commit()
    }

    fun setNotificationToken(token: String): Boolean {
        return tokenPref.edit().putString("token", token).commit()
    }

    fun setAppGrounds(isInForeGround: Boolean): Boolean {
        return groundPref.edit().putBoolean("foreground", isInForeGround).commit()
    }

    fun setMessageCount(count: Int): Boolean {
        return messagePref.edit().putInt("count", count).commit()
    }

    fun setLastLocation(location: String): Boolean {
        return locationPref.edit().putString("location", location).commit()
    }

    fun clearSpecificPref(prefName: String): Boolean {
        return ctx.getSharedPreferences(prefName, Context.MODE_PRIVATE).edit().clear().commit()
    }

    fun clearAllStorage() {
        allPrefs.forEach { pref ->
            with(pref) {
                edit().clear().commit()
            }
        }
    }


}