package com.org.zoner.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.org.zoner.R
import com.org.zoner.databinding.TransactionLayoutBinding
import com.org.zoner.pojo.Transaction

class TransactionAdapter(context: Context, transactionList: ArrayList<Transaction>) :
    RecyclerView.Adapter<TransactionAdapter.TransactionHolder>() {
    var transactions = transactionList
    var ctx = context
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TransactionHolder {
        val view = DataBindingUtil.inflate<TransactionLayoutBinding>(
            LayoutInflater.from(ctx), R.layout.transaction_layout, parent, false
        )
        return TransactionHolder(view)
    }

    override fun getItemCount(): Int {
        return transactions.size
    }

    override fun onBindViewHolder(holder: TransactionHolder, position: Int) {
        val obj = transactions[position]
        holder.item.transaction = obj
    }

    inner class TransactionHolder(itemView: TransactionLayoutBinding) :
        RecyclerView.ViewHolder(itemView.root) {
        val item: TransactionLayoutBinding = itemView
    }

}