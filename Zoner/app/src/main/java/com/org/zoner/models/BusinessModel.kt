package com.org.zoner.models

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.google.firebase.firestore.GeoPoint
import com.org.zoner.API.BusinessRepo
import com.org.zoner.offline.*

class BusinessModel(application: Application) : AndroidViewModel(application) {
    var repo: BusinessRepo = BusinessRepo(application)
    var api: com.org.zoner.API.Business = com.org.zoner.API.Business(application.applicationContext)
    var allBusinesses: LiveData<List<Business>> = repo.getAllBusinessesFromDB()
    var allSearches: LiveData<List<BusinessesWithSearches>> = repo.getSearches()




    fun rememberSearch(search: Search, callback: (Long) -> Unit) {
        repo.insertSearch(search) { id ->
            callback(id)
        }
    }



    fun deleteSearch(search: Search) {
        repo.deleteSearch(search)
    }




    fun allBusinessesFromId(id: String): LiveData<Business> {
        return repo.getBusinessById(id)
    }

    fun insertBusiness(business: Business, callback: (Long) -> Unit) {
        repo.insertBusiness(business) { id ->
            callback(id)
        }
    }


}