package com.org.zoner.activities

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.chaos.view.PinView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuth.AuthStateListener
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthProvider
import com.hbb20.CountryCodePicker
import com.org.zoner.API.Auth
import com.org.zoner.API.User
import com.org.zoner.R
import com.org.zoner.interfaces.SMSVerificationCallback
import com.org.zoner.interfaces.UserExistsInterface
import es.dmoral.toasty.Toasty

class LoginRegisterActivity : AppCompatActivity(), View.OnClickListener {
    var authStateListener: AuthStateListener? = null
    var firebaseAuth: FirebaseAuth? = null

    lateinit var timeOutText: TextView
    lateinit var verifyText: TextView
    lateinit var verifyButton: RelativeLayout
    lateinit var pinView: PinView
    lateinit var phoneNumberContainer: LinearLayout
    lateinit var countryCodePicker: CountryCodePicker
    lateinit var phoneNumber: EditText
    lateinit var autoFillText: TextView
    lateinit var userApi: User
    var bottomSheetBehavior: BottomSheetBehavior<*>? = null
    lateinit var detailsBottomSheet: LinearLayout
    lateinit var name: TextInputEditText
    lateinit var email: TextInputEditText
    lateinit var closeBusinessSheet: ImageView
    lateinit var submitFab: FloatingActionButton
    lateinit var auth: Auth


    var sentSmsVerification = false
    var vid: String? = null

    override fun onStart() {
        super.onStart()
        firebaseAuth!!.addAuthStateListener(authStateListener!!)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_register)
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
        checkAuthStatus()
        val window = window
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.statusBarColor = this.resources.getColor(R.color.colorAccent)
        }
        init()
    }

    private fun checkAuthStatus() {
        firebaseAuth = FirebaseAuth.getInstance()
        authStateListener = AuthStateListener {
            val user = it.currentUser
            if (user != null) {
                val i = Intent(this@LoginRegisterActivity, MainActivity::class.java)
                startActivity(i)
            }
        }
    }

    override fun onStop() {
        super.onStop()
        firebaseAuth!!.removeAuthStateListener(authStateListener!!)
        sentSmsVerification = false
    }

    private fun init() {

        auth = Auth(this@LoginRegisterActivity, firebaseAuth = this.firebaseAuth!!)

        verifyButton = findViewById(R.id.verifyButton)
        verifyText = findViewById(R.id.verifyText)
        timeOutText = findViewById(R.id.timeOutText)
        autoFillText = findViewById(R.id.autoFillText)
        pinView = findViewById(R.id.pinView)
        phoneNumberContainer = findViewById(R.id.phoneNumberContainer)
        phoneNumber = findViewById(R.id.phoneNumber)
        countryCodePicker = findViewById(R.id.countryCodePicker)
        detailsBottomSheet = findViewById(R.id.additonalDetailsBS)
        bottomSheetBehavior = BottomSheetBehavior.from(detailsBottomSheet)
        name = findViewById(R.id.nameEditText)
        email = findViewById(R.id.emailEditText)
        closeBusinessSheet = findViewById(R.id.closeBottomSheet)
        submitFab = findViewById(R.id.submitFab)

        verifyButton.setOnClickListener(this)

        userApi = User(this@LoginRegisterActivity)


        closeBusinessSheet.setOnClickListener(this)
        submitFab.setOnClickListener(this)

    }


    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.verifyButton -> {
                if (sentSmsVerification == false) {
                    checkPhoneValidity()
                } else {
                    if (vid != null && !pinView.text.toString().isBlank()) {
                        Toasty.success(this, "Verifying code...", Toasty.LENGTH_SHORT).show()
                        auth.signInUser(
                            PhoneAuthProvider.getCredential(vid!!, pinView.text.toString()),
                            countryCodePicker.selectedCountryCodeWithPlus + phoneNumber.text.toString()
                        ) { success ->
                            if (success) {
                                auth.checkUserExistenceOnDb(
                                    countryCodePicker.selectedCountryCodeWithPlus + phoneNumber.text.toString(),
                                    object : UserExistsInterface {
                                        override fun exists() {
                                            userApi.doesEmailExist(countryCodePicker.selectedCountryCodeWithPlus + phoneNumber.text.toString()) { exists ->
                                                if (exists) {
                                                    val intent = Intent(
                                                        this@LoginRegisterActivity,
                                                        MainActivity::class.java
                                                    )
                                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                                                    startActivity(intent)
                                                } else{
                                                    Log.d("Login", "nON EXISTENT 1")
                                                    bottomSheetBehavior!!.state =
                                                        BottomSheetBehavior.STATE_EXPANDED
                                                }
                                            }

                                        }

                                        override fun nonExistent() {
                                           Log.d("Login", "nON EXISTENT 2 ")
                                            bottomSheetBehavior!!.state =
                                                BottomSheetBehavior.STATE_EXPANDED

                                        }
                                    })
                            }
                        }
                    } else {
                        Toasty.error(this, "Please write the code...", Toasty.LENGTH_SHORT).show()
                    }
                }
            }
            R.id.closeBottomSheet -> {
                bottomSheetBehavior!!.state == BottomSheetBehavior.STATE_COLLAPSED
                bottomSheetBehavior!!.peekHeight = 400
            }
            R.id.submitFab -> {

                auth.addNewUserToStore(
                    mutableMapOf(
                        "n" to name.text.toString(),
                        "e" to email.text.toString(),
                        "p" to countryCodePicker.selectedCountryCodeWithPlus + phoneNumber.text.toString()
                            .replace("+", ""),
                        "ts" to System.currentTimeMillis()
                    )
                )
            }
        }
    }

    private fun checkPhoneValidity() {
        if (phoneNumber.text.toString() == "") {
            Toasty.info(
                this@LoginRegisterActivity,
                getString(R.string.phone_left_empty),
                Toasty.LENGTH_SHORT
            ).show()
        } else {
            val view = this.currentFocus
            view?.let { v ->
                val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
                imm?.hideSoftInputFromWindow(v.windowToken, 0)
            }

            verifyButton.isEnabled = true
            verifyText.text = getString(R.string.verify_code)


            pinView.visibility = View.VISIBLE
            pinView.alpha = 0f


            val fadeOut: ObjectAnimator =
                ObjectAnimator.ofFloat(phoneNumberContainer, "alpha", 1f, 0f)
            fadeOut.interpolator = AccelerateDecelerateInterpolator()
            fadeOut.duration = 1000

            val fadeIn: ObjectAnimator =
                ObjectAnimator.ofFloat(pinView, "alpha", 0f, 1f)
            fadeIn.interpolator = AccelerateDecelerateInterpolator()
            fadeIn.duration = 1000

            animatorSet.play(fadeOut).with(fadeIn)
            animatorSet.start()

            phoneNumberContainer.visibility = View.GONE
            timeOutText.visibility = View.VISIBLE


            autoFillText.text = getString(R.string.verify_code)
            codeTimer()
        }
    }

    val animatorSet: AnimatorSet = AnimatorSet()

    private fun codeTimer() {
        sentSmsVerification = true
        auth.sendSMSVerification(countryCodePicker.selectedCountryCodeWithPlus + phoneNumber.text.toString(),
            object : SMSVerificationCallback {
                override fun onSuccess(credential: PhoneAuthCredential) {

                }

                override fun onError(errorMessage: String) {
                    Toasty.error(this@LoginRegisterActivity, errorMessage, Toasty.LENGTH_LONG)
                        .show()
                }

                override fun onCodeSent(verificationId: String) {
                    this@LoginRegisterActivity.vid = verificationId
                }
            })


        object : CountDownTimer(60000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                timeOutText.text =
                    """${getString(R.string.time_out_text)} ${(millisUntilFinished / 1000).toInt()}"""
            }

            override fun onFinish() {
                verifyButton.isEnabled = true
                timeOutText.text = " "
                verifyText.text = getString(R.string.verify_number)
            }
        }.start()
    }
}