package com.org.zoner.utils.image

import android.content.Context
import android.graphics.Bitmap
import android.net.Uri
import android.provider.MediaStore
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import java.io.ByteArrayOutputStream
import java.util.concurrent.TimeUnit


class ImageUpload(context: Context) {
    private val ctx = context
    private val firebaseStorage: FirebaseStorage = FirebaseStorage.getInstance()
    private val storageReference: StorageReference = firebaseStorage.reference

    fun uploadImage(
        images: ArrayList<String>,
        businessId: String, field: String,
        callback: (Pair<String?, Int>) -> Unit
    ) {

        val baos = ByteArrayOutputStream()
        val urls = arrayListOf<String>()
        for ((pos, uriString) in images.withIndex()) {
            baos.reset()
            getBitmapFromURI(uriString).compress(Bitmap.CompressFormat.JPEG, 80, baos)

            val ref: StorageReference =
                storageReference.child("uploads/${System.currentTimeMillis().toString()}.jpeg")
            val uploadTask: UploadTask = ref.putBytes(baos.toByteArray())

            uploadTask.continueWithTask { task ->
                if (!task.isSuccessful) {
                    throw task.exception!!
                }
                ref.downloadUrl
            }.addOnCompleteListener(OnCompleteListener<Uri?> { task ->
                if (task.isSuccessful) {
                    val downloadUri = task.result
                    urls.add(downloadUri.toString()).also {
                        if (pos == images.size - 1) {
                            updateBusiness(urls, field, businessId) {
                                callback(Pair(null, pos))
                            }
                        } else
                            callback(Pair(null, pos))
                    }

                } else {
                    // Handle failures
                    // ...
                }
            })
        }

    }


    private fun updateBusiness(
        urls: ArrayList<String>,
        field: String,
        businessId: String, callback: (Boolean) -> Unit
    ) {
        val hoursToMillis = TimeUnit.HOURS.toMillis(24)
        FirebaseFirestore.getInstance().collection("business")
            .document(businessId)
            .update(mapOf(field to urls, "sa" to System.currentTimeMillis() + hoursToMillis)).addOnSuccessListener {
                callback(true)
            }
    }

    private fun getBitmapFromURI(uriString: String): Bitmap {
        return MediaStore.Images.Media.getBitmap(ctx.contentResolver, Uri.parse(uriString))
    }

    fun deleteImageFromBucket(urls: ArrayList<String>, callback: (Boolean) -> Unit) {
        for ((pos, url) in urls.withIndex()) {
            firebaseStorage.getReferenceFromUrl(url).delete().addOnSuccessListener {
                if (pos == urls.size - 1)
                    callback(true)
            }.addOnFailureListener {
                callback(false)
            }
        }
    }
}