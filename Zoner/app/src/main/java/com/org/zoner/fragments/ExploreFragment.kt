package com.org.zoner.fragments

import android.Manifest
import android.app.AlertDialog
import android.content.Context.LOCATION_SERVICE
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.location.LocationManager
import android.os.Bundle
import android.os.Looper
import android.provider.Settings
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.NonNull
import androidx.core.app.ActivityCompat
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.GeoPoint
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.google.gson.reflect.TypeToken
import com.mapbox.android.core.location.*
import com.mapbox.android.core.permissions.PermissionsListener
import com.mapbox.android.core.permissions.PermissionsManager
import com.mapbox.api.directions.v5.DirectionsCriteria
import com.mapbox.api.directions.v5.MapboxDirections
import com.mapbox.api.directions.v5.models.DirectionsResponse
import com.mapbox.core.constants.Constants
import com.mapbox.geojson.Feature
import com.mapbox.geojson.FeatureCollection
import com.mapbox.geojson.LineString
import com.mapbox.geojson.Point
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions
import com.mapbox.mapboxsdk.location.modes.CameraMode
import com.mapbox.mapboxsdk.location.modes.RenderMode
import com.mapbox.mapboxsdk.maps.MapView
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback
import com.mapbox.mapboxsdk.maps.Style
import com.mapbox.mapboxsdk.plugins.annotation.SymbolManager
import com.mapbox.mapboxsdk.plugins.annotation.SymbolOptions
import com.mapbox.mapboxsdk.style.expressions.Expression.*
import com.mapbox.mapboxsdk.style.layers.LineLayer
import com.mapbox.mapboxsdk.style.layers.Property
import com.mapbox.mapboxsdk.style.layers.Property.ICON_ROTATION_ALIGNMENT_VIEWPORT
import com.mapbox.mapboxsdk.style.layers.Property.TEXT_ANCHOR_TOP_RIGHT
import com.mapbox.mapboxsdk.style.layers.PropertyFactory
import com.mapbox.mapboxsdk.style.layers.PropertyFactory.*
import com.mapbox.mapboxsdk.style.layers.SymbolLayer
import com.mapbox.mapboxsdk.style.sources.GeoJsonOptions
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource
import com.mapbox.mapboxsdk.utils.BitmapUtils
import com.org.zoner.API.Business
import com.org.zoner.API.PersistentStore
import com.org.zoner.BuildConfig
import com.org.zoner.R
import com.org.zoner.activities.BusinessDetailActivity
import com.org.zoner.activities.SearchActivity
import com.org.zoner.adapters.ListBusinessAdapter
import com.org.zoner.interfaces.OnBottomReached
import com.org.zoner.interfaces.RecyclerViewListener
import com.org.zoner.utils.image.ExploreBalloonFactory
import com.skydoves.balloon.balloon
import es.dmoral.toasty.Toasty
import kotlinx.android.synthetic.main.fragment_explore.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * A simple [Fragment] subclass.
 */
class ExploreFragment : Fragment(), OnMapReadyCallback,
    PermissionsListener, LocationEngineCallback<LocationEngineResult>,
    View.OnClickListener {
    lateinit var mapView: MapView
    lateinit var mapBoxMap: MapboxMap
    lateinit var searchInfoText: TextView

    lateinit var cancelFilter: TextView
    lateinit var openMenuOptions: ImageView
    lateinit var collapseBottomSheet: ImageView
    lateinit var closeBusinessSheet: ImageView
    var bottomSheetBehavior: BottomSheetBehavior<*>? = null
    var bottomSheetBehaviorBusinesses: BottomSheetBehavior<*>? = null
    lateinit var filterBottomSheet: LinearLayout
    lateinit var businessListBottomSheet: LinearLayout
    lateinit var businessRecyclerView: RecyclerView
    lateinit var spinner: Spinner
    var arrayAdapter: ArrayAdapter<String>? = null
    var myBusinessesArray = ArrayList<String>()
    var permissionsManager: PermissionsManager? = null
    private var locationEngine: LocationEngine? = null
    private lateinit var businessAdapter: ListBusinessAdapter
    private lateinit var sharedPreferences: SharedPreferences
    private lateinit var gson: Gson

    private val optionsBallon by balloon(ExploreBalloonFactory::class)

    lateinit var businessApi: Business

    private val MAP_BOX_MAP_URI = "mapbox://styles/zoner/ckbf1xnkp3q4n1iqoi8ugi4wp"
    private val DEFAULT_WAIT_TIME_INTERVAL = 10000
    private val MAX_WAIT_TIME = DEFAULT_WAIT_TIME_INTERVAL * 2

    private val LINE_LAYER_ID = "LINE_LAYER_ID"
    private val LINE_SOURCE = "LINE_SOURCE"
    private val ICONS_SOURCE = "ICONS_SOURCE"
    private val ICONS_LAYER = "ICONS_LAYER"
    private val ICON_ID = "icon_id"

    private val TAG = "ExploreFragment"
    lateinit var store: PersistentStore

    var FILTER: String? = null

    lateinit var symbolManager: SymbolManager

    var resumedFromLocationSettings = false
    var businessList = arrayListOf<com.org.zoner.pojo.Business>()

    override fun onStart() {
        super.onStart()
        mapView.onStart()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val rootView =
            inflater.inflate(R.layout.fragment_explore, container, false)
        if (isAdded) {
            init(rootView, savedInstanceState)
        }
        return rootView
    }

    private fun init(rootView: View, savedInstanceState: Bundle?) {

        store = PersistentStore(requireActivity())
        businessApi = Business(requireActivity())
        gson = Gson()

        mapView = rootView.findViewById(R.id.mapView)
        searchInfoText = rootView.findViewById(R.id.searchInfoText)
        openMenuOptions = rootView.findViewById(R.id.openMenuOptions)
        filterBottomSheet = rootView.findViewById(R.id.filterBottomSheet)
        cancelFilter = rootView.findViewById(R.id.cancelFilter)
        closeBusinessSheet = rootView.findViewById(R.id.closeBusinessBottomSheet)
        businessListBottomSheet = rootView.findViewById(R.id.businessListBottomSheet)
        businessRecyclerView = rootView.findViewById(R.id.businessRecyclerView)
        collapseBottomSheet =
            rootView.findViewById(R.id.collapseBottomSheet)
        spinner = rootView.findViewById(R.id.businessTypeSpinner)
        populate()
        arrayAdapter = ArrayAdapter(
            requireActivity(),
            R.layout.businesses_spinner_layout,
            myBusinessesArray
        )
        arrayAdapter!!.setDropDownViewResource(R.layout.businesses_spinner_layout)
        spinner.adapter = arrayAdapter
        bottomSheetBehavior = BottomSheetBehavior.from(filterBottomSheet)
        bottomSheetBehaviorBusinesses = BottomSheetBehavior.from(businessListBottomSheet)

        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (position != 0) {
                    FILTER = arrayAdapter!!.getItem(position) as String
                    collapseSheet()
                }
            }

        }


        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(requireActivity())
        if (sharedPreferences.getBoolean("pref_list_first", false))
            bottomSheetBehaviorBusinesses!!.state = BottomSheetBehavior.STATE_EXPANDED


        val locationManager =
            requireActivity().getSystemService(LOCATION_SERVICE) as LocationManager

        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            showGPSDisabledAlertToUser()
        }

        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this)
        searchInfoText.setOnClickListener(this)
        openMenuOptions.setOnClickListener(this)
        cancelFilter.setOnClickListener(this)
        collapseBottomSheet.setOnClickListener(this)
        closeBusinessSheet.setOnClickListener(this)

        if (!store.getExploreBalloon()) {
            optionsBallon!!.showAlignBottom(openMenuOptions, 0, 35)
        }

        optionsBallon!!.setOnBalloonDismissListener {
            store.setExploreShownBalloon()
        }



        businessRecyclerView.layoutManager = LinearLayoutManager(activity)

        if (savedInstanceState != null) {
            businessList = getListFromString(savedInstanceState.getString("businesses")!!)
        }

    }

    private fun populate() {
        myBusinessesArray.add("No filter")
        myBusinessesArray.add("Agriculture")
        myBusinessesArray.add("Food and Beverages")
        myBusinessesArray.add("Health")
        myBusinessesArray.add("Transport")
        myBusinessesArray.add("Education")
    }


    private fun showGPSDisabledAlertToUser() {
        val alertDialogBuilder =
            AlertDialog.Builder(requireActivity())
        alertDialogBuilder.setMessage("GPS is disabled in your device. Would you like to enable it?")
            .setCancelable(false)
            .setPositiveButton(
                "Enable GPS"
            ) { _, _ ->
                resumedFromLocationSettings = true
                val callGPSSettingIntent = Intent(
                    Settings.ACTION_LOCATION_SOURCE_SETTINGS
                )
                requireActivity().startActivity(callGPSSettingIntent)
            }
        alertDialogBuilder.setNegativeButton(
            "Cancel"
        ) { dialog, _ -> dialog.cancel() }
        val alert = alertDialogBuilder.create()
        alert.show()
    }


    override fun onMapReady(mapboxMap: MapboxMap) {
        if (BuildConfig.DEBUG && activity == null) {
            error("Assertion failed")
        }
        var mapStyle: Style?

        val drawable = ResourcesCompat.getDrawable(
            requireActivity().resources,
            R.drawable.ic_location_on_orange_24dp,
            null
        )
        val iconBitmap = BitmapUtils.getBitmapFromDrawable(drawable)
        val uiSettings = mapboxMap.uiSettings
        uiSettings.compassGravity = Gravity.BOTTOM
        uiSettings.setCompassMargins(40, 0, 0, 160)

        mapBoxMap = mapboxMap
        mapboxMap.setStyle(

            Style.Builder().fromUri(MAP_BOX_MAP_URI).withImage(
                ICON_ID,
                iconBitmap!!
            )
        ) { style ->
            enableLocation(style)
            addSources(style)
            addLayer(style)
            mapStyle = style

            symbolManager = SymbolManager(mapView, mapboxMap, style)

            symbolManager.addClickListener { symbol ->
                val data = symbol.data as JsonObject
                val intent = Intent(requireActivity(), BusinessDetailActivity::class.java)
                intent.putExtra("businessId", data.get("businessId").asString)
                intent.putExtra("name", data.get("businessName").asString)
                intent.putExtra("description", data.get("businessDescription").asString)
                intent.putExtra("created", data.get("businessCreated").asString)
                intent.putExtra("message_token", data.get("message_token").asString)
                intent.putExtra("status", gson.toJson(data.getAsJsonArray("businessStatus")))
                intent.putExtra("images", gson.toJson(data.getAsJsonArray("businessImages")))
                intent.putExtra(
                    "point",
                    "${data.get("coordinates").asJsonObject.get("latitude").asDouble.toString()},${data.get(
                        "coordinates"
                    ).asJsonObject.get("longitude").asDouble.toString()} "
                )
                requireActivity().startActivity(intent)
            }

            symbolManager.iconAllowOverlap = true
            symbolManager.iconTranslate = arrayOf(-2f, 5f)
            symbolManager.iconRotationAlignment = ICON_ROTATION_ALIGNMENT_VIEWPORT

            if (businessList.isNotEmpty())
                addFeaturesToMap(businessList, mapStyle!!)

            mapBoxMap.addOnMapClickListener { latlng ->
                if (pointHasResults) {
                    val distance = calculateDistanceFromPreviousPoint(
                        GeoPoint(
                            latlng.latitude,
                            latlng.longitude
                        ), FLING_POINT!!
                    )

                    if (distance > 5) {
                        Log.d(TAG, "Not refreshing camera moved > 5km ")
                        Toasty.success(
                            requireContext(),
                            "Loading more businesses...",
                            Toasty.LENGTH_SHORT
                        )
                            .show()

                        addBusinessesToMaps(
                            GeoPoint(
                                latlng.latitude,
                                latlng.longitude
                            ), 5.0, mapStyle!!
                        )
                    } else {
                        Toasty.success(
                            requireContext(),
                            "All businesses in this area have already been loaded",
                            Toasty.LENGTH_SHORT
                        ).show()

                    }

                } else {
                    FLING_POINT = GeoPoint(
                        mapBoxMap.cameraPosition.target.latitude,
                        mapBoxMap.cameraPosition.target.longitude
                    )

                    addBusinessesToMaps(
                        GeoPoint(
                            mapBoxMap.cameraPosition.target.latitude,
                            mapBoxMap.cameraPosition.target.longitude
                        ), 5.0, mapStyle!!
                    )
                }
                return@addOnMapClickListener true
            }
        }
    }

    private fun addLayer(@NonNull style: Style) {
        style.addLayer(
            LineLayer(LINE_LAYER_ID, LINE_SOURCE).withProperties(
                lineCap(Property.LINE_CAP_ROUND),
                lineJoin(Property.LINE_JOIN_ROUND),
                lineGradient(
                    interpolate(
                        linear(),
                        lineProgress(),
                        stop(0f, requireActivity().resources.getColor(R.color.colorAccent)),
                        stop(1f, requireActivity().resources.getColor(R.color.colorPrimary))
                    )
                )
            )
        )

        style.addLayer(
            SymbolLayer(ICONS_LAYER, ICONS_SOURCE).withProperties(
                PropertyFactory.iconImage(ICON_ID),
                iconAllowOverlap(true),
                iconOffset(arrayOf(0f, 9f))
            )
        )
    }

    private fun addSources(@NonNull style: Style) {
        style.addSource(
            GeoJsonSource(
                LINE_SOURCE, GeoJsonOptions().withLineMetrics(true)
            )
        )

        style.addSource(
            GeoJsonSource(
                ICONS_SOURCE,
                FeatureCollection.fromFeatures(arrayOf<Feature>())
            )
        )
    }

    private fun getDirections(destination: Point, @NonNull style: Style) {
        val directionsBuilder = MapboxDirections.builder()
            .origin(ORIGIN_POINT!!)
            .destination(destination)
            .accessToken(requireActivity().getString(R.string.MAP_BOX_ACCESS_TOKEN))
            .overview(DirectionsCriteria.OVERVIEW_FULL)
            .profile(DirectionsCriteria.PROFILE_DRIVING_TRAFFIC)
            .build()
        directionsBuilder.enqueueCall(object : Callback<DirectionsResponse> {
            override fun onFailure(call: Call<DirectionsResponse>, t: Throwable) {
                Log.d(TAG, t.message)
            }

            override fun onResponse(
                call: Call<DirectionsResponse>,
                response: Response<DirectionsResponse>
            ) {
                if (response.isSuccessful) {
                    val firstRoute = response.body()!!.routes()[0]
                    val lineGradientSource = style.getSourceAs(LINE_SOURCE) as GeoJsonSource?
                    if (lineGradientSource != null) {
                        val lineString =
                            LineString.fromPolyline(firstRoute.geometry()!!, Constants.PRECISION_6)
                        lineGradientSource.setGeoJson(
                            Feature.fromGeometry(
                                lineString
                            )
                        )
                    }
                } else {
                    Log.d(TAG, "Directions response is unsuccessful")
                }
            }

        })

    }

    private fun enableLocation(style: Style) {
        if (PermissionsManager.areLocationPermissionsGranted(activity)) {
            val locationComponent = mapBoxMap.locationComponent
            locationComponent.activateLocationComponent(
                LocationComponentActivationOptions.builder(
                    requireActivity(),
                    style
                ).build()
            )
            if (ActivityCompat.checkSelfPermission(
                    requireContext(),
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    requireContext(),
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return
            }
            locationComponent.isLocationComponentEnabled = true
            locationComponent.cameraMode = CameraMode.TRACKING_COMPASS
            locationComponent.renderMode = RenderMode.COMPASS
            locationComponent.tiltWhileTracking(70.0)
            initializeLocationEngine()
        } else {
            permissionsManager = PermissionsManager(this)
            permissionsManager!!.requestLocationPermissions(activity)
        }
    }

    private fun initializeLocationEngine() {
        locationEngine = LocationEngineProvider.getBestLocationEngine(requireActivity())
        val locationEngineRequest =
            LocationEngineRequest.Builder(DEFAULT_WAIT_TIME_INTERVAL.toLong())
                .setPriority(LocationEngineRequest.PRIORITY_HIGH_ACCURACY)
                .setMaxWaitTime(MAX_WAIT_TIME.toLong()).build()
        if (ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {

            return
        }
        locationEngine!!.requestLocationUpdates(locationEngineRequest, this, Looper.getMainLooper())
        locationEngine!!.getLastLocation(this)
    }

    override fun onSuccess(result: LocationEngineResult) {
        val location = result.lastLocation ?: return
        ORIGIN_POINT =
            Point.fromLngLat(location.longitude, location.latitude)
        store.setLastLocation("${location.latitude},${location.longitude}")
        if (result.lastLocation != null) {
            mapBoxMap.locationComponent
                .forceLocationUpdate(location)
        }
    }

    private fun addBusinessesToMaps(geoPoint: GeoPoint, radius: Double, style: Style) {
        Log.d(TAG, "Adding business to maps")
        businessApi.getAllBusinessesListedInRange(geoPoint, radius, category = FILTER) { l ->

            if (l.isNotEmpty()) {
                if (noBusinessAsListInfo.visibility == View.VISIBLE) {
                    noBusinessAsListInfo.visibility = View.GONE
                }
                businessList.clear()
                businessList = l

                pointHasResults = true

                businessAdapter = ListBusinessAdapter(requireActivity(), l, adapterListener())
                businessRecyclerView.adapter = businessAdapter

                businessAdapter.setOnBottomReachedListener(object : OnBottomReached {
                    override fun bottomReached(position: Int) {

                    }

                })
                businessAdapter.notifyDataSetChanged()
                addFeaturesToMap(l, style)
            } else {
                Toasty.error(
                    requireContext(),
                    "This area doesn't seem to have businesses",
                    Toasty.LENGTH_SHORT
                ).show()
            }
        }
    }

    private fun addFeaturesToMap(
        l: ArrayList<com.org.zoner.pojo.Business>,
        style: Style
    ) {

        val features = arrayListOf<Feature>()

        for ((_, b) in l.withIndex()) {
            // km  = ( P / 50) + 2

            val paidKilometers = (b.lastPaidAmount!!.toInt() / 50) + 2
            Log.d(TAG, paidKilometers.toString())

            val diffDistance = calculateDistanceFromPreviousPoint(
                GeoPoint(
                    ORIGIN_POINT!!.latitude(),
                    ORIGIN_POINT!!.longitude()
                ), GeoPoint(b.coordinates.latitude, b.coordinates.longitude)
            )

            if (diffDistance <= paidKilometers) {
                val symbol = symbolManager.create(
                    SymbolOptions()
                        .withLatLng(LatLng(b.coordinates.latitude, b.coordinates.longitude))
                        .withIconImage(ICON_ID)
                        .withIconSize(1.5f)
                )
                val parse = JsonParser()
                val jsonObject: JsonObject = parse.parse(gson.toJson(b)) as JsonObject
                symbol.data = jsonObject
                symbol.textField = b.businessName
                symbol.textColor = "#f12711"
                symbol.textAnchor = TEXT_ANCHOR_TOP_RIGHT
            }
            val iconSource = style.getSourceAs<GeoJsonSource>(ICONS_SOURCE)
            iconSource!!.setGeoJson(FeatureCollection.fromFeatures(features))
        }

    }

    fun adapterListener(): RecyclerViewListener {
        return object : RecyclerViewListener {
            override fun onClick(view: View?, position: Int) {

            }

            override fun onLongClick(view: View?, position: Int) {

            }

        }
    }

    private fun calculateDistanceFromPreviousPoint(newPoint: GeoPoint, oldPoint: GeoPoint): Double {
        return 6378 * kotlin.math.acos(
            kotlin.math.sin(Math.toRadians(newPoint.latitude)) * kotlin.math.sin(
                Math.toRadians(
                    oldPoint.latitude
                )
            )
                    + kotlin.math.cos(Math.toRadians(newPoint.latitude)) * kotlin.math.cos(
                Math.toRadians(
                    oldPoint.latitude
                )
            ) * kotlin.math.cos(
                Math.toRadians(oldPoint.longitude) - Math.toRadians(newPoint.longitude)
            )
        )
    }

    override fun onFailure(exception: Exception) {
        if (exception.message != null) {
            Toast.makeText(activity, exception.message, Toast.LENGTH_SHORT).show()
        }
    }

    override fun onResume() {
        super.onResume()
        if (resumedFromLocationSettings) {
            mapBoxMap.getStyle { style ->
                enableLocation(style)
            }
            resumedFromLocationSettings = false
        }
        mapView.onResume()
    }

    override fun onPause() {
        super.onPause()
        mapView.onPause()
    }

    override fun onStop() {
        super.onStop()
        if (locationEngine != null) {
            locationEngine!!.removeLocationUpdates(this)
        }
        mapView.onStop()
        FILTER = null
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView.onLowMemory()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (locationEngine != null) {
            locationEngine!!.removeLocationUpdates(this)
        }
        mapView.onDestroy()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        mapView.onSaveInstanceState(outState)
        outState.putString("businesses", getStringFromBusinessList())
    }

    fun getStringFromBusinessList(): String {
        return gson.toJson(businessList)
    }

    fun getListFromString(json: String): ArrayList<com.org.zoner.pojo.Business> {
        val token = object : TypeToken<ArrayList<com.org.zoner.pojo.Business>>() {}.type
        return gson.fromJson(json, token)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        permissionsManager!!.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onExplanationNeeded(permissionsToExplain: List<String>) {
        Toast.makeText(
            activity, "Zoner requires location permission to show you businesses around you",
            Toast.LENGTH_LONG
        ).show()
    }

    override fun onPermissionResult(granted: Boolean) {
        if (granted) {
            mapBoxMap.setStyle(
                Style.Builder().fromUri(MAP_BOX_MAP_URI)
            ) { style -> enableLocation(style) }
        } else {
            Toast.makeText(
                activity,
                "You have denied location permissions. Zoner requires location permission to show you businesses around you.",
                Toast.LENGTH_LONG
            ).show()
            requireActivity().finish()
        }
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.searchInfoText -> {
                val i = Intent(activity, SearchActivity::class.java)
                requireActivity().startActivity(i)
            }
            R.id.openMenuOptions -> {
                val popupMenu = PopupMenu(activity, v)
                requireActivity().menuInflater.inflate(R.menu.explore_menu, popupMenu.menu)
                popupMenu.setOnMenuItemClickListener { item ->
                    when (item.itemId) {
                        R.id.list -> {
                            bottomSheetBehaviorBusinesses!!.state =
                                BottomSheetBehavior.STATE_EXPANDED
                        }
                        R.id.filter -> bottomSheetBehavior!!.setState(BottomSheetBehavior.STATE_EXPANDED)
                        R.id.signOut -> {
                            val persistentStore =
                                PersistentStore(requireActivity())
                            persistentStore.clearAllStorage()
                            FirebaseAuth.getInstance().signOut()
                            requireActivity().finish()
                        }
                    }
                    false
                }
                popupMenu.show()
            }
            R.id.cancelFilter, R.id.collapseBottomSheet -> collapseSheet()
            R.id.closeBusinessBottomSheet -> {
                bottomSheetBehaviorBusinesses!!.state = BottomSheetBehavior.STATE_COLLAPSED
            }
        }
    }

    private fun collapseSheet() {
        bottomSheetBehavior!!.state = BottomSheetBehavior.STATE_COLLAPSED
    }

    private var ORIGIN_POINT: Point? = null
    private var FLING_POINT: GeoPoint? = null
    private var pointHasResults = false

}