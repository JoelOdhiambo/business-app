<?php

$input = json_decode(file_get_contents('php://input'));


$resultCode = $input->Body->stkCallback->ResultCode;
$resultDesc = $input->Body->stkCallback->ResultDesc;

$checkoutRequestId = $input->Body->stkCallback->CheckoutRequestID;
$items;
$receipt;
$amount;

$servername = "localhost";
$username = "id13445664_zonerapp";
$password = "3FrPY|?AW]~dmT0s";
$dbname = "id13445664_zonerdb";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

$sql = "INSERT INTO dataviewer (crd, resultcode, resultdesc) VALUES ('{$checkoutRequestId}', '{$resultCode}','{$resultDesc}')";

if ($conn->query($sql) === TRUE) {
} else {
  echo "Error: " . $sql . "<br>" . $conn->error;
}

$conn->close();



switch ($resultCode) {
    case 0:
        $items = $input->Body->stkCallback->CallbackMetadata->Item;
        $receipt = $items[1]->Value;
        $amount  = $items[0]->Value;
        $message =  "Your payment was successful. Thank you!";
        break;
    case 1032:
        $message = "Payment request cancelled";
        break;
    case 1035:
        $message = "Payment request timed out";
        break;
    default:
        $message = "An unexpected error occured ".$resultCode;
        break;
}



$handler = new Handler;

if (isset($checkoutRequestId)) {
    if ((int) $resultCode == 0) {
        print_r($handler->addTransactionField($checkoutRequestId, $amount, $receipt));
    }
}
$player_id = $handler->getMessagingId($checkoutRequestId);

if ($player_id != '?') {
    $oneSignalResp = json_decode($handler->sendNotification(isset($receipt) ?  $receipt : 'No receipt', $message, $player_id));
} else {
    print_r('Invalid or missing player id');
}

if ((int) $resultCode != 0) {
    print_r($handler->removeBusiness($checkoutRequestId));
}

$rmCheckoutId = $handler->removeCheckoutRequestId($checkoutRequestId);


class Handler
{

    
    public function getMessagingId($checkoutRequestId)
    {
        $parameters = array("id" => $checkoutRequestId);
        $parameters = json_encode($parameters);
        $response = $this->callUrl($parameters, 'https://us-central1-zoner-b88d7.cloudfunctions.net/web/api/v1/getToken');
        return ($response);
    }

    public function removeBusiness($checkoutRequestId)
    {
        $parameters = array("id" => $checkoutRequestId);
        $parameters = json_encode($parameters);
        $response = $this->callUrl($parameters, 'https://us-central1-zoner-b88d7.cloudfunctions.net/web/api/v1/removeBusiness');
        return ($response);
    }

    public function addTransactionField($checkoutRequestId, $amount, $receipt)
    {
        $parameters = array("id" => $checkoutRequestId, "amount" => $amount, "receipt" => $receipt);
        $parameters = json_encode($parameters);
        $response = $this->callUrl($parameters, 'https://us-central1-zoner-b88d7.cloudfunctions.net/web/api/v1/addTransaction');
        return ($response);
    }

    public function removeCheckoutRequestId($checkoutRequestId)
    {
        $parameters = array("id" => $checkoutRequestId);
        $parameters = json_encode($parameters);
        $response = $this->callUrl($parameters, 'https://us-central1-zoner-b88d7.cloudfunctions.net/web/api/v1/rmCheckoutRequestId');
        return ($response);
    }
    public function sendNotification($receipt, $message, $player_id)
    {
        $contents = array("en" => $message);
        $heading = array("en" => "Zoner Payments");
        $fields = array(
            "app_id" => "65867896-fe1e-4c5e-9f41-7a90abeec400",
            "heading" => $heading,
            "contents" => $contents,
            "include_player_ids" => array($player_id),
            "data" => array("receipt" => $receipt, "info" => "payments", "type" => 2604)
        );

        $fields = json_encode($fields);

        return  $this->callUrl($fields, "https://onesignal.com/api/v1/notifications");
    }

    private function callUrl($fields, $url)
    {
        $c = curl_init();
        curl_setopt($c, CURLOPT_URL, $url);
        curl_setopt($c, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8'));
        curl_setopt($c, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($c, CURLOPT_HEADER, FALSE);
        curl_setopt($c, CURLOPT_POST, TRUE);
        curl_setopt($c, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($c, CURLOPT_SSL_VERIFYPEER, FALSE);
        $response = curl_exec($c);
        curl_close($c);
        return ($response);
    }
}
