/* eslint-disable promise/no-nesting */
const functions = require('firebase-functions');
const admin = require('firebase-admin')

const firebase = admin.initializeApp()

const express = require('express')
const bodyParser = require('body-parser')

const app = express()
const main = express()

main.use('/api/v1', app)
main.use(bodyParser.json())
main.use(bodyParser.urlencoded({ extended: false }))

const transactionCollection = admin.firestore().collection('transaction')
const usersCollection = admin.firestore().collection('user')
const businessCollection = admin.firestore().collection('business')


let getDataFromCheckoutId = (checkoutRequestId) => {
    return new Promise((resolve, reject) => {
        usersCollection.where('crd', '==', checkoutRequestId)
            .limit(1).get().then((querySnapshot) => {
                if (!querySnapshot.empty) {
                    querySnapshot.forEach((snapshot) => {
                        resolve(snapshot)
                    })
                } else {
                    reject(new Error('Querysnapshot does not exist'));
                }
                return
            }).catch((err) => {
                reject(err.message)
                return
            })
    })
}

let removeCheckOutRequestId = (userDocumentId) => {
    return new Promise((resolve, reject) => {
        usersCollection.doc(userDocumentId).update({
            crd: admin.firestore.FieldValue.delete(),
            bid: admin.firestore.FieldValue.delete()
        }).then((result) => {
            resolve('Deleted')
            return
        }).catch((error) => {
            reject(error.message)
        })
    })
}

app.post('/getToken', (req, res) => {
    if (req.body.id) {
        getDataFromCheckoutId(req.body.id).then((d) => {
            return res.send(d.data().t)
        }).catch((e) => {
            return res.send("?")
        })
    } else {
        res.send("?")
    }
})

app.post('/rmCheckoutRequestId', (req, res) => {
    getDataFromCheckoutId(req.body.id).then((d) => {
        return removeCheckOutRequestId(d.id).then((_) => {
            return res.send('Deleted')
        }).catch((e) => {
            res.send(e.message)
        })
    }).catch((e) => {

        return res.send("?")
    })
})

app.post('/addTransaction', (req, res) => {
    getDataFromCheckoutId(req.body.id).then((d) => {
        return transactionCollection.doc().set({
            uid: d.id,
            bid: d.data().bid,
            r: req.body.receipt,
            a: req.body.amount,
            ts: Date.now()
        }).then((_) => {
            return res.send('Transaction Added')
        }).catch((e) => {
            return res.send("?")
        })
    }).catch((e) => {
        return res.send("?")
    })
})

app.post("/removeBusiness", (req, res) => {
    if (req.body.id) {
        getDataFromCheckoutId(req.body.id).then((d) => {
            return businessCollection.doc(d.data().bid).delete().then((_) => {
                return res.send("Business Removed");
            }).catch((e) => {
                return res.send(e.message)
            })
        }).catch((e) => {
            return res.send("?")
        })


    } else {
        res.send("?")
    }
})


app.get('/checkExpiries', (req, res) => {
    businessCollection.where('p', '>', '0')
        .orderBy('ts', 'asc')
        .get()
        .then((snapshot) => {
            snapshot.forEach(s => {
                latestTransaction(s.id).then((querySnapshot) => {
                    return querySnapshot.forEach(q => {
                        if (Date.now() > parseInt(q.data().ts))
                            return businessCollection.doc(s.id).update({ x: true })
                        return ''
                    })
                }).catch((_) => {

                })
            })
            return

        }).catch((_) => {

        })
})

let latestTransaction = (businessId) => {
    return new Promise((v, j) => {
        transactionCollection.where('bid', '==', businessId)
            .orderBy('ts', 'desc')
            .limit(1)
            .get()
            .then((querySnapshot) => {
                v(querySnapshot)
                return
            }).catch((e) => {
                j(e.message)
            })
    })
}

const cleanUpBusinessFiles = functions.firestore.database().document('business/{businessid}')
    .onDelete((snapshot, context) => {
        if (snapshot.data().r || snapshot.data().s) {
            let profileImages = snapshot.data().r ? snapshot.data().r : []
            let statusImages = snapshot.data().s ? snapshot.data().s : []
            return deleteFilesFromBucket([...profileImages, ...statusImages])
        } else {
            return 'Skipping...No image urls.'
        }
    })

app.get('/checkExpiredStatus', async (req, res) => {
    try {
        const statusesSnapshot = await businessCollection.where('sa', "<=", Date.now()).get()
        if (!statusesSnapshot.empty) {
            statusesSnapshot.forEach( async (snapshot) => {
                if (snapshot.exists &&  snapshot.data() && snapshot.data().s) {
                    await businessCollection.doc(snapshot.id).update({
                        sa: admin.firestore.FieldValue.delete(),
                        s: admin.firestore.FieldValue.delete()
                    })
                    return await deleteFilesFromBucket(snapshot.data().s)
                } else
                    return 'Error'
            })
           
        } else return res.send('No statuses')
        return res.send('Status deleted')

    } catch (error) {
        return res.send('An error occured')
    }

})

const cleanUpBlogFiles = functions.firestore.database().document('blog/{blogid}')
    .onDelete((snapshot, context) => {
        if (snapshot.exists && snapshot.data() && snapshot.data().r) {
            return deleteFilesFromBucket([snapshot.data().r])
        }
        return 'Skipping...No image urls.'
    })



const deleteFilesFromBucket = (imagepaths) => {
  try {
    let bucket = firebase.storage().bucket()
    if (imagepaths && imagepaths.length > 0) {
        let deletionPromises = imagepaths.map(p => {
            return bucket.file(getPathStorageFromUrl(p)).delete()
        })
        return Promise.all(deletionPromises)
    } else {
        return false
    }
  } catch (error) {
      return false
  }
}

const getPathStorageFromUrl = (url) => {
    const baseUrl = "https://firebasestorage.googleapis.com/v0/b/zoner-b88d7.appspot.com/o/";
    let imagePath = url.replace(baseUrl, "");
    const indexOfEndPath = imagePath.indexOf("?");
    imagePath = imagePath.substring(0, indexOfEndPath);
    imagePath = imagePath.replace("%2F", "/");
    return imagePath;
}


const web = functions.https.onRequest(main)


module.exports = {
    web,
    cleanUpBusinessFiles,
    cleanUpBlogFiles
}